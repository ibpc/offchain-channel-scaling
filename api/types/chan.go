/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package types

// ChanCreate create chan struct
type ChanCreate struct {
	ChanName string `json:"chan_name" binding:"required"`
	Balance  int64  `json:"balance" binding:"required"`
}

// ChanJoin join chan struct
type ChanJoin struct {
	ChanName string `json:"chan_name" binding:"required"`
	Balance  int64  `json:"balance" binding:"required"`
}

// Trade trade struct
type Trade struct {
	ChanName string `json:"chan_name" binding:"required"`
	Amount   int    `json:"amount" binding:"required"`
	NodeID   string `json:"node_id" binding:"required"`
	Sync     bool   `json:"sync"`
}

// Status node status
type Status struct {
	ChanName string `form:"chan_name" binding:"required"`
}

// ChanExit exit chan struct
type ChanExit struct {
	ChanName string `json:"chan_name" binding:"required"`
}

// ChanExitResp exit chan response struct
type ChanExitResp struct {
	Code    int    `json:"code"`
	Balance int64  `json:"balance"`
	Msg     string `json:"msg"`
}
