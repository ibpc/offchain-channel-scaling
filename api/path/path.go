/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package path

const (
	// HealthCheck 服务端健康检查路由
	HealthCheck = "/health"
	// Shutdown 服务端关闭路由
	Shutdown = "/shutdown"
)
