## 链下通道（长安链）

* [相关文档](https://docs.chainmaker.org.cn/dev/%E9%93%BE%E4%B8%8B%E6%89%A9%E5%AE%B9%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3.html)

* 使用指南

```shell
make build  # 编译应用

./bin/oce node run  # oce deamon 启动

./bin/oce chan create -n node01 # 创建通道 -n 节点名称
./bin/oce chan join -n node02 # 加入通道 -n 节点名称

./bin/oce chan trade -a 10 -n node01 # 交易 -a 交易金额 -n 交易节点名称

# 快捷启动 4 节点
make four
之后，直接执行交易即可
```

* 目录结构
```shell
.
├── api             # client server API 相关
│   ├── path          # API 路径
│   └── types         # API 参数定义
├── bin             # 生成可执行文件路径
│   └── oce           # 可执行文件
├── client          # client 端相关定义
│   ├── sockets       # client 连接 server 所用 sockets 定义及方法
│   ├── tlsconfig     # client 连接 server tls 相关配置
│   ├── channel.go    # 业务相关，client 端通道相关操作封装
│   ├── client.go     # client 结构定义，以及创建方法
│   ├── errors.go     # client 端相关错误定义
│   ├── health.go     # client 端对服务端健康检查
│   ├── request.go    # client 端请求封装
│   ├── trade.go      # 业务相关，client 端交易相关操作封装
│   └── transport.go  # client 通信所用 transport
├── common          # 通用常量定义
│   └── string.go     # string 类型常量定义
├── config          # 配置文件相关
│   ├── config.go     # 配置文件对应结构定义
│   ├── config.yaml   # 配置文件
│   ├── config2.yaml  # 起 4 节点所用配置文件2
│   ├── config3.yaml  # 起 4 节点所用配置文件3
│   └── config4.yaml  # 起 4 节点所用配置文件4
├── go.mod
├── go.sum
├── main            # 命令行工具定义
│   ├── cmd           # 命令行子命令定义
│   └── main.go       # main.go 项目开始的地方
├── Makefile
├── module          # 各类模块定义
│   ├── crypto        # 链下通道密码学相关模块
│   ├── logger        # 日志模块
│   ├── network       # 链下通道网络模块
│   ├── node          # 链下通道节点模块
│   ├── noeq          # 唯一值模块
│   └── util.go       # 临时工具模块
├── README.md
├── server          # server 端相关定义
│   ├── controller    # server 端控制函数定义
│   ├── exit          # server 端优雅退出
│   ├── middleware    # server 端中间件
│   ├── router        # server 端路路由
│   └── server.go     # server 端启动位置
├── testdata       # 测试使用数据
│   ├── cert
│   └── pid
└── version        # 项目版本
    └── version.go    # 版本结构定义
```
