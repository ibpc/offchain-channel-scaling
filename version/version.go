/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package version

import "fmt"

var (
	// Name name
	Name string
	// Version project version
	Version string
	// BuildTime project build time
	BuildTime string
	// BuildHost project build host
	BuildHost string
	// GOVersion project build go version
	GOVersion string
	// GitBranch project git branch
	GitBranch string
	// GitLog project git log
	GitLog string
)

// GetVersionInfo get version info
func GetVersionInfo() string {
	return fmt.Sprintf("Version: %s Build Time: %s Build Host: %s Go Version: %s Git Branch: %s Git Log: %s",
		Version, BuildTime, BuildHost, GOVersion, GitBranch, GitLog)
}
