/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package server

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"chainmaker.org/ibpc/offchain-channel-scaling/config"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/contract"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/logger"
	"chainmaker.org/ibpc/offchain-channel-scaling/server/exit"
	"chainmaker.org/ibpc/offchain-channel-scaling/server/router"

	"go.uber.org/zap"
)

// Run server run
func Run() {
	// 初始化路由
	engine := router.Init()
	log := logger.GetLogger()

	var err error
	contract.Cli, err = contract.CreateChainClientWithSDKConf(config.GetAllConf().SdkConfFile)
	if err != nil {
		log.Error("Contract cli sdk failed", zap.Error(err))
		return
	}

	// 启动服务
	srv := &http.Server{Addr: config.GetServerConf().ListenAddr, Handler: engine}

	go func() {
		exit.Exit = make(chan os.Signal, 1)
		signal.Notify(exit.Exit, syscall.SIGINT, syscall.SIGTERM)
		<-exit.Exit

		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()
		// 执行 s.Shutdown 时，s.ListenAndServe 会立即结束
		err := srv.Shutdown(ctx)
		if err != nil {
			log.Error("s.Shutdown failed", zap.Error(err))
			return
		}
	}()

	log.Info("==== Start server at", zap.String("address:", config.GetServerConf().ListenAddr))
	// // 启动服务
	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		log.Sugar().Errorf("s.ListenAndServe failed, err=%v", err)
	}

	log.Info("==== Server is shutdown ====")
}
