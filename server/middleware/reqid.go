/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package middleware

import (
	"fmt"

	"chainmaker.org/ibpc/offchain-channel-scaling/common"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/noeq"

	"github.com/gin-gonic/gin"
)

// ReqID req-id 中间件，方便追踪日志
func ReqID() gin.HandlerFunc {
	return func(c *gin.Context) {
		requestID := c.Request.Header.Get(common.ReqID)
		if requestID == "" {
			id := noeq.GenID()
			requestID = fmt.Sprintf("%d", id)
			c.Request.Header.Set(common.ReqID, requestID)
		}
		c.Set(common.ReqID, requestID)
	}
}
