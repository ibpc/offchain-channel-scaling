/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package router

import (
	"chainmaker.org/ibpc/offchain-channel-scaling/api/path"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/logger"
	"chainmaker.org/ibpc/offchain-channel-scaling/server/controller"
	"chainmaker.org/ibpc/offchain-channel-scaling/server/middleware"

	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
)

// Init 初始化路由
func Init() (engine *gin.Engine) {
	engine = gin.New()
	engine.Use(gin.LoggerWithWriter(logger.GetLogWriter()), gin.RecoveryWithWriter(logger.GetLogWriter()))

	pprof.Register(engine, "/debug")

	router := engine.Group("/api", middleware.ReqID())
	v1 := router.Group("/v1")

	v1.GET(path.HealthCheck, controller.Health.Check)
	v1.GET(path.Shutdown, controller.Health.Shutdown)

	channel := v1.Group("/chan")
	{
		channel.POST("/create", controller.Chan.Create)
		channel.POST("/join", controller.Chan.Join)
		channel.POST("/exit", controller.Chan.Exit)
		channel.GET("/status", controller.Chan.Status)
		channel.GET("/balance", controller.Chan.Balance)
	}

	trade := v1.Group("/trade")
	{
		trade.POST("", controller.Trade.Trade)
	}

	return
}
