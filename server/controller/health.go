/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package controller

import (
	"syscall"

	"chainmaker.org/ibpc/offchain-channel-scaling/server/exit"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// Health health controller
var Health healthCtrl

type healthCtrl struct {
	Controller
}

// Check check oce deamon health
func (hc healthCtrl) Check(ctx *gin.Context) {
	err := hc.MakeContext(ctx, "[Check]").Errors
	if err != nil {
		hc.Logger.Error("Bind param failed.", zap.Error(err))
		hc.Error(500, err, err.Error())
		return
	}

	hc.OK(nil, "ok")
}

// Shutdown shutdown deamon
func (hc healthCtrl) Shutdown(ctx *gin.Context) {
	err := hc.MakeContext(ctx, "[Check]").Errors
	if err != nil {
		hc.Logger.Error("Bind param failed.", zap.Error(err))
		hc.Error(500, err, err.Error())
		return
	}

	hc.OK(nil, "ok")

	exit.Exit <- syscall.SIGINT
}
