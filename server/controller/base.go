/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package controller

import (
	"fmt"

	"chainmaker.org/ibpc/offchain-channel-scaling/common"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/logger"
	"chainmaker.org/ibpc/offchain-channel-scaling/server/controller/request"
	"chainmaker.org/ibpc/offchain-channel-scaling/server/controller/response"

	vd "github.com/bytedance/go-tagexpr/v2/validator"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"go.uber.org/zap"
)

// Controller server route controller
type Controller struct {
	Context *gin.Context
	Logger  *zap.Logger
	Errors  error
}

// AddError add error
func (ctrl *Controller) AddError(err error) {
	if ctrl.Errors == nil {
		ctrl.Errors = err
	} else if err != nil {
		ctrl.Logger.Sugar().Error(err)
		ctrl.Errors = fmt.Errorf("%v; %w", ctrl.Errors, err)
	}
}

// MakeContext 设置http上下文
func (ctrl *Controller) MakeContext(c *gin.Context, params ...string) *Controller {
	requestID := c.GetString(common.ReqID)
	ctrl.Context = c
	if len(params) == 0 {
		ctrl.Logger = logger.GetLogger().With(zap.String("req-id", requestID))
	} else {
		ctrl.Logger = logger.GetLogger().With(zap.String("func", params[0]), zap.String("req-id", requestID))
	}
	return ctrl
}

// Bind 参数校验
func (ctrl *Controller) Bind(d interface{}, bindings ...binding.Binding) *Controller {
	var err error
	if len(bindings) == 0 {
		bindings = request.Constructor.GetBindingForGin(d)
	}
	for i := range bindings {
		if bindings[i] == nil {
			err = ctrl.Context.ShouldBindUri(d)
		} else {
			err = ctrl.Context.ShouldBindWith(d, bindings[i])
		}
		if err != nil && err.Error() == "EOF" {
			ctrl.Logger.Warn("request body is not present anymorctrl. ")
			err = nil
			continue
		}
		if err != nil {
			ctrl.AddError(err)
			break
		}
	}
	if err1 := vd.Validate(d); err1 != nil {
		ctrl.AddError(err1)
	}
	return ctrl
}

// Error 通常错误数据处理
func (ctrl Controller) Error(code int, err error, msg string) {
	response.Error(ctrl.Context, code, err, msg)
}

// OK 通常成功数据处理
func (ctrl Controller) OK(data interface{}, msg string) {
	response.OK(ctrl.Context, data, msg)
}

// PageOK 分页数据处理
func (ctrl Controller) PageOK(result interface{}, count int, pageIndex int, pageSize int, msg string) {
	response.PageOK(ctrl.Context, result, count, pageIndex, pageSize, msg)
}

// Custom 兼容函数
func (ctrl Controller) Custom(data gin.H) {
	response.Custum(ctrl.Context, data)
}
