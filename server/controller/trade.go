/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package controller

import (
	"time"

	"chainmaker.org/ibpc/offchain-channel-scaling/api/types"
	"chainmaker.org/ibpc/offchain-channel-scaling/common"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/node"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

var (
	// Trade trade controller
	Trade tradeCtrl
)

type tradeCtrl struct {
	Controller
}

// Trade begin trade
func (cc tradeCtrl) Trade(ctx *gin.Context) {
	var (
		req types.Trade
	)
	err := cc.MakeContext(ctx, "[Trade]").Bind(&req).Errors
	if err != nil {
		cc.Logger.Error("Bind param failed.", zap.Error(err))
		cc.Error(500, err, err.Error())
		return
	}

	ins.RLock()
	existNode := ins.nodes[req.ChanName]
	ins.RUnlock()
	if existNode == nil {
		cc.Logger.Sugar().Errorf("Not create or join this channel, channel name is %s.", req.ChanName)
		cc.Error(500, nil, "Not create or join this channel")
		return
	}

	// 发起交易请求
	txId, err := existNode.SendTxRequest(req.Amount, req.NodeID)
	if err != nil {
		cc.Logger.Error("SendTxRequest failed.", zap.Error(err))
		cc.Error(500, err, err.Error())
		return
	}

	if req.Sync {
		msgChan := make(chan node.TradeMsg, 10)
		node.TradeMsgChan.AddChan(txId, msgChan)

		endTimer := time.NewTimer(time.Second * 5)

		select {
		case <-endTimer.C:
			endTimer.Stop()
			node.TradeMsgChan.DelChan(txId)
			cc.Logger.Error("Tx failed.", zap.Error(err))
			cc.Error(500, nil, "trade failed")
			return
		case msg := <-msgChan:
			endTimer.Stop()
			node.TradeMsgChan.DelChan(txId)
			if msg.Code == common.TradeMsgFail {
				cc.Logger.Error("Tx failed.", zap.String("error", msg.Msg))
				cc.Error(500, nil, msg.Msg)
				return
			}
		}
	}

	cc.OK(nil, "ok")
}
