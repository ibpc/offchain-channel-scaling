/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package controller

import (
	"sync"
	"time"

	"chainmaker.org/ibpc/offchain-channel-scaling/api/types"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/node"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

var (
	// Chan channel controller
	Chan chanCtrl
	ins  *instance
)

type instance struct {
	nodes map[string]*node.Node
	sync.RWMutex
}

func init() {
	ins = &instance{nodes: make(map[string]*node.Node)}
}

type chanCtrl struct {
	Controller
}

// Create create offchain channel
func (cc chanCtrl) Create(ctx *gin.Context) {
	var (
		req types.ChanCreate
	)
	err := cc.MakeContext(ctx, "[Create]").Bind(&req).Errors
	if err != nil {
		cc.Logger.Error("Bind param failed.", zap.Error(err))
		cc.Error(500, err, err.Error())
		return
	}

	ins.RLock()
	existNode := ins.nodes[req.ChanName]
	ins.RUnlock()
	if existNode != nil {
		cc.Logger.Sugar().Errorf("joined this channel, channel name is %s.", req.ChanName)
		cc.Error(500, nil, "joined this channel")
		return
	}

	// 则直接返回
	newNode, err := node.CreateNode(req.ChanName)
	if err != nil {
		cc.Logger.Error("CreateNode failed.", zap.Error(err))
		cc.Error(500, err, err.Error())
		return
	}

	err = newNode.CreateChannel(int(req.Balance))
	if err != nil {
		cc.Logger.Error("CreateChannel failed.", zap.Error(err))
		cc.Error(500, err, err.Error())
		return
	}

	newNode.Joined = true

	ins.Lock()
	defer ins.Unlock()
	ins.nodes[req.ChanName] = newNode

	cc.OK(newNode.Id, "OK")
}

// Join join offchain channel
func (cc chanCtrl) Join(ctx *gin.Context) {
	var (
		req types.ChanJoin
	)
	err := cc.MakeContext(ctx, "[Join]").Bind(&req).Errors
	if err != nil {
		cc.Logger.Error("Bind param failed.", zap.Error(err))
		cc.Error(500, err, err.Error())
		return
	}

	ins.RLock()
	existNode := ins.nodes[req.ChanName]
	ins.RUnlock()
	if existNode != nil {
		// 节点未加入通道，再次加入
		if !existNode.Joined {
			var isLeader bool
			isLeader, err = existNode.JoinChannel(int(req.Balance))
			if err != nil {
				cc.Logger.Error("JoinChannel failed.", zap.Error(err))
				cc.Error(500, err, err.Error())
				return
			}

			if !isLeader {
				existNode.SendJoinRequest()
			}

			cc.OK(nil, "ok")
			return
		}
		cc.Logger.Sugar().Errorf("joined this channel, channel name is %s.", req.ChanName)
		cc.OK(nil, "joined this channel")
		return
	}

	// 则直接返回
	newNode, err := node.CreateNode(req.ChanName)
	if err != nil {
		cc.Logger.Error("CreateNode failed.", zap.Error(err))
		cc.Error(500, err, err.Error())
		return
	}

	isLeader, err := newNode.JoinChannel(int(req.Balance))
	if err != nil {
		cc.Logger.Error("JoinChannel failed.", zap.Error(err))
		cc.Error(500, err, err.Error())
		return
	}

	if !isLeader {
		// 发送加入通道消息
		go func() {
			for {
				errSend := newNode.SendPing(newNode.LeaderID())
				if errSend == nil {
					newNode.SendJoinRequest()
					break
				}
				time.Sleep(time.Second)
			}
		}()
	}

	ins.Lock()
	defer ins.Unlock()
	ins.nodes[req.ChanName] = newNode

	cc.OK(newNode.Id, "OK")
}

// Exit exit offchain channel
func (cc chanCtrl) Exit(ctx *gin.Context) {
	var (
		req types.ChanExit
	)
	err := cc.MakeContext(ctx, "[Exit]").Bind(&req).Errors
	if err != nil {
		cc.Logger.Error("Bind param failed.", zap.Error(err))
		cc.Error(500, err, err.Error())
		return
	}

	ins.RLock()
	existNode := ins.nodes[req.ChanName]
	ins.RUnlock()
	if existNode == nil {
		cc.Logger.Sugar().Errorf("not joined this channel, channel name is %s.", req.ChanName)
		cc.Error(500, nil, "not joined this channel")
		return
	}

	if existNode.Exited {
		existNode.Joined = false
		cc.OK(gin.H{"code": 1, "balance": existNode.Balance, "msg": "exit success"}, "ok")
	}

	err = existNode.SendExitRequest()
	if err != nil {
		cc.Logger.Error("SendExitRequest failed.", zap.Error(err))
		cc.Error(500, err, err.Error())
		return
	}

	cc.OK(gin.H{"code": 0, "balance": existNode.Balance, "msg": "joined exit queue, please wait a minute"}, "ok")
}

// Balance node balance
func (cc chanCtrl) Balance(ctx *gin.Context) {
	var (
		req types.Status
	)
	err := cc.MakeContext(ctx, "[Balance]").Bind(&req).Errors
	if err != nil {
		cc.Logger.Error("Bind param failed.", zap.Error(err))
		cc.Error(500, err, err.Error())
		return
	}

	ins.RLock()
	existNode := ins.nodes[req.ChanName]
	ins.RUnlock()
	if existNode == nil {
		cc.Logger.Sugar().Errorf("Not create or join this channel, channel name is %s.", req.ChanName)
		cc.Error(500, nil, "Not create or join this channel")
		return
	}

	cc.OK(gin.H{"code": 0, "balance": existNode.Balance, "msg": ""}, "ok")
}

// Status node status
func (cc chanCtrl) Status(ctx *gin.Context) {
	var (
		req types.Status
	)
	err := cc.MakeContext(ctx, "[Status]").Bind(&req).Errors
	if err != nil {
		cc.Logger.Error("Bind param failed.", zap.Error(err))
		cc.Error(500, err, err.Error())
		return
	}

	ins.RLock()
	existNode := ins.nodes[req.ChanName]
	ins.RUnlock()
	if existNode == nil {
		cc.Logger.Sugar().Errorf("Not create or join this channel, channel name is %s.", req.ChanName)
		cc.Error(500, nil, "Not create or join this channel")
		return
	}

	existNode.PrintStatus()

	cc.OK(nil, "ok")
}
