/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package response

// Response server response
type Response struct {
	// 数据集
	RequestId string `protobuf:"bytes,1,opt,name=requestId,proto3" json:"requestId,omitempty"`
	Code      int32  `protobuf:"varint,2,opt,name=code,proto3" json:"code,omitempty"`
	Msg       string `protobuf:"bytes,3,opt,name=msg,proto3" json:"msg,omitempty"`
}

type response struct {
	Response
	Data interface{} `json:"data"`
}

// Page server api page
type Page struct {
	Count     int `json:"count"`
	PageIndex int `json:"pageIndex"`
	PageSize  int `json:"pageSize"`
}

type page struct {
	Page
	List interface{} `json:"list"`
}

// SetData set response data
func (e *response) SetData(data interface{}) {
	e.Data = data
}

// Clone clone response
func (e response) Clone() Responses {
	return &e
}

// SetTraceID set trace id
func (e *response) SetTraceID(id string) {
	e.RequestId = id
}

// SetMsg set response message
func (e *response) SetMsg(s string) {
	e.Msg = s
}

// SetCode set response code
func (e *response) SetCode(code int32) {
	e.Code = code
}
