/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package response

import (
	"fmt"

	"chainmaker.org/ibpc/offchain-channel-scaling/common"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/noeq"

	"github.com/gin-gonic/gin"
)

// GenerateMsgIDFromContext 生成msgID
func GenerateMsgIDFromContext(c *gin.Context) string {
	requestId := c.GetHeader(common.ReqID)
	if requestId == "" {
		id := noeq.GenID()
		requestId = fmt.Sprintf("%d", id)
		c.Header(common.ReqID, requestId)
	}
	return requestId
}
