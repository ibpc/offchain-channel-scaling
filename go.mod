module chainmaker.org/ibpc/offchain-channel-scaling

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.3.1-0.20221124040209-ba155eda3a31
	chainmaker.org/chainmaker/logger/v2 v2.2.1
	chainmaker.org/chainmaker/net-libp2p v1.1.2
	chainmaker.org/chainmaker/pb-go/v2 v2.3.0
	chainmaker.org/chainmaker/protocol/v2 v2.3.0
	github.com/Microsoft/go-winio v0.5.2
	github.com/agiledragon/gomonkey/v2 v2.2.0
	github.com/btcsuite/btcd v0.21.0-beta
	github.com/bytedance/go-tagexpr/v2 v2.9.3
	github.com/common-nighthawk/go-figure v0.0.0-20210622060536-734e95fb86be
	github.com/gin-contrib/pprof v1.4.0
	github.com/gin-gonic/gin v1.8.1
	github.com/google/uuid v1.3.0
	github.com/pkg/errors v0.9.1
	github.com/smartystreets/goconvey v1.7.2
	github.com/spf13/cobra v1.4.0
	github.com/spf13/viper v1.12.0
	github.com/stretchr/testify v1.7.5
	github.com/tjfoc/gmsm v1.4.1
	go.uber.org/zap v1.21.0
	golang.org/x/net v0.0.0-20221014081412-f15817d10f9b
	google.golang.org/grpc v1.50.1
	google.golang.org/protobuf v1.28.1
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)

require (
	chainmaker.org/chainmaker/sdk-go/v2 v2.3.1
	cloud.google.com/go/iam v0.7.0 // indirect
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/hashicorp/golang-lru v0.5.5-0.20210104140557-80c98217689d // indirect
	github.com/huin/goupnp v1.0.1-0.20210310174557-0ca763054c88 // indirect
	github.com/smartystreets/assertions v1.13.0 // indirect
)

replace github.com/libp2p/go-libp2p-core => chainmaker.org/chainmaker/libp2p-core v1.0.0
