/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package config

import (
	"fmt"
	"log"

	"chainmaker.org/ibpc/offchain-channel-scaling/module/logger"

	"github.com/spf13/viper"
)

var (
	// Dev 判断是否开发状态
	Dev bool
	// ConfPath 配置文件位置
	ConfPath string

	allConf *AllConfig
)

// AllConfig 所有配置对应的结构体
type AllConfig struct {
	ChainId      string            `mapstructure:"chain_id"`
	AuthType     string            `mapstructure:"auth_type"`
	ContractMock string            `mapstructure:"contract_mock"`
	ContractName string            `mapstructure:"contract_name"`
	SdkConfFile  string            `mapstructure:"sdk_conf_file"`
	UserConfig   *UserConfig       `mapstructure:"user"`
	LogConfig    *logger.LogConifg `mapstructure:"log"`
	NetConfig    *NetConfig        `mapstructure:"net"`
	ServerConfig *ServerConfig     `mapstructure:"server"`
}

// NetConfig 网络配置
type NetConfig struct {
	Provider                string       `mapstructure:"provider"`
	ListenAddr              string       `mapstructure:"listen_addr"`
	PeerStreamPoolSize      int          `mapstructure:"peer_stream_pool_size"`
	MaxPeerCountAllow       int          `mapstructure:"max_peer_count_allow"`
	PeerEliminationStrategy int          `mapstructure:"peer_elimination_strategy"`
	CaPath                  []string     `mapstructure:"ca"`
	Seeds                   []string     `mapstructure:"seeds"`
	TLSConfig               netTlsConfig `mapstructure:"tls"`
}

// UserConfig 用户配置
type UserConfig struct {
	PrivKeyFile string `mapstructure:"sign_priv_key_file"`
	CertFile    string `mapstructure:"sign_cert_file"`
	Hash        string `mapstructure:"hash"`
}

// ServerConfig 服务配置
type ServerConfig struct {
	ListenAddr string `mapstructure:"listen_addr"`
}

type netTlsConfig struct {
	Enable      string `mapstructure:"enable"`
	PrivKeyFile string `mapstructure:"priv_key_file"`
	CertFile    string `mapstructure:"cert_file"`
}

// InitConfig 初始化配置
func InitConfig() {
	viper.SetConfigFile(ConfPath)
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("read config failed, err=%v\n", err)
		return
	}

	allConf, err = ParseAllConf()
	if err != nil {
		log.Fatalf("parse config failed, err=%v\n", err)
		return
	}

	err = logger.InitLogger(GetLogConf())
	if err != nil {
		log.Fatalf("init logger failed, err=%v\n", err)
		return
	}
}

// ParseAllConf parse all conf
func ParseAllConf() (*AllConfig, error) {
	var allConf AllConfig
	err := viper.Unmarshal(&allConf)
	if err != nil {
		return nil, fmt.Errorf("get all config failed: %s", err.Error())
	}
	if allConf.UserConfig == nil {
		return nil, fmt.Errorf("get all config failed: not found user config")
	}
	if allConf.LogConfig == nil {
		return nil, fmt.Errorf("get all config failed: not found log config")
	}
	if allConf.ServerConfig == nil {
		return nil, fmt.Errorf("get all config failed: not found server config")
	}
	if allConf.NetConfig == nil {
		return nil, fmt.Errorf("get all config failed: not found net config")
	}
	return &allConf, nil
}

// GetAllConf Get all conf
func GetAllConf() *AllConfig {
	return allConf
}

// GetUserConf Get user conf
func GetUserConf() *UserConfig {
	return allConf.UserConfig
}

// GetLogConf Get log conf
func GetLogConf() *logger.LogConifg {
	return allConf.LogConfig
}

// GetNetConf Get net conf
func GetNetConf() *NetConfig {
	return allConf.NetConfig
}

// GetServerConf Get server conf
func GetServerConf() *ServerConfig {
	return allConf.ServerConfig
}
