/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package config

import (
	"testing"
)

func TestInitConfig(t *testing.T) {
	ConfPath = "./config.yaml"
	InitConfig()
	_, err := ParseAllConf()
	if err != nil {
		t.Fatalf("ParseAllConf failed, err=%v\n", err)
	}
}
