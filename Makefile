GOPATH:=$(shell go env GOPATH)
ROOT_DIR = $(CURDIR)
BIN_DIR = $(ROOT_DIR)/bin
LINUX_DIR = $(BIN_DIR)/linux

REPO_NAME = chainmaker.org/ibpc/offchain-channel-scaling

CURR_TIME = $(shell date "+%Y%m%d-%H%M%S")
HOST_NAME = $(shell hostname)
GO_VERSION = $(shell go version)
GIT_LOG = $(shell git log -1 | sed s/\'/\"/g)
GIT_BRANCH = $(shell git rev-parse --abbrev-ref HEAD)

APP_VERSION = 1.0.0

APP_NAME = oce
DEPLOY_CONTRACTNAME = dpy

LDFLAGS = "-w -s -X '$(REPO_NAME)/version.Version=${APP_VERSION}' \
-w -s -X '$(REPO_NAME)/version.BuildTime=${CURR_TIME}' \
-w -s -X '$(REPO_NAME)/version.BuildHost=${HOST_NAME}' \
-w -s -X '$(REPO_NAME)/version.GOVersion=${GO_VERSION}' \
-w -s -X '$(REPO_NAME)/version.GitLog=${GIT_LOG}' \
-w -s -X '$(REPO_NAME)/version.GitBranch=${GIT_BRANCH}'"

printversion:
	@echo "================================================"
	@echo "server       version: "$(APP_VERSION)
	@echo "================================================"

build: printversion
	@mkdir -p bin
	@cd $(BIN_DIR) && go build -o $(APP_NAME) -ldflags $(LDFLAGS) $(REPO_NAME)/main

deploy_contract: 
	go build -o $(DEPLOY_CONTRACTNAME) $(REPO_NAME)/assist && ./$(DEPLOY_CONTRACTNAME)

clear_log:
	@rm log/* || echo "log is clear"  && echo "" > nohup.out
	@rm sdk.log* || echo "sdk.log is clear"
	@rm module/contract/sdk.* || echo "module sdk.log is clear"

cover_node:
	@go test -coverprofile=coverprofile -gcflags=all=-l ./module/node/ ./module/network/ ./module/crypto/ ./module/noeq/
	@go tool cover -html=coverprofile -o coverfile.html

four:
	# @nohup ./bin/ctatmock &
	# @sleep 1
	@./bin/oce node run
	@./bin/oce node run -c config/config2.yaml 
	@./bin/oce node run -c config/config3.yaml
	@./bin/oce node run -c config/config4.yaml
	@sleep 2
	@./bin/oce chan create -b 300000 -n chan1
	@sleep 1
	@./bin/oce chan join -c config/config2.yaml -b 300000 -n chan1
	@./bin/oce chan join -c config/config3.yaml -b 300000 -n chan1
	@./bin/oce chan join -c config/config4.yaml -b 300000 -n chan1

show-four:
	@ps -ef | grep "./bin/oce node run" | grep -v "grep" || echo "not find oce deamon"

stop-four:
	@ps -ef | grep "./bin/oce node run" | grep -v "grep" | awk '{print $$2}' | xargs kill

exit4:
	./bin/oce chan exit -n chan1 -c config/config4.yaml

join4:
	./bin/oce chan join -c config/config4.yaml -b 1000 -n chan1

balance1:
	./bin/oce chan balance -n chan1 -c config/config.yaml

trade1-3:
	./bin/oce chan trade -a 10 -n chan1 -i QmSbUfSLtQavZQ4CmfNsrgzXw3iyHPSEbVQhngRpGwd9vw -c config/config.yaml -s

trade1-2:
	./bin/oce chan trade -a 10 -n chan1 -i QmfHFdpTN1RCLvvqpivwF5KwDTefaGMNezqxw7MoZRzmjh -c config/config.yaml -s

trade2-1:
	./bin/oce chan trade -a 10 -n chan1 -i QmUtQmnGVyvmBcKHqbmywiHJwU5p5CrbsVynKuRSocKrDs -c config/config2.yaml -s

trade3-2:
	./bin/oce chan trade -a 10 -n chan1 -i QmfHFdpTN1RCLvvqpivwF5KwDTefaGMNezqxw7MoZRzmjh -c config/config3.yaml -s

trade1-4:
	./bin/oce chan trade -a 10 -n chan1 -i QmTJLZjed7mnAyCdLhPeEvaMgvsH3Hf1GgdN8zwsZ2Xz2P -c config/config.yaml -s

trade2-4:
	./bin/oce chan trade -a 10 -n chan1 -i QmTJLZjed7mnAyCdLhPeEvaMgvsH3Hf1GgdN8zwsZ2Xz2P -c config/config2.yaml -s

qta:
	echo "clear environment"
	cd testdata/test && ./stop_four.sh
	cd testdata/test && ./clear_log.sh
	echo "start new qta test"
	cd testdata/test && ./build.sh
	cd testdata/test && ./start_four.sh
	sleep 10
	cd testdata/test && ./trade.sh
	cd testdata/test && ./stop_four.sh
	cd testdata/test && ./clear_log.sh
