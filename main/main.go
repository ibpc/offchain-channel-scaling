/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"fmt"

	"chainmaker.org/ibpc/offchain-channel-scaling/main/cmd"
	"github.com/spf13/cobra"
)

func main() {
	// oce => offchain expansion
	mainCmd := &cobra.Command{
		Use:   "oce",
		Short: "ChainMaker off-chain-expansion cli",
		Long:  `ChainMaker block chain off-chain-expansion cli`,
	}

	mainCmd.AddCommand(cmd.NodeCMD())
	mainCmd.AddCommand(cmd.ChanCMD())
	mainCmd.AddCommand(cmd.VersionCMD())

	err := mainCmd.Execute()
	if err != nil {
		fmt.Println(err)
	}
}
