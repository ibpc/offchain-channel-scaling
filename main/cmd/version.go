/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package cmd

import (
	"fmt"
	"strings"

	"chainmaker.org/ibpc/offchain-channel-scaling/version"
	"github.com/common-nighthawk/go-figure"
	"github.com/spf13/cobra"
)

// VersionCMD version cmd
func VersionCMD() *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: "Show offchain expansion version",
		Long:  "Show offchain expansion version",
		RunE: func(cmd *cobra.Command, _ []string) error {
			PrintVersion()
			return nil
		},
	}
}

// PrintVersion print version info
func PrintVersion() {
	figCM := figure.NewFigure("ChainMaker", "slant", true)
	strCM := figCM.String()
	figOCE := figure.NewFigure("OCE Cli", "slant", true)
	strOCE := figOCE.String()
	fragment := "================================================================================="
	strBuilder := new(strings.Builder)
	strBuilder.WriteString(fmt.Sprintf("version: %s\n", version.Version))
	strBuilder.WriteString(fmt.Sprintf("build time: %s\n", version.BuildTime))
	strBuilder.WriteString(fmt.Sprintf("go version: %s\n", version.GOVersion))
	strBuilder.WriteString(fmt.Sprintf("git branch: %v\n", version.GitBranch))
	strBuilder.WriteString(fmt.Sprintf("git log: %vs\n", version.GitLog))
	versionInfo := strBuilder.String()

	fmt.Printf("\n%s\n%s%s%s\n%s\n", fragment, strCM, strOCE, fragment, versionInfo)
}
