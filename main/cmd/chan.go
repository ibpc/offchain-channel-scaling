/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package cmd

import (
	"context"
	"fmt"
	"net/http"

	"chainmaker.org/ibpc/offchain-channel-scaling/api/types"
	"chainmaker.org/ibpc/offchain-channel-scaling/client"
	"chainmaker.org/ibpc/offchain-channel-scaling/config"
	"github.com/spf13/cobra"
)

// ChanCMD create chainmaker offchain-expansion channel
func ChanCMD() *cobra.Command {
	chanCmd := &cobra.Command{
		Use:   "chan",
		Short: "chan command",
		Long:  `chan command`,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) (err error) {
			config.InitConfig()
			cli, err = client.NewClient(config.GetServerConf().ListenAddr, http.DefaultClient, nil)
			if err != nil {
				fmt.Printf("NewClient failed, err=%v\n", err)
				return
			}
			return
		},
	}

	chanCmd.AddCommand(createCMD())
	chanCmd.AddCommand(joinCMD())
	chanCmd.AddCommand(tradeCMD())
	chanCmd.AddCommand(exitCMD())
	chanCmd.AddCommand(balanceCMD())
	chanCmd.AddCommand(statusCMD())

	chanCmd.PersistentFlags().StringVarP(&config.ConfPath, "conf_path", "c", "config/config.yaml", "config file path")

	return chanCmd
}

// createCMD create chainmaker offchain-expansion channel
func createCMD() *cobra.Command {
	var chanName string
	var balance int
	chanCmd := &cobra.Command{
		Use:   "create",
		Short: "create channel",
		Long:  `create chainmaker offchain-expansion channel`,
		Run: func(cmd *cobra.Command, args []string) {
			var req = types.ChanCreate{
				ChanName: chanName,
				Balance:  int64(balance),
			}

			nodeID, err := cli.ChannelCreate(context.Background(), &req)
			if err != nil {
				fmt.Printf("channel create failed, err=%v\n", err)
				return
			}
			fmt.Printf("channel create success, nodeID: %s\n", nodeID)
		},
	}

	chanCmd.Flags().StringVarP(&chanName, "chan_name", "n", "chan1", "channel name")
	chanCmd.Flags().IntVarP(&balance, "balance", "b", 1000, "user balance")

	return chanCmd
}

// joinCMD join chainmaker offchain-expansion channel
func joinCMD() *cobra.Command {
	var chanName string
	var balance int
	joinCmd := &cobra.Command{
		Use:   "join",
		Short: "join channel",
		Long:  `join chainmaker offchain-expansion channel`,
		Run: func(cmd *cobra.Command, args []string) {
			var req = types.ChanJoin{
				ChanName: chanName,
				Balance:  int64(balance),
			}

			nodeID, err := cli.ChannelJoin(context.Background(), &req)
			if err != nil {
				fmt.Printf("channel join failed, err=%v\n", err)
				return
			}
			fmt.Printf("channel join success, nodeID: %s\n", nodeID)
		},
	}

	joinCmd.Flags().StringVarP(&chanName, "chan_name", "n", "chan1", "channel name")
	joinCmd.Flags().IntVarP(&balance, "balance", "b", 1000, "user balance")

	return joinCmd
}

// tradeCMD  chainmakeroffchain-expansion channel trade
func tradeCMD() *cobra.Command {
	var (
		chanName   string
		nodeID     string
		amount     int
		syncResult bool
	)
	tradeCmd := &cobra.Command{
		Use:   "trade",
		Short: "trade command",
		Long:  `trade command`,
		Run: func(cmd *cobra.Command, args []string) {
			var req = types.Trade{
				ChanName: chanName,
				Amount:   amount,
				NodeID:   nodeID,
				Sync:     syncResult,
			}

			err := cli.Trade(context.Background(), &req)
			if err != nil {
				fmt.Printf("trade failed, err=%v\n", err)
				return
			}
			fmt.Println("trade success")
		},
	}

	tradeCmd.Flags().StringVarP(&chanName, "chan_name", "n", "chan1", "channel name")
	tradeCmd.Flags().StringVarP(&nodeID, "node_id", "i", "node01", "node id")
	tradeCmd.Flags().IntVarP(&amount, "amount", "a", 0, "trade amount")
	tradeCmd.Flags().BoolVarP(&syncResult, "sync", "s", false, "sync get trade result")

	return tradeCmd
}

// statusCMD  chainmakeroffchain-expansion channel status
func statusCMD() *cobra.Command {
	var (
		chanName string
	)
	tradeCmd := &cobra.Command{
		Use:   "status",
		Short: "status command",
		Long:  `status command`,
		Run: func(cmd *cobra.Command, args []string) {
			var req = types.Status{
				ChanName: chanName,
			}

			err := cli.Status(context.Background(), &req)
			if err != nil {
				fmt.Printf("print status failed, err=%v\n", err)
				return
			}
			fmt.Println("print status success")
		},
	}

	tradeCmd.Flags().StringVarP(&chanName, "chan_name", "n", "chan1", "channel name")

	return tradeCmd
}

// exitCMD exit chainmakeroffchain-expansion channel
func exitCMD() *cobra.Command {
	var (
		chanName string
	)
	exitCmd := &cobra.Command{
		Use:   "exit",
		Short: "trade command",
		Long:  `trade command`,
		Run: func(cmd *cobra.Command, args []string) {
			var req = types.ChanExit{
				ChanName: chanName,
			}

			resp, err := cli.ChannelExit(context.Background(), &req)
			if err != nil {
				fmt.Printf("exit channel failed, err=%v\n", err)
				return
			}
			if resp.Code == 1 {
				fmt.Printf("exit channel success, balance is %d\n", resp.Balance)
				return
			}
			fmt.Printf("%s\n", resp.Msg)
		},
	}

	exitCmd.Flags().StringVarP(&chanName, "chan_name", "n", "chan1", "channel name")

	return exitCmd
}

// balanceCMD get balance of channel
func balanceCMD() *cobra.Command {
	var (
		chanName string
	)
	exitCmd := &cobra.Command{
		Use:   "balance",
		Short: "trade command",
		Long:  `trade command`,
		Run: func(cmd *cobra.Command, args []string) {
			var req = types.Status{
				ChanName: chanName,
			}

			resp, err := cli.Balance(context.Background(), &req)
			if err != nil {
				fmt.Printf("get balance failed, err=%v\n", err)
				return
			}
			if resp.Code == 0 {
				fmt.Printf("get balance success, balance is %d\n", resp.Balance)
				return
			}
			fmt.Printf("%s\n", resp.Msg)
		},
	}

	exitCmd.Flags().StringVarP(&chanName, "chan_name", "n", "chan1", "channel name")

	return exitCmd
}
