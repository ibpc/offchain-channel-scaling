/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package cmd

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/exec"

	"chainmaker.org/ibpc/offchain-channel-scaling/client"
	"chainmaker.org/ibpc/offchain-channel-scaling/config"
	"chainmaker.org/ibpc/offchain-channel-scaling/server"
	"github.com/spf13/cobra"
)

var cli *client.Client

// NodeCMD create chainmaker offchain-expansion channel
func NodeCMD() *cobra.Command {
	nodeCmd := &cobra.Command{
		Use:   "node",
		Short: "node command",
		Long:  `node command`,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) (err error) {
			config.InitConfig()
			cli, err = client.NewClient(config.GetServerConf().ListenAddr, http.DefaultClient, nil)
			if err != nil {
				fmt.Printf("NewClient failed, err=%v\n", err)
				return
			}
			return
		},
	}

	nodeCmd.AddCommand(nodeRunCMD())
	nodeCmd.AddCommand(nodeStopCMD())
	nodeCmd.AddCommand(nodeCheckCMD())

	nodeCmd.PersistentFlags().StringVarP(&config.ConfPath, "conf_path", "c", "config/config.yaml", "config file path")

	return nodeCmd
}

// nodeRunCMD create chainmaker offchain-expansion channel
func nodeRunCMD() *cobra.Command {
	var consoleRun bool
	nodeRunCmd := &cobra.Command{
		Use:   "run",
		Short: "node run",
		Long:  `offchain-expansion node run`,
		Run: func(cmd *cobra.Command, args []string) {
			_, err := cli.Health(context.Background())
			if err == nil {
				fmt.Printf("oce deamon running\n")
				return
			}

			if !consoleRun {
				args := os.Args[1:3]
				args = append(args, "-c", config.ConfPath, "-f")
				cmd := exec.Command(os.Args[0], args...)
				err = cmd.Start() // 开始执行新进程，不等待新进程退出
				if err != nil {
					fmt.Printf("oce deamon run failed, err=%v\n", err)
					return
				}
				return
			}

			server.Run()
		},
	}

	// nodeRunCmd.Flags().StringVarP(&config.ConfPath, "conf_path", "c", "config/config.yaml", "config file path")
	nodeRunCmd.Flags().BoolVarP(&consoleRun, "front", "f", false, "front run")

	return nodeRunCmd
}

// nodeRunCMD create chainmaker offchain-expansion channel
func nodeStopCMD() *cobra.Command {
	nodeStopCmd := &cobra.Command{
		Use:   "stop",
		Short: "node stop",
		Long:  `offchain-expansion node stop`,
		Run: func(cmd *cobra.Command, args []string) {
			err := cli.Shutdown(context.Background())
			if err != nil {
				fmt.Printf("deamon shutdown failed, err=%v\n", err)
				return
			}
			fmt.Println("oce deamon is shutdown")
		},
	}

	return nodeStopCmd
}

// nodeCheckCMD check chainmaker offchain-expansion channel
func nodeCheckCMD() *cobra.Command {
	nodeCheckCmd := &cobra.Command{
		Use:   "check",
		Short: "node check",
		Long:  `offchain-expansion node check`,
		Run: func(cmd *cobra.Command, args []string) {
			cli, err := client.NewClient(config.GetServerConf().ListenAddr, http.DefaultClient, nil)
			if err != nil {
				fmt.Printf("NewClient failed, err=%v\n", err)
				return
			}

			_, err = cli.Health(context.Background())
			if err != nil {
				fmt.Printf("cli.Health failed, err=%v\n", err)
				return
			}
			fmt.Println("oce deamon is health")
		},
	}

	nodeCheckCmd.Flags().StringVarP(&config.ConfPath, "conf_path", "c", "config/config.yaml", "config file path")

	return nodeCheckCmd
}
