#
# Copyright (C) BABEC. All rights reserved.
# Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
# and Privacy Computing. All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

cd ../..
make stop-four
