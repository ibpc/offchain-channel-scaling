/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package client

import (
	"context"
	"encoding/json"

	"chainmaker.org/ibpc/offchain-channel-scaling/api/types"
)

// Trade trade
func (cli *Client) Trade(ctx context.Context, trade *types.Trade) error {
	resp, err := cli.post(ctx, "/trade", nil, trade, nil)
	if err != nil {
		return err
	}
	defer ensureReaderClosed(resp)

	var commResp commonResponse
	err = json.NewDecoder(resp.body).Decode(&commResp)
	if err != nil {
		return err
	}
	err = cli.checkRespDataErr(&commResp)
	return err
}
