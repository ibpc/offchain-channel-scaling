/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package client

import (
	"context"
	"encoding/json"
	"net/url"

	"chainmaker.org/ibpc/offchain-channel-scaling/api/types"
)

// ChannelCreate create channel
func (cli *Client) ChannelCreate(ctx context.Context, req *types.ChanCreate) (nodeID string, err error) {
	resp, err := cli.post(ctx, "/chan/create", nil, req, nil)
	if err != nil {
		return
	}
	defer ensureReaderClosed(resp)

	var commResp struct {
		commonResponse
		Data string `json:"data"`
	}
	err = json.NewDecoder(resp.body).Decode(&commResp)
	if err != nil {
		return
	}
	err = cli.checkRespDataErr(&commResp.commonResponse)
	if err != nil {
		return
	}
	nodeID = commResp.Data
	return
}

// ChannelJoin join channel
func (cli *Client) ChannelJoin(ctx context.Context, req *types.ChanJoin) (nodeID string, err error) {
	resp, err := cli.post(ctx, "/chan/join", nil, req, nil)
	if err != nil {
		return
	}
	defer ensureReaderClosed(resp)

	var commResp struct {
		commonResponse
		Data string `json:"data"`
	}
	err = json.NewDecoder(resp.body).Decode(&commResp)
	if err != nil {
		return
	}
	err = cli.checkRespDataErr(&commResp.commonResponse)
	if err != nil {
		return
	}
	nodeID = commResp.Data
	return
}

// ChannelExit exit channel
func (cli *Client) ChannelExit(ctx context.Context, req *types.ChanExit) (exitResp *types.ChanExitResp, err error) {
	resp, err := cli.post(ctx, "/chan/exit", nil, req, nil)
	if err != nil {
		return
	}
	defer ensureReaderClosed(resp)

	var commResp struct {
		commonResponse
		Data types.ChanExitResp `json:"data"`
	}
	err = json.NewDecoder(resp.body).Decode(&commResp)
	if err != nil {
		return
	}
	err = cli.checkRespDataErr(&commResp.commonResponse)
	if err != nil {
		return
	}
	exitResp = &commResp.Data
	return
}

// Balance balance of channel
func (cli *Client) Balance(ctx context.Context, req *types.Status) (exitResp *types.ChanExitResp, err error) {
	query := url.Values{}
	query.Set("chan_name", req.ChanName)
	resp, err := cli.get(ctx, "/chan/balance", query, nil)
	if err != nil {
		return
	}
	defer ensureReaderClosed(resp)

	var commResp struct {
		commonResponse
		Data types.ChanExitResp `json:"data"`
	}
	err = json.NewDecoder(resp.body).Decode(&commResp)
	if err != nil {
		return
	}
	err = cli.checkRespDataErr(&commResp.commonResponse)
	if err != nil {
		return
	}
	exitResp = &commResp.Data
	return
}

// Status status
func (cli *Client) Status(ctx context.Context, status *types.Status) error {
	query := url.Values{}
	query.Set("chan_name", status.ChanName)
	resp, err := cli.get(ctx, "/chan/status", query, nil)
	if err != nil {
		return err
	}
	defer ensureReaderClosed(resp)

	var commResp commonResponse
	err = json.NewDecoder(resp.body).Decode(&commResp)
	if err != nil {
		return err
	}
	err = cli.checkRespDataErr(&commResp)
	return err
}
