/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package client

import (
	"fmt"
	"net/http"
	"net/url"
	"path"
	"strings"

	"chainmaker.org/ibpc/offchain-channel-scaling/client/sockets"
)

// DefaultOceHost default oce server host
const DefaultOceHost = "http://127.0.0.1:12309/api/v1"

const (
	// HTTP string
	HTTP = "http"
	// HTTPS string
	HTTPS = "https"
)

// Client is the API client that performs all operations
// against a oce server.
type Client struct {
	// scheme sets the scheme for the client
	scheme string
	// host holds the server address to connect to
	host string
	// proto holds the client protocol i.e. unix.
	proto string
	// addr holds the client address.
	addr string
	// basePath holds the path to prepend to the requests.
	basePath string
	// client used to send and receive http requests.
	client *http.Client
	// custom http headers configured by users.
	customHTTPHeaders map[string]string
}

// NewClient initializes a new API client for the given host.
// It uses the given http client as transport.
// It also initializes the custom http headers to add to each request.
func NewClient(host string, client *http.Client, httpHeaders map[string]string) (*Client, error) {
	hostURL, err := ParseHostURL(host)
	if err != nil {
		return nil, err
	}

	if client == nil {
		transport := new(http.Transport)
		err = sockets.ConfigureTransport(transport, hostURL.Scheme, hostURL.Host)
		if err != nil {
			return nil, err
		}
		client = &http.Client{
			Transport: transport,
		}
	}

	scheme := HTTP
	tlsConfig := resolveTLSConfig(client.Transport)
	if tlsConfig != nil {
		scheme = HTTPS
	}

	return &Client{
		scheme:            scheme,
		host:              host,
		proto:             hostURL.Scheme,
		addr:              hostURL.Host,
		basePath:          hostURL.Path,
		client:            client,
		customHTTPHeaders: httpHeaders,
	}, nil
}

// Close the transport used by the client
func (cli *Client) Close() error {
	if t, ok := cli.client.Transport.(*http.Transport); ok {
		t.CloseIdleConnections()
	}
	return nil
}

// ParseHostURL parses a url string, validates the string is a host url, and
// returns the parsed URL
func ParseHostURL(host string) (*url.URL, error) {
	if host == "" {
		return nil, fmt.Errorf("unable to parse oce host `%s`", host)
	}
	if !strings.Contains(host, "://") {
		host = fmt.Sprintf("http://%s", host)
	}
	protoAddrParts := strings.SplitN(host, "://", 2)
	if len(protoAddrParts) == 1 {
		return nil, fmt.Errorf("unable to parse oce host `%s`", host)
	}

	var basePath string
	proto, addr := protoAddrParts[0], protoAddrParts[1]
	if proto == "tcp" || proto == HTTP || proto == HTTPS {
		parsed, err := url.Parse(host)
		if err != nil {
			return nil, err
		}
		addr = parsed.Host
		basePath = parsed.Path
	}
	if basePath == "" {
		basePath = "/api/v1"
	}
	return &url.URL{
		Scheme: proto,
		Host:   addr,
		Path:   basePath,
	}, nil
}

// getAPIPath returns the versioned request path to call the api.
// It appends the query parameters to the path if they are not empty.
func (cli *Client) getAPIPath(p string, query url.Values) string {
	apiPath := path.Join(cli.basePath, p)
	if len(query) != 0 {
		return (&url.URL{Path: apiPath, RawQuery: query.Encode()}).String()
	}
	return (&url.URL{Path: apiPath}).String()
}
