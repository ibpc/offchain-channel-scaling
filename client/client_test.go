/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package client

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetAPIPath(t *testing.T) {
	testcases := []struct {
		path     string
		query    url.Values
		expected string
	}{
		{"/containers/json", nil, "/containers/json"},
		{"/containers/json", url.Values{}, "/containers/json"},
		{"/containers/json", url.Values{"s": []string{"c"}}, "/containers/json?s=c"},
	}

	for _, testcase := range testcases {
		c := Client{basePath: "/"}
		actual := c.getAPIPath(testcase.path, testcase.query)
		assert.Equal(t, actual, testcase.expected)
	}
}

func TestParseHostURL(t *testing.T) {
	testcases := []struct {
		host        string
		expected    *url.URL
		expectedErr string
	}{
		{
			host:        "",
			expectedErr: "unable to parse oce host",
		},
		{
			host:     "foobar",
			expected: &url.URL{Scheme: "http", Host: "foobar", Path: "/api/v1"},
		},
		{
			host:     "foo://bar",
			expected: &url.URL{Scheme: "foo", Host: "bar", Path: "/api/v1"},
		},
		{
			host:     "tcp://localhost:2476",
			expected: &url.URL{Scheme: "tcp", Host: "localhost:2476", Path: "/api/v1"},
		},
		{
			host:     "tcp://localhost:2476/path",
			expected: &url.URL{Scheme: "tcp", Host: "localhost:2476", Path: "/path"},
		},
	}

	for _, testcase := range testcases {
		actual, err := ParseHostURL(testcase.host)
		if testcase.expectedErr != "" {
			assert.ErrorContains(t, err, testcase.expectedErr)
		}
		assert.Equal(t, testcase.expected, actual)
	}
}
