/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package client

import (
	"context"
	"encoding/json"

	"chainmaker.org/ibpc/offchain-channel-scaling/api/path"
	"chainmaker.org/ibpc/offchain-channel-scaling/api/types"
)

// HealthResponse 健康检查返回结果定义
type HealthResponse struct {
	commonResponse
	Data types.Health `json:"data"`
}

// Health check oce deamon health
func (cli *Client) Health(ctx context.Context) (*types.Health, error) {
	resp, err := cli.get(ctx, path.HealthCheck, nil, nil)
	if err != nil {
		return nil, err
	}
	defer ensureReaderClosed(resp)

	var healthResponse HealthResponse
	err = json.NewDecoder(resp.body).Decode(&healthResponse)
	if err != nil {
		return nil, err
	}
	err = cli.checkRespDataErr(&healthResponse.commonResponse)
	return &healthResponse.Data, err
}

// Shutdown shutdown oce deamon
func (cli *Client) Shutdown(ctx context.Context) error {
	resp, err := cli.get(ctx, path.Shutdown, nil, nil)
	if err != nil {
		return err
	}
	defer ensureReaderClosed(resp)

	var healthResponse HealthResponse
	err = json.NewDecoder(resp.body).Decode(&healthResponse)
	if err != nil {
		return err
	}
	err = cli.checkRespDataErr(&healthResponse.commonResponse)
	return err
}
