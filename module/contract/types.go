/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package contract

// JoinChannelResp join channel resp type
type JoinChannelResp struct {
	LeaderId   string `json:"leader_id"`
	LeaderAddr string `json:"leader_address"`
	Epoch      int    `json:"epoch"`
}

// ChannelInfoResp channel info resp type
type ChannelInfoResp struct {
	LeaderId        string         `json:"leader_id"`
	LeaderAddr      string         `json:"leader_address"`
	Epoch           int            `json:"epoch"`
	Status          int            `json:"status"`
	ChalleageStatus int            `json:"challeage_status"`
	BalanceSet      map[string]int `json:"balance"`
}
