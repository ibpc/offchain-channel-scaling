/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package contract

import (
	"encoding/json"
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
)

const (
	innerMethod           = "invoke_contract"
	createContractTimeout = 5
)

// Cli chainmaker sdk chain client
var Cli *sdk.ChainClient

// CreateChainClientWithSDKConf create a chain client with sdk config file path
func CreateChainClientWithSDKConf(sdkConfPath string) (*sdk.ChainClient, error) {
	cc, err := sdk.NewChainClient(
		sdk.WithConfPath(sdkConfPath),
	)
	if err != nil {
		return nil, err
	}

	// Enable certificate compression
	if cc.GetAuthType() == sdk.PermissionedWithCert {
		if err := cc.EnableCertHash(); err != nil {
			return nil, err
		}
	}
	return cc, nil
}

// CreateContract create contract with sdk chain client
func CreateContract(client *sdk.ChainClient, contractName, contractVersion, contractFilePath string,
	withSyncResult bool, usernames ...string) (txId string, err error) {
	resp, err := createContract(client, contractName, contractVersion, contractFilePath,
		common.RuntimeType_DOCKER_GO, []*common.KeyValuePair{}, withSyncResult, usernames...)
	if resp != nil {
		txId = resp.TxId
		return
	}

	return
}

// CreateChannel create channel
func CreateChannel(client *sdk.ChainClient, contractName string, params map[string][]byte,
	withSyncResult bool) (resp *ChannelInfoResp, err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("create"),
		},
		{
			Key:   "channel_name",
			Value: params["channel_name"],
		},
		{
			Key:   "user_id",
			Value: params["user_id"],
		},
		{
			Key:   "user_address",
			Value: params["user_address"],
		},
		{
			Key:   "user_pubkey",
			Value: params["user_pubkey"],
		},
		{
			Key:   "key_type",
			Value: params["key_type"],
		},
		{
			Key:   "hash_type",
			Value: params["hash_type"],
		},
		{
			Key:   "max_user",
			Value: params["max_user"],
		},
		{
			Key:   "balance",
			Value: params["balance"],
		},
		{
			Key:   "sign",
			Value: params["sign"],
		},
	}

	contractResp, err := invokeContract(client, contractName, innerMethod, "", kvs, withSyncResult,
		&common.Limit{GasLimit: 200000})
	if err != nil {
		return
	}

	resp = new(ChannelInfoResp)
	err = json.Unmarshal(contractResp.ContractResult.Result, resp)
	if err != nil {
		return
	}
	return
}

// JoinChannel join channel
func JoinChannel(client *sdk.ChainClient, contractName string, params map[string][]byte) (
	resp *ChannelInfoResp, err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("join"),
		},
		{
			Key:   "channel_name",
			Value: params["channel_name"],
		},
		{
			Key:   "user_id",
			Value: params["user_id"],
		},
		{
			Key:   "user_address",
			Value: params["user_address"],
		},
		{
			Key:   "user_pubkey",
			Value: params["user_pubkey"],
		},
		{
			Key:   "key_type",
			Value: params["key_type"],
		},
		{
			Key:   "hash_type",
			Value: params["hash_type"],
		},
		{
			Key:   "balance",
			Value: params["balance"],
		},
		{
			Key:   "sign",
			Value: params["sign"],
		},
	}

	contractResp, err := invokeContract(client, contractName, innerMethod, "", kvs, true, &common.Limit{GasLimit: 200000})
	if err != nil {
		return
	}

	resp = new(ChannelInfoResp)
	err = json.Unmarshal(contractResp.ContractResult.Result, resp)
	if err != nil {
		return
	}

	return
}

// BeginExchange begin exchange
func BeginExchange(client *sdk.ChainClient, contractName string, params map[string][]byte) (err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("exchange"),
		},
		{
			Key:   "channel_name",
			Value: params["channel_name"],
		},
	}

	_, err = invokeContract(client, contractName, innerMethod, "", kvs, true, &common.Limit{GasLimit: 200000})
	if err != nil {
		return
	}

	return
}

// Commit commit epoch state
func Commit(client *sdk.ChainClient, contractName string, params map[string][]byte) (
	err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("commit"),
		},
		{
			Key:   "channel_name",
			Value: params["channel_name"],
		},
		{
			Key:   "data",
			Value: params["data"],
		},
	}

	_, err = invokeContract(client, contractName, innerMethod, "", kvs, true, &common.Limit{GasLimit: 200000})
	if err != nil {
		return
	}

	return
}

// Challeage user challeage
func Challeage(client *sdk.ChainClient, contractName string, params map[string][]byte) (err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("challeage"),
		},
		{
			Key:   "channel_name",
			Value: params["channel_name"],
		},
		{
			Key:   "merkletree",
			Value: params["merkletree"],
		},
	}

	_, err = invokeContract(client, contractName, innerMethod, "", kvs, true, &common.Limit{GasLimit: 200000})
	if err != nil {
		return
	}

	return
}

// GetChalleage leader get user challeage
func GetChalleage(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp []string, err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("getchalleage"),
		},
		{
			Key:   "channel_name",
			Value: params["channel_name"],
		},
	}

	contractResp, err := queryContract(client, contractName, innerMethod, kvs)
	if err != nil {
		return
	}

	err = json.Unmarshal(contractResp.ContractResult.Result, &resp)
	if err != nil {
		return
	}

	return
}

// Answer answer
func Answer(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp string, err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("answer"),
		},
		{
			Key:   "channel_name",
			Value: params["channel_name"],
		},
		{
			Key:   "answer_id",
			Value: params["answer_id"],
		},
		{
			Key:   "merkletree",
			Value: params["merkletree"],
		},
	}

	contractResp, err := invokeContract(client, contractName, innerMethod, "", kvs, true, &common.Limit{GasLimit: 200000})
	if err != nil {
		return
	}

	resp = string(contractResp.ContractResult.Result)

	return
}

// ChannelInfo channel info
func ChannelInfo(client *sdk.ChainClient, contractName string, params map[string][]byte) (
	resp *ChannelInfoResp, err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("chaninfo"),
		},
		{
			Key:   "channel_name",
			Value: params["channel_name"],
		},
	}

	contractResp, err := queryContract(client, contractName, innerMethod, kvs)
	if err != nil {
		return
	}

	resp = new(ChannelInfoResp)
	err = json.Unmarshal(contractResp.ContractResult.Result, resp)
	if err != nil {
		return
	}

	return
}

// UserInfo user info
func UserInfo(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp string, err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("userinfo"),
		},
		{
			Key:   "channel_name",
			Value: params["channel_name"],
		},
		{
			Key:   "user_id",
			Value: params["user_id"],
		},
	}

	contractResp, err := queryContract(client, contractName, innerMethod, kvs)
	if err != nil {
		return
	}

	resp = string(contractResp.ContractResult.Result)

	return
}

// Rollback rollback this epoch state
func Rollback(client *sdk.ChainClient, contractName string, params map[string][]byte) (err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("rollback"),
		},
		{
			Key:   "channel_name",
			Value: params["channel_name"],
		},
	}

	resp, err := invokeContract(client, contractName, innerMethod, "", kvs, true, &common.Limit{GasLimit: 200000})
	if err != nil {
		return
	}

	fmt.Printf("queryContract success. resp=%+v\n", resp)

	return
}

// Exit exit this channel
func Exit(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp string, err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("exit"),
		},
		{
			Key:   "channel_name",
			Value: params["channel_name"],
		},
	}

	contractResp, err := invokeContract(client, contractName, innerMethod, "", kvs, true, &common.Limit{GasLimit: 200000})
	if err != nil {
		return
	}

	resp = string(contractResp.ContractResult.Result)

	return
}

// Get get channel state
func Get(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp string, err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("get"),
		},
		{
			Key:   "channel_name",
			Value: params["channel_name"],
		},
		{
			Key:   "key",
			Value: params["key"],
		},
	}

	contractResp, err := queryContract(client, contractName, innerMethod, kvs)
	if err != nil {
		return
	}

	resp = string(contractResp.ContractResult.Result)

	return
}

// Set set channel state
func Set(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp string, err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("set"),
		},
		{
			Key:   "channel_name",
			Value: params["channel_name"],
		},
		{
			Key:   "key",
			Value: params["key"],
		},
		{
			Key:   "val",
			Value: params["val"],
		},
	}

	contractResp, err := invokeContract(client, contractName, innerMethod, "", kvs, true, &common.Limit{GasLimit: 200000})
	if err != nil {
		return
	}

	resp = string(contractResp.ContractResult.Result)

	return
}

func invokeContract(client *sdk.ChainClient, contractName, method, txId string,
	kvs []*common.KeyValuePair, withSyncResult bool, limit *common.Limit) (resp *common.TxResponse, err error) {

	resp, err = client.InvokeContractWithLimit(contractName, method, txId, kvs, -1, withSyncResult, limit)
	if err != nil {
		return
	}

	if resp.Code != common.TxStatusCode_SUCCESS {
		err = fmt.Errorf("invoke contract failed, [code:%d]/[msg:%s]/[result:%+v]", resp.Code, resp.ContractResult.Message,
			string(resp.ContractResult.Result))
		return
	}

	return
}

func queryContract(client *sdk.ChainClient, contractName, method string, kvs []*common.KeyValuePair) (
	resp *common.TxResponse, err error) {
	resp, err = client.QueryContract(contractName, method, kvs, -1)
	if err != nil {
		return
	}

	if resp.Code != common.TxStatusCode_SUCCESS {
		err = fmt.Errorf("query contract failed, [code:%d]/[msg:%s]/[result:%+v]", resp.Code, resp.Message,
			string(resp.ContractResult.Result))
		return
	}

	return
}

func createContract(client *sdk.ChainClient, contractName, version, byteCodePath string, runtime common.RuntimeType,
	kvs []*common.KeyValuePair, withSyncResult bool, usernames ...string) (*common.TxResponse, error) {

	payload, err := client.CreateContractCreatePayload(contractName, version, byteCodePath, runtime, kvs)
	if err != nil {
		return nil, err
	}

	payload = client.AttachGasLimit(payload, &common.Limit{
		GasLimit: 60000000,
	})

	endorsers, err := GetEndorsersWithAuthType(client.GetHashType(),
		client.GetAuthType(), payload, usernames...)
	if err != nil {
		return nil, err
	}

	resp, err := client.SendContractManageRequest(payload, endorsers, createContractTimeout, withSyncResult)
	if err != nil {
		return resp, err
	}

	err = CheckProposalRequestResp(resp, true)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

// Transfer transfer stake from a to b
func Transfer(client *sdk.ChainClient, contractName string, params map[string][]byte, withSyncResult bool) (
	resp string, err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("transfer"),
		},
		{
			Key:   "from",
			Value: params["from"],
		},
		{
			Key:   "to",
			Value: params["to"],
		},
		{
			Key:   "num",
			Value: params["num"],
		},
	}

	contractResp, err := invokeContract(client, contractName, innerMethod, "", kvs, withSyncResult,
		&common.Limit{GasLimit: 200000})
	if err != nil {
		return
	}

	resp = string(contractResp.ContractResult.Result)
	return
}

// BalanceOf get balance from user
func BalanceOf(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp string,
	err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("balanceOf"),
		},
		{
			Key:   "from",
			Value: params["from"],
		},
	}

	contractResp, err := queryContract(client, contractName, innerMethod, kvs)
	if err != nil {
		return
	}

	resp = string(contractResp.ContractResult.Result)
	return
}

// InvokeStake invoke stake contract
func InvokeStake(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp string,
	err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("invoke"),
		},
		{
			Key:   "user_id",
			Value: params["user_id"],
		},
		{
			Key:   "user_pubkey",
			Value: params["user_pubkey"],
		},
		{
			Key:   "key_type",
			Value: params["key_type"],
		},
		{
			Key:   "hash_type",
			Value: params["hash_type"],
		},
	}

	contractResp, err := invokeContract(client, contractName, innerMethod, "", kvs, true, &common.Limit{GasLimit: 200000})
	if err != nil {
		return
	}

	resp = string(contractResp.ContractResult.Result)
	return
}

// SenderPk get sender pk
func SenderPk(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp string,
	err error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("senderPk"),
		},
	}

	contractResp, err := queryContract(client, contractName, innerMethod, kvs)
	if err != nil {
		return
	}

	resp = string(contractResp.ContractResult.Result)
	return
}
