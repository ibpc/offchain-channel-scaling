/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package network

import (
	"crypto/rand"
	"encoding/json"
	"math/big"
	"time"

	"chainmaker.org/ibpc/offchain-channel-scaling/module/logger"

	"chainmaker.org/chainmaker/net-libp2p/libp2pnet"
)

// MsgType message type
type MsgType int32

const (
	// HBPing heart beat message type
	HBPing MsgType = iota
	// NodeJoin node join message type
	NodeJoin
	// NodeJoinResp node join response message type
	NodeJoinResp
	// NodeExit node exit message type
	NodeExit
	// NodePubkeySync node pub key sync message type
	NodePubkeySync
	// TxMsg tx message message type
	TxMsg
	// TxRequest tx request message type
	TxRequest
	// TxCommit tx commit message type
	TxCommit
	// TxNotify tx notify message type
	TxNotify
	// UpdateStateRequest update state request message type
	UpdateStateRequest
	// UpdateStateReply update state reply message type
	UpdateStateReply
	// UpdateStateConfirm update state confirm message type
	UpdateStateConfirm
	// TradePhaseNotify trade phase notify message type
	TradePhaseNotify
	// ConsensusPhaseNotify consensus phase notify message type
	ConsensusPhaseNotify
)

// Message define message struct
type Message struct {
	MsgType MsgType
	Data    []byte
	From    string
	To      string
}

// SendMsg send message
func SendMsg(chanName string, node *libp2pnet.LibP2pNet, msg *Message, receiveId string) (err error) {
	flog := logger.GetSugaLogger().With("func", "SendMsg")

	var data []byte
	data, err = json.Marshal(msg)
	if err != nil {
		flog.Errorf("json marshal err, %v", err)
		return
	}

	for i := 0; i < 3; i++ {
		err = node.SendMsg(chanName, receiveId, msgFlag, data)
		if err != nil {
			flog.Errorf("[%s] send msg to [%s] in [%s] err, %v", node.GetNodeUid(), receiveId, chanName, err)
			randN, err := rand.Int(rand.Reader, big.NewInt(1000))
			if err != nil {
				flog.Errorf("rand.Int error, %v", err)
			}
			time.Sleep(time.Duration(randN.Int64()+500) * time.Millisecond)
			continue
		}
		break
	}

	return
}
