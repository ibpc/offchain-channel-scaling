/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package network

import (
	"fmt"
	"io/ioutil"

	"chainmaker.org/ibpc/offchain-channel-scaling/config"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/logger"

	cmlog "chainmaker.org/chainmaker/logger/v2"
	"chainmaker.org/chainmaker/net-libp2p/libp2pnet"
	"chainmaker.org/chainmaker/protocol/v2"
)

// CreateP2PNet use channel name create p2pnet
func CreateP2PNet(chanName string) (nodeNetwork *libp2pnet.LibP2pNet, err error) {
	oriLog := logger.GetSugaLogger()
	flog := oriLog.With("func", "CreateP2PNet")

	// get net log
	netLog := cmlog.GetLogger(cmlog.MODULE_NET)
	netLog.SetLogger(oriLog)

	// get net config
	netConf := config.GetNetConf()

	// create a startup flag
	nodeReadySignalC := make(chan struct{})
	nodeNetwork, err = createNet(
		netLog,
		WithListenAddr(netConf.ListenAddr),
		WithReadySignalC(nodeReadySignalC),
		WithCrypto(false, netConf.TLSConfig.PrivKeyFile, netConf.TLSConfig.CertFile),
		WithTrustRoots(chanName, netConf.CaPath...),
	)
	if err != nil {
		flog.Errorf("create net err, %v", err)
		return
	}

	err = nodeNetwork.Start()
	if err != nil {
		flog.Errorf("net start err, %v", err)
		return
	}

	// get node pid
	nodePid := nodeNetwork.GetNodeUid()
	// string splicing to construct full address
	nodeFullAddr := netConf.ListenAddr + "/p2p/" + nodePid
	flog.Infof("[%v] start", nodeFullAddr)

	close(nodeReadySignalC)
	return
}

// create libp2p net object
func createNet(logger protocol.Logger, opts ...NetOption) (*libp2pnet.LibP2pNet, error) {
	localNet, err := libp2pnet.NewLibP2pNet(logger)
	if err != nil {
		return nil, err
	}
	// load configuration
	if err := apply(localNet, opts...); err != nil {
		return nil, err
	}
	return localNet, nil
}

// NetOption is a function apply options to net instance.
type NetOption func(ln *libp2pnet.LibP2pNet) error

// WithReadySignalC set a ready flag
func WithReadySignalC(signalC chan struct{}) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		ln.Prepare().SetReadySignalC(signalC)
		return nil
	}
}

// WithListenAddr set addr that the local net will listen on.
func WithListenAddr(addr string) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		ln.Prepare().SetListenAddr(addr)
		return nil
	}
}

// WithCrypto set private key file and tls cert file for the net to create connection.
func WithCrypto(pkMode bool, keyFile string, certFile string) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		var (
			err                 error
			keyBytes, certBytes []byte
		)
		keyBytes, err = ioutil.ReadFile(keyFile)
		if err != nil {
			return fmt.Errorf("read private key file %v err, %v", keyFile, err)
		}
		if !pkMode {
			certBytes, err = ioutil.ReadFile(certFile)
			if err != nil {
				return fmt.Errorf("read cert file %v err, %v", certFile, err)
			}
		}
		ln.Prepare().SetPubKeyModeEnable(pkMode)
		ln.Prepare().SetKey(keyBytes)
		if !pkMode {
			ln.Prepare().SetCert(certBytes)
		}
		return nil
	}
}

// WithTrustRoots set up custom Trust Roots
func WithTrustRoots(chainId string, caFiles ...string) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		var trustRoots [][]byte
		for _, caPath := range caFiles {
			caBytes, err := ioutil.ReadFile(caPath)
			if err != nil {
				return fmt.Errorf("read ca file %v err, %v", caPath, err)
			}
			trustRoots = append(trustRoots, caBytes)
		}

		ln.SetChainCustomTrustRoots(chainId, trustRoots)
		return nil
	}
}

// WithSeeds set addresses of discovery service node.
func WithSeeds(seeds ...string) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		for _, seed := range seeds {
			ln.Prepare().AddBootstrapsPeer(seed)
		}
		return nil
	}
}

// WithPeerStreamPoolSize set the max stream pool size for every node that connected to us.
func WithPeerStreamPoolSize(size int) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		ln.Prepare().SetPeerStreamPoolSize(size)
		return nil
	}
}

// WithPubSubMaxMessageSize set max message size (M) for pub/sub.
func WithPubSubMaxMessageSize(size int) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		ln.Prepare().SetPubSubMaxMsgSize(size)
		return nil
	}
}

// WithMaxPeerCountAllowed set max count of nodes that connected to us.
func WithMaxPeerCountAllowed(max int) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		ln.Prepare().SetMaxPeerCountAllow(max)
		return nil
	}
}

// WithPeerEliminationStrategy set the strategy for eliminating node when the count of nodes
// that connected to us reach the max value.
func WithPeerEliminationStrategy(strategy int) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		ln.Prepare().SetPeerEliminationStrategy(strategy)
		return nil
	}
}

// WithBlackAddresses set addresses of the nodes for blacklist.
func WithBlackAddresses(blackAddresses ...string) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		for _, ba := range blackAddresses {
			ln.Prepare().AddBlackAddress(ba)
		}
		return nil
	}
}

// WithBlackNodeIds set ids of the nodes for blacklist.
func WithBlackNodeIds(blackNodeIds ...string) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		for _, bn := range blackNodeIds {
			ln.Prepare().AddBlackPeerId(bn)
		}
		return nil
	}
}

// WithMsgCompression set whether compressing the payload when sending msg.
func WithMsgCompression(enable bool) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		ln.SetCompressMsgBytes(enable)
		return nil
	}
}

// WithInsecurity set insecurity p2p
func WithInsecurity(isInsecurity bool) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		ln.Prepare().SetIsInsecurity(isInsecurity)
		return nil
	}
}

// WithPktEnable set pk enable
func WithPktEnable(pktEnable bool) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		ln.Prepare().SetPktEnable(pktEnable)
		return nil
	}
}

// WithPriorityControlEnable config priority controller
func WithPriorityControlEnable(priorityCtrlEnable bool) NetOption {
	return func(ln *libp2pnet.LibP2pNet) error {
		ln.Prepare().SetPriorityCtrlEnable(priorityCtrlEnable)
		return nil
	}
}

// apply options.
func apply(ln *libp2pnet.LibP2pNet, opts ...NetOption) error {
	for _, opt := range opts {
		if opt == nil {
			continue
		}
		if err := opt(ln); err != nil {
			return err
		}
	}
	return nil
}
