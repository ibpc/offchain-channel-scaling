/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package network

import (
	"fmt"
	"reflect"
	"testing"

	"chainmaker.org/chainmaker/net-libp2p/libp2pnet"
	"chainmaker.org/ibpc/offchain-channel-scaling/config"
	"github.com/agiledragon/gomonkey/v2"
)

func TestCreateP2PNet(t *testing.T) {
	config.ConfPath = "./testconf/config.yaml"
	config.InitConfig()

	var pnet libp2pnet.LibP2pNet
	gomonkey.ApplyMethod(reflect.TypeOf(&pnet), "Start", func(*libp2pnet.LibP2pNet) error {
		return nil
	})
	gomonkey.ApplyMethod(reflect.TypeOf(&pnet), "GetNodeUid", func(*libp2pnet.LibP2pNet) string {
		return "kj23h492492hnjk"
	})

	chanName := "test_chan"
	net1, err := CreateP2PNet(chanName)
	if err != nil {
		t.Fatalf("CreateP2PNet 01 failed, err=%v", err)
	}
	err = SetHandler(chanName, net1, "node01")
	if err != nil {
		t.Fatalf("SetHandler 01 failed, err=%v", err)
	}

	addr := config.GetNetConf().ListenAddr

	patch := gomonkey.ApplyMethod(reflect.TypeOf(net1), "SendMsg", func(*libp2pnet.LibP2pNet, string, string,
		string, []byte) error {
		return nil
	})
	defer patch.Reset()

	config.ConfPath = "./testconf/config1.yaml"
	config.InitConfig()

	net2, err := CreateP2PNet(chanName)
	if err != nil {
		t.Fatalf("CreateP2PNet 02 failed, err=%v", err)
	}
	err = SetHandler(chanName, net2, "node02")
	if err != nil {
		t.Fatalf("SetHandler 02 failed, err=%v", err)
	}
	err = net2.AddSeed(fmt.Sprintf("%s/p2p/%s", addr, net1.GetNodeUid()))
	if err != nil {
		t.Log("SetHandler 02 failed, err=", err)
	}

	msg := Message{Data: []byte("msg test")}
	err = SendMsg(chanName, net2, &msg, net1.GetNodeUid())
	if err != nil {
		t.Log("SendMsg failed, err=", err)
	}
}
