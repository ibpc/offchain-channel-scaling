/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package network

import (
	"encoding/json"

	"chainmaker.org/ibpc/offchain-channel-scaling/module/logger"

	"chainmaker.org/chainmaker/net-libp2p/libp2pnet"
)

const (
	msgFlag = "TEST_MSG_SEND"
)

// SetHandler set p2p message handler
func SetHandler(chanName string, nodeNetwork *libp2pnet.LibP2pNet, nodeName string) (err error) {
	flog := logger.GetSugaLogger().With("func", "SetHandler")

	nodeMsgHandler := func(peerId string, msgData []byte) (errHandler error) {
		flog.Infof("[%v][%v] recv a msg from peer[%v]", nodeName, chanName, peerId)

		var msg Message
		errHandler = json.Unmarshal(msgData, &msg)
		if errHandler != nil {
			flog.Error("json unmarshal err = ", errHandler)
			return
		}

		flog.Infof("msg: { [MsgType]:[%v], [data]:[%v], [from]:[%v], [To]:[%v] }", msg.MsgType, msg.Data, msg.From, msg.To)
		return
	}

	// register msg handler
	err = nodeNetwork.DirectMsgHandle(chanName, msgFlag, nodeMsgHandler)
	if err != nil {
		flog.Errorf("[%s]Register msg handler err, %s", nodeName, err)
		return
	}
	return
}
