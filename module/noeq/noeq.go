/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package noeq

import (
	"crypto/md5"
	crand "crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"io"
	"math/rand"
	"sync"
	"time"
)

//
// snowflake算法参考be/kit/noeq中的实现
// 41位时间戳、10位机器号、12位序列号
//
// ------------------------------------------
// |   timestamp    |  worker  |  sequence  |
// ------------------------------------------
// |       41       |    10    |     12     |
// ------------------------------------------
//
// 1.Worker值保证了多个进程生成的ID不同；
// 2.Timestamp保证了同一进程在不同时间(精度毫秒)生成的ID不同；
// 3.Sequence保证了同一进程在同一毫秒生成的ID不同。
//
// 整体使用63位，首位始终为0不使用：
//   1.timestamp使用41位表示，可以让程序运行近70年;
//   2.worker使用10位表示，可以扩展1024台机器(进程);
//   3.sequence使用12位表示，单进程同一毫秒可以生成4096个ID。
//

var (
	epoch          = int64(1553513783000) // 定义元/起始时间
	seqBits uint64 = 12
	widBits uint64 = 10
	tsBits  uint64 = 41

	widShift = seqBits
	tsShift  = widBits + widShift
	seqMax   = int64(-1) ^ (int64(-1) << seqBits) // 序列号节点最大值 4095
	widMax   = int64(-1) ^ (int64(-1) << widBits) // 机器节点最大值 1023
	tsMax    = int64(-1) ^ (int64(-1) << tsBits)  // 时间戳节点最大值 2199023255551

	lts int64      // 上次生成时间戳
	seq int64      // 序列号
	wid int64      // 机器ID
	mu  sync.Mutex // 锁
)

func init() {
	rand.Seed(time.Now().Unix())
}

// Init 初始化WorkerID
func Init(workerID int64) {
	wid = (workerID & widMax) << widShift
}

// milliSeconds 获取当前毫秒
func milliSeconds() int64 {
	return time.Now().UnixNano() / 1e6
}

// GenRangeID 根据个数生成区间内最小和最大ID
func GenRangeID(num int64) (min, max int64) {
	mu.Lock()
	defer mu.Unlock()

	ts := milliSeconds()
	if ts < lts {
		// ts<lts 有可能是容器回秒
		ts = lts
	}

	if ts == lts {
		if seq >= seqMax {
			// 当序号已经达到最大值
			for ts <= lts {
				ts = milliSeconds()
			}
			seq = 0
		}
	} else {
		seq = 0
	}

	if seq+num-1 > seqMax {
		num = seqMax - seq + 1
	} else if num < 1 {
		num = 1
	}

	min = (((ts - epoch) & tsMax) << tsShift) | wid | seq
	max = min + num - 1
	seq += num
	lts = ts
	return
}

// GenID 生成ID
func GenID() (id int64) {
	id, _ = GenRangeID(1)
	return
}

// ParseID 解析ID，还原出机器ID、时间、序号
func ParseID(id int64) (wid, ms, seq int64) {
	seq = id & seqMax
	wid = (id >> widShift) & widMax
	ms = (id>>tsShift)&tsMax + epoch
	return
}

// GenUID 生成字符串ID
func GenUID() string {
	return GenRandStr(32)
}

// GenJWTMess gen jwt mess
func GenJWTMess() string {
	return GenRandStr(12)
}

// GenSalt 生成盐
func GenSalt() string {
	return GenRandStr(8)
}

// TimeFormat time format
const TimeFormat = "20060102150405"

// GenRandStr 生成固定位数的随机字符串
func GenRandStr(n int) string {
	b := make([]byte, 48)
	if _, err := io.ReadFull(crand.Reader, b); err != nil {
		return ""
	}
	h := md5.New()
	h.Write([]byte(base64.URLEncoding.EncodeToString(b)))

	if n < 1 || n > 32 {
		n = 32
	}

	return hex.EncodeToString(h.Sum(nil))[:n]
}
