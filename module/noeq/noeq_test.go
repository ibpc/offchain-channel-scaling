/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package noeq

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestNoeq(t *testing.T) {
	Convey("TestNoeq", t, func() {
		Convey("Init", func() {
			So(func() {
				Init(1)
			}, ShouldNotPanic)
		})

		Convey("milliSeconds", func() {
			So(func() {
				milliSeconds()
			}, ShouldNotPanic)
		})

		Convey("GenRangeID", func() {
			min, max := GenRangeID(5)
			t.Logf("mix: %d, max: %d", min, max)
		})

		Convey("GenID", func() {
			So(func() {
				id := GenID()
				ParseID(id)
			}, ShouldNotPanic)
		})

		Convey("GenUID", func() {
			str := GenUID()
			So(str, ShouldHaveLength, 32)
		})

		Convey("GenJWTMess", func() {
			str := GenJWTMess()
			So(str, ShouldHaveLength, 12)
		})

		Convey("GenSalt", func() {
			str := GenSalt()
			So(str, ShouldHaveLength, 8)
		})

		Convey("GenRandStr", func() {
			str := GenRandStr(8)
			So(str, ShouldHaveLength, 8)
		})
	})
}
