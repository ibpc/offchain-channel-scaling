/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package node

import (
	"context"
	"fmt"
	"reflect"
	"testing"

	"chainmaker.org/chainmaker/net-libp2p/libp2pnet"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
	"chainmaker.org/ibpc/offchain-channel-scaling/config"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/contract"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/network"

	"github.com/agiledragon/gomonkey/v2"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSend(t *testing.T) {
	config.ConfPath = testConfPath
	config.InitConfig()

	// create four node and run
	var err error
	var node *Node

	var otherID = "xxx"

	var pnet libp2pnet.LibP2pNet
	gomonkey.ApplyMethod(reflect.TypeOf(&pnet), "Start", func(*libp2pnet.LibP2pNet) error {
		return nil
	})
	gomonkey.ApplyMethod(reflect.TypeOf(&pnet), "GetNodeUid", func(*libp2pnet.LibP2pNet) string {
		return nodeId
	})

	node, err = CreateNode("chain1")
	if err != nil {
		t.Fatalf("CreateNode failed, err=%v", err)
	}

	gomonkey.ApplyFunc(contract.CreateChannel, func(client *sdk.ChainClient, contractName string, params map[string][]byte, withSyncResult bool) (resp *contract.ChannelInfoResp, err error) {
		resp = &contract.ChannelInfoResp{
			LeaderId: node.Id,
		}
		return
	})

	gomonkey.ApplyFunc(contract.Commit, func(client *sdk.ChainClient, contractName string, params map[string][]byte) error {
		return nil
	})

	gomonkey.ApplyFunc(contract.ChannelInfo, func(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp *contract.ChannelInfoResp, err error) {
		resp = new(contract.ChannelInfoResp)
		resp.LeaderId = otherID
		resp.LeaderAddr = node.NetAddr
		resp.Status = 1
		resp.ChalleageStatus = 0

		return
	})

	gomonkey.ApplyFunc(contract.GetChalleage, func(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp []string, err error) {
		resp = make([]string, 0)
		return
	})

	node.ctx, node.ctxCancelFunc = context.WithCancel(context.Background())

	err = node.CreateChannel(10000)
	if err != nil {
		t.Fatalf("CreateChannel failed, err=%v", err)
	}

	gomonkey.ApplyFunc(network.SendMsg, func(chanName string, node *libp2pnet.LibP2pNet, msg *network.Message, receiveId string) error {
		return nil
	})

	Convey("SendTest", t, func() {
		Convey("SendPing", func() {
			err = node.SendPing(node.Id)
			So(err, ShouldNotBeNil)

			err = node.SendPing("xxxx")
			So(err, ShouldBeNil)
		})

		Convey("SendJoinRequest", func() {
			So(func() {
				node.SendJoinRequest()
			}, ShouldNotPanic)
		})

		Convey("SendExitRequest", func() {
			err = node.SendExitRequest()
			So(err, ShouldBeNil)

			node.state.LeaderId = node.Id

			err = node.SendExitRequest()
			So(err, ShouldBeNil)
		})

		Convey("SendTxRequest", func() {
			_, err = node.SendTxRequest(100, otherID)
			So(err, ShouldNotBeNil)

			node.SetPhase(TradePhase)

			_, err = node.SendTxRequest(100, otherID)
			So(err, ShouldBeNil)
		})

		Convey("SendTradePhaseNotify", func() {
			node.SendTradePhaseNotify()

			node.state.LeaderId = otherID

			node.SendTradePhaseNotify()
			So(err, ShouldBeNil)
		})

		Convey("sendConsensusPhaseNotify", func() {
			node.state.LeaderId = node.Id
			node.sendConsensusPhaseNotify()
			So(err, ShouldBeNil)
		})
	})

	Convey("answer-1", t, func() {
		node.answer()
		So(err, ShouldBeNil)
	})

	gomonkey.ApplyFunc(contract.GetChalleage, func(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp []string, err error) {
		resp = []string{otherID}
		return
	})

	gomonkey.ApplyFunc(contract.Answer, func(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp string, err error) {
		err = fmt.Errorf("test")
		return
	})

	Convey("answer-2", t, func() {
		node.answer()
		So(err, ShouldBeNil)
	})

	gomonkey.ApplyFunc(contract.Answer, func(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp string, err error) {
		resp = "true"
		return
	})

	Convey("answer-3", t, func() {
		node.answer()
		So(err, ShouldBeNil)
	})

	gomonkey.ApplyFunc(contract.Answer, func(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp string, err error) {
		resp = "false"
		return
	})

	Convey("answer-3", t, func() {
		node.answer()
		So(err, ShouldBeNil)
	})
}
