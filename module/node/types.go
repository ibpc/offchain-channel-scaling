/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package node

import (
	"context"
	"sync"
	"time"

	"chainmaker.org/ibpc/offchain-channel-scaling/module/crypto"

	cmCrypto "chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/net-libp2p/libp2pnet"
)

// Node off chain node
type Node struct {
	Id             string             // node id
	ChannelName    string             // channel net name
	NodeName       string             // node name TODO: 有没有用，待最终确认
	NetAddr        string             // node network address
	Balance        int                // node balance
	Joined         bool               // node joined channel
	Exited         bool               // node exited channel
	JoinFailReason string             // node exited channel
	ctx            context.Context    // consensus state confirm
	ctxCancelFunc  context.CancelFunc // consensus state confirm

	state     *State    // node state
	leader    *Leader   // only leader use
	txSet     []Tx      // an epoch tx set
	nodePhase Phase     // node phase
	Once      sync.Once // once send trade phase notify

	keyPair         *crypto.KeyPair        // node public and private key pair
	nodeSignOpts    *cmCrypto.SignOpts     // Signing options
	otherNodePubKey map[string]*NodeEntity // exist node public key map

	NodeLibp2pNet *libp2pnet.LibP2pNet //node network

	sync.RWMutex
}

// TradeMsg 交易信息
type TradeMsg struct {
	Code int
	TxId string
	Msg  string
}

// TradeMsgChan 交易信息通道
var TradeMsgChan tradeMsgChan

type tradeMsgChan struct {
	mutex sync.RWMutex
	Chans map[string]chan TradeMsg
}

func (tc *tradeMsgChan) AddChan(txId string, tradeMsgChan chan TradeMsg) {
	tc.mutex.Lock()
	defer tc.mutex.Unlock()

	if tc.Chans == nil {
		tc.Chans = make(map[string]chan TradeMsg)
	}

	tc.Chans[txId] = tradeMsgChan
}

func (tc *tradeMsgChan) DelChan(txId string) {
	tc.mutex.Lock()
	defer tc.mutex.Unlock()

	if tc.Chans == nil {
		return
	}

	delete(tc.Chans, txId)
}

func (tc *tradeMsgChan) GetChan(txId string) (tradeMsgChan chan TradeMsg) {
	tc.mutex.RLock()
	defer tc.mutex.RUnlock()

	if tc.Chans == nil {
		return nil
	}

	tradeMsgChan = tc.Chans[txId]
	return
}

// NodeEntity node entity
type NodeEntity struct {
	PubKey *crypto.PubKeyEntity
	Seed   string
}

// Tx transaction struct
type Tx struct {
	Id          string // tx id
	Epoch       int    // epoch of tx
	Sender      string // sender id
	Receiver    string // receiver id
	Amount      int    // tx amount
	LeaderSig   []byte // leader sig
	SenderSig   []byte // sender sig
	ReceiverSig []byte // receiver sig

	StartTime time.Time     // tx start time
	Duration  time.Duration // tx duration time
}

// Leader off chain epoch leader
type Leader struct {
	txSet            []Tx            // an epoch tx set
	enrollmentSet    map[string]int  // node enrollment set
	withdrawSet      map[string]int  // node exit set
	sendAmountSet    map[string]int  // all nodes send amount
	receiveAmountSet map[string]int  // all nodes receive amount
	txIdUsed         map[string]bool // the epoch used txid
	newState         State
}

// State state
type State struct {
	Epoch       int
	LeaderId    string
	BalanceSet  map[string]int
	WithdrawSet map[string]int // node exit set
	SigSet      map[string][]byte
}

// Phase phase
type Phase int

const (
	// InitPhase init phase, no operation can be performed
	InitPhase Phase = iota
	// TradePhase Trades can be made at this stage
	TradePhase
	// ConsensusPhase The trading phase timeout triggers entering the consensus phase
	ConsensusPhase
	// ArbitrationPhase Unable to reach a consensus, all nodes enter the challenge phase
	ArbitrationPhase
	// ExitPhase exit phase
	ExitPhase
)

// JoinData join channel data
type JoinData struct {
	KeyType cmCrypto.KeyType
	PubKey  []byte // user public key
	Seed    string // user p2p net seed
}

// JoinResp join channel resp
type JoinResp struct {
	Success bool
	Reason  string // user p2p net seed
}
