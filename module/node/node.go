/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package node

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"time"

	"chainmaker.org/ibpc/offchain-channel-scaling/config"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/contract"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/crypto"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/logger"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/network"

	"go.uber.org/zap"
)

// phaseTicker global ticker
var nodeTimer *time.Timer

// TradeTimeout of the transaction phase (in seconds)
const TradeTimeout int64 = 30

// CreateNode create channel node
func CreateNode(channelName string) (node *Node, err error) {
	flog := logger.GetLogger().With(zap.String("func", "CreateNode"), zap.String("channel", channelName))
	p2pNet, err := network.CreateP2PNet(channelName)
	if err != nil {
		flog.Error("CreateP2PNet failed", zap.Error(err))
		return
	}

	allConf := config.GetAllConf()
	if allConf == nil {
		flog.Error("allConf is nil")
		return
	}

	keyPair, opts, err := crypto.GetKeyPair(allConf.AuthType, allConf.UserConfig)
	if err != nil {
		flog.Error("GetKeyPair failed", zap.Error(err))
		return
	}

	pubkeyBytes, _ := keyPair.PublicKey.String()

	flog.Sugar().Infof("[====]My Public key is : %v", pubkeyBytes)
	flog.Sugar().Infof("[====]My Public key type is : %d", keyPair.PublicKey.Type())
	flog.Sugar().Infof("[====]My Sign opt type is : %d", opts.Hash)

	nodeID := p2pNet.GetNodeUid()

	node = &Node{
		NodeLibp2pNet:   p2pNet,
		Id:              nodeID,
		ChannelName:     channelName,
		NetAddr:         fmt.Sprintf("%s/p2p/%s", allConf.NetConfig.ListenAddr, nodeID),
		nodePhase:       InitPhase,
		keyPair:         keyPair,
		nodeSignOpts:    opts,
		otherNodePubKey: make(map[string]*NodeEntity),
		state: &State{
			Epoch:       0,
			BalanceSet:  make(map[string]int),
			SigSet:      make(map[string][]byte),
			WithdrawSet: make(map[string]int),
		},
		leader: &Leader{
			txSet:            make([]Tx, 0),
			enrollmentSet:    make(map[string]int),
			withdrawSet:      make(map[string]int),
			sendAmountSet:    make(map[string]int),
			receiveAmountSet: make(map[string]int),
			txIdUsed:         make(map[string]bool),
		},
	}

	publicKeyBytes, err := keyPair.PublicKey.Bytes()
	if err != nil {
		flog.Error("keyPair.PublicKey.Bytes failed", zap.Error(err))
		return
	}

	pubKeyEntity := crypto.PubKeyEntity{
		PublicKey:   keyPair.PublicKey,
		KeyType:     keyPair.KeyType,
		PubKeyBytes: publicKeyBytes,
	}

	node.otherNodePubKey[nodeID] = &NodeEntity{
		PubKey: &pubKeyEntity,
		Seed:   node.NetAddr,
	}

	// Set up the message handler function
	err = node.setMsgHandler()
	return
}

// initLeaderNode init node leader field
func (node *Node) initLeaderNode() {
	node.leader = &Leader{
		txSet:            make([]Tx, 0),
		enrollmentSet:    make(map[string]int),
		withdrawSet:      make(map[string]int),
		sendAmountSet:    make(map[string]int),
		receiveAmountSet: make(map[string]int),
		txIdUsed:         make(map[string]bool),
	}
}

// CreateChannel create channel
func (node *Node) CreateChannel(balance int) (err error) {
	flog := logger.GetLogger().With(zap.String("func", "CreateChannel"), zap.String("channel", node.ChannelName))

	publicKeyBytes, err := node.keyPair.PublicKey.Bytes()
	if err != nil {
		flog.Error("keyPair.PublicKey.Bytes failed", zap.Error(err))
		return
	}

	keyType := node.keyPair.PublicKey.Type()

	signBody := fmt.Sprintf("%s_%s_%s", node.Id, config.GetAllConf().ContractName, fmt.Sprint(balance))
	sign, err := crypto.SignTx(node.keyPair.PrivateKey, node.nodeSignOpts, signBody)
	if err != nil {
		flog.Error("crypto.SignTx failed", zap.Error(err))
		return
	}

	params := map[string][]byte{
		"channel_name": []byte(node.ChannelName),
		"user_id":      []byte(node.Id),
		"user_address": []byte(node.NetAddr),
		"user_pubkey":  publicKeyBytes,
		"key_type":     []byte(fmt.Sprint(keyType)),
		"hash_type":    []byte(fmt.Sprint(node.nodeSignOpts.Hash)),
		"max_user":     []byte(fmt.Sprint(50)),
		"balance":      []byte(fmt.Sprint(balance)),
		"sign":         sign,
	}

	resp, err := contract.CreateChannel(contract.Cli, config.GetAllConf().ContractName, params, true)
	if err != nil {
		flog.Error("CreateChannel failed", zap.Error(err))
		return
	}

	if resp.LeaderId == node.Id {
		// // The node is the new leader node
		node.state.Epoch = resp.Epoch
		node.state.LeaderId = resp.LeaderId
		for userId, balance := range resp.BalanceSet {
			node.state.BalanceSet[userId] = balance
		}
		node.Balance = node.state.BalanceSet[node.Id]
	} else {
		node.state.Epoch = resp.Epoch
		node.state.LeaderId = resp.LeaderId
		for userId, balance := range resp.BalanceSet {
			node.state.BalanceSet[userId] = balance
		}
		node.Balance = node.state.BalanceSet[node.Id]

		flog.Sugar().Infof("contract.CreateChannel resp, leader_id=%s, balance=%d", resp.LeaderId, balance)
		err = node.NodeLibp2pNet.AddSeed(resp.LeaderAddr)
		// 发送加入通道消息
		go func() {
			for {
				errSend := node.SendPing(node.LeaderID())
				if errSend == nil {
					node.SendJoinRequest()
					break
				}
				time.Sleep(time.Second)
			}
		}()
	}

	return
}

// JoinChannel join channel
func (node *Node) JoinChannel(balance int) (isLeader bool, err error) {
	flog := logger.GetLogger().With(zap.String("func", "JoinChannel"), zap.String("channel", node.ChannelName))

	publicKeyBytes, err := node.keyPair.PublicKey.Bytes()
	if err != nil {
		flog.Error("keyPair.PublicKey.Bytes failed", zap.Error(err))
		return
	}

	keyType := node.keyPair.PublicKey.Type()

	signBody := fmt.Sprintf("%s_%s_%s", node.Id, config.GetAllConf().ContractName, fmt.Sprint(balance))
	sign, err := crypto.SignTx(node.keyPair.PrivateKey, node.nodeSignOpts, signBody)
	if err != nil {
		flog.Error("crypto.SignTx failed", zap.Error(err))
		return
	}

	params := map[string][]byte{
		"channel_name": []byte(node.ChannelName),
		"user_id":      []byte(node.Id),
		"user_address": []byte(node.NetAddr),
		"user_pubkey":  publicKeyBytes,
		"key_type":     []byte(fmt.Sprint(keyType)),
		"hash_type":    []byte(fmt.Sprint(node.nodeSignOpts.Hash)),
		"balance":      []byte(fmt.Sprint(balance)),
		"sign":         sign,
	}

	flog.Sugar().Infof("pubkey bytes, %v", publicKeyBytes)

	resp, err := contract.JoinChannel(contract.Cli, config.GetAllConf().ContractName, params)
	if err != nil {
		flog.Error("JoinChannel failed", zap.Error(err))
		return
	}

	if resp.LeaderId == node.Id {
		isLeader = true
		// The node is the new leader node
		node.initLeaderNode()
		node.Lock()
		node.state.Epoch = resp.Epoch
		node.state.LeaderId = resp.LeaderId
		for userId, balance := range resp.BalanceSet {
			node.state.BalanceSet[userId] = balance
		}
		node.Balance = node.state.BalanceSet[node.Id]
		node.Unlock()
		// set trade timer
		nodeTimer = time.NewTimer(time.Second * time.Duration(TradeTimeout))
		go func() {
			<-nodeTimer.C
			// The timer is triggered, and the consensus phase is entered
			go node.sendConsensusPhaseNotify()
			nodeTimer.Stop()
		}()
	} else {
		node.Lock()
		node.Balance = balance
		node.state.BalanceSet[node.Id] = balance
		// 设置 leaderId
		node.state.LeaderId = resp.LeaderId
		node.Unlock()
		flog.Sugar().Infof("contract.JoinChannel resp, leader_id=%s, balance=%d", resp.LeaderId, balance)
		err = node.NodeLibp2pNet.AddSeed(resp.LeaderAddr)
	}

	return
}

// setMsgHandler set message handler
func (node *Node) setMsgHandler() (err error) {
	flog := logger.GetSugaLogger().With("func", "setMsgHandler")
	msgFlag := "TEST_MSG_SEND"

	rand.Seed(time.Now().UnixNano())

	// handlerFunction set
	handlerFuncMap := make(map[network.MsgType]func(data []byte, from string))
	handlerFuncMap[network.HBPing] = node.handlerPing
	handlerFuncMap[network.NodeJoin] = node.handlerJoinRequest
	handlerFuncMap[network.NodeJoinResp] = node.handlerJoinResponse
	handlerFuncMap[network.NodeExit] = node.handlerExitRequest
	handlerFuncMap[network.NodePubkeySync] = node.handlerPubKeySyncRequest
	handlerFuncMap[network.TxMsg] = node.handlerTxMsg
	handlerFuncMap[network.TxRequest] = node.handlerTxRequest
	handlerFuncMap[network.TxCommit] = node.handlerTxCommit
	handlerFuncMap[network.TxNotify] = node.handlerTxNotify
	handlerFuncMap[network.UpdateStateRequest] = node.handlerUpdateStateRequest
	handlerFuncMap[network.UpdateStateReply] = node.handlerUpdateStateReply
	handlerFuncMap[network.UpdateStateConfirm] = node.handlerUpdateStateConfirm
	handlerFuncMap[network.TradePhaseNotify] = node.handlerTradePhaseNotify

	// set the processor to receive information for the node
	nodeMsgHandler := func(peerId string, data []byte) (errHandler error) {
		var msg network.Message
		errHandler = json.Unmarshal(data, &msg)
		if errHandler != nil {
			flog.Errorf("json unmarshal err, %v", errHandler)
			return
		}
		// Message preprocessing
		switch msg.MsgType {
		case network.TxNotify, network.TxRequest, network.TxCommit:
			if node.nodePhase != TradePhase {
				flog.Errorf("[%v][%v] is not in tradePhase! nodePhase: [%v]", node.Id, peerId, node.nodePhase)
				return
			}
		case network.UpdateStateConfirm, network.UpdateStateReply:
			if node.nodePhase != ConsensusPhase {
				flog.Errorf("[%v][%v] is not in consensusPhase! nodePhase: [%v]", node.Id, peerId, node.nodePhase)
				return
			}
		}

		// Executes the handler for the corresponding message type
		if handlerFunc, ok := handlerFuncMap[msg.MsgType]; ok {
			go handlerFunc(msg.Data, msg.From)
		} else {
			flog.Error(" Unknown message type! ")
		}

		return
	}

	// register msg handler
	err = node.NodeLibp2pNet.DirectMsgHandle(node.ChannelName, msgFlag, nodeMsgHandler)
	if err != nil {
		flog.Errorf("%s register msg handler err, %v", node.NodeName, err)
		return
	}
	return
}

// LeaderID get LeaderID
func (node *Node) LeaderID() (leaderID string) {
	node.RLock()
	defer node.RUnlock()

	leaderID = node.state.LeaderId
	return
}

// SetPhase set phase
func (node *Node) SetPhase(phase Phase) {
	node.Lock()
	defer node.Unlock()

	node.nodePhase = phase
}

// Phase get phase
func (node *Node) Phase() (phase Phase) {
	node.RLock()
	defer node.RUnlock()

	phase = node.nodePhase
	return
}

// CheckTxId check tx id
func (node *Node) CheckTxId(txId string) (exist bool) {
	node.RLock()
	defer node.RUnlock()

	for idx := range node.txSet {
		if node.txSet[idx].Id == txId {
			exist = true
			return
		}
	}
	return
}

// PrintStatus print node status
func (node *Node) PrintStatus() {
	flog := logger.GetSugaLogger().With("func", "PrintStatus")
	flog.Infof("=== balance: %d, phase: %d, joined: %v", node.Balance, node.nodePhase, node.Joined)
	flog.Infof("=== state: %+v, leader: %+v", *node.state, *node.leader)
}
