/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package node

import (
	"context"
	"encoding/json"
	"fmt"
	"sort"
	"strings"
	"time"

	"chainmaker.org/ibpc/offchain-channel-scaling/common"
	"chainmaker.org/ibpc/offchain-channel-scaling/config"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/contract"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/crypto"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/logger"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/merkletree"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/network"

	"github.com/google/uuid"
	"go.uber.org/zap"
)

// SendPing send heart beat ping
func (node *Node) SendPing(receiverId string) (err error) {
	flog := logger.GetSugaLogger().With(zap.String("func", "SendPing"), zap.String("channel", node.ChannelName))
	// check node is the leader
	if strings.EqualFold(node.Id, receiverId) {
		err = fmt.Errorf("you cannot send transactions to yourself")
		flog.Errorf("you cannot send transactions to yourself")
		return
	}

	var msg = network.Message{
		MsgType: network.HBPing,
		Data:    []byte("ping"),
		From:    node.Id,
		To:      node.state.LeaderId,
	}

	err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, receiverId)
	return
}

// SendJoinRequest send join request
func (node *Node) SendJoinRequest() {
	flog := logger.GetSugaLogger().With(zap.String("func", "SendJoinRequest"), zap.String("channel", node.ChannelName))

	params := map[string][]byte{
		"channel_name": []byte(node.ChannelName),
	}

	resp, err := contract.ChannelInfo(contract.Cli, config.GetAllConf().ContractName, params)
	if err != nil {
		flog.Error("GetChannelInfo failed", zap.Error(err))
		return
	}

	node.Lock()
	node.state.LeaderId = resp.LeaderId
	node.state.Epoch = resp.Epoch
	node.Unlock()

	// check node is the leader
	if strings.EqualFold(node.Id, node.state.LeaderId) {
		flog.Error("you are leader, no need send join request")
		return
	}

	pubkeyBytes, err := node.keyPair.PublicKey.Bytes()
	if err != nil {
		flog.Errorf("get public key bytes error, %v", err)
		return
	}

	msgData, err := json.Marshal(JoinData{
		KeyType: node.keyPair.PublicKey.Type(),
		PubKey:  pubkeyBytes,
		Seed:    node.NetAddr,
	})
	if err != nil {
		flog.Errorf("json marshal join data error, %v", err)
		return
	}

	var msg = network.Message{
		MsgType: network.NodeJoin,
		Data:    msgData,
		From:    node.Id,
		To:      node.state.LeaderId,
	}

	err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, node.state.LeaderId)
	if err != nil {
		flog.Errorf("send message error, %v", err)
		return
	}

	node.Lock()
	node.nodePhase = InitPhase
	node.Unlock()
}

// SendExitRequest send exit request
func (node *Node) SendExitRequest() (err error) {
	flog := logger.GetSugaLogger().With(zap.String("func", "SendExitRequest"), zap.String("channel", node.ChannelName))
	var msg = network.Message{
		MsgType: network.NodeExit,
		From:    node.Id,
		To:      node.state.LeaderId,
	}

	node.Joined = false
	// check node is the leader
	if strings.EqualFold(node.Id, node.state.LeaderId) {
		node.Lock()
		node.leader.withdrawSet[node.Id] = 1
		node.Unlock()
		return
	}

	err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, node.state.LeaderId)
	if err != nil {
		flog.Errorf("send message error, %v", err)
		return
	}
	return
}

// sendTxMsg 发送发起交易结果信息
func (node *Node) sendTxMsg(err error, txId, sender string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "sendTxMsg"),
		zap.String("channel", node.ChannelName), zap.String("userID", node.Id))
	var tradeMsg = TradeMsg{
		Code: common.TradeMsgFail,
		TxId: txId,
		Msg:  err.Error(),
	}
	// send tradeMsg to sender
	var encodingMsg []byte
	encodingMsg, err = json.Marshal(tradeMsg)
	if err != nil {
		flog.Errorf("json marshal error, %v", err)
		return
	}
	msg := network.Message{MsgType: network.TxMsg, Data: encodingMsg, From: node.Id, To: sender}
	if !strings.EqualFold(node.Id, sender) {
		err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, sender)
		if err != nil {
			flog.Errorf("network Send tradeMsg error, %v", err)
		}
	} else {
		go node.handlerTxMsg(encodingMsg, node.Id)
	}
}

// SendTxRequest the transaction is constructed, signed and sent to the receiver (sender)
func (node *Node) SendTxRequest(amount int, receiverId string) (txId string, err error) {
	flog := logger.GetSugaLogger().With(zap.String("func", "SendTxRequest"), zap.String("channel", node.ChannelName))
	if strings.EqualFold(node.Id, receiverId) {
		err = fmt.Errorf("you cannot send transactions to yourself")
		flog.Errorf("%s", err.Error())
		return
	}
	if node.Phase() != TradePhase {
		err = fmt.Errorf("you are not in tradePhase, please wait a minute, phase: [%v] ", node.Phase())
		flog.Errorf("%s", err.Error())
		return
	}

	// Generate a transaction ID (UUID)
	txId = uuid.New().String()
	// sender signs tx
	signTx := Tx{
		Id:       txId,
		Epoch:    node.state.Epoch,
		Sender:   node.Id,
		Receiver: receiverId,
		Amount:   amount,
	}
	sig, err := crypto.SignTx(node.keyPair.PrivateKey, node.nodeSignOpts, signTx)
	if err != nil {
		flog.Errorf("sign tx error, %v", err)
		return
	}
	signTx.SenderSig = sig
	signTx.StartTime = time.Now()

	data, _ := json.Marshal(signTx)
	msg := network.Message{MsgType: network.TxRequest, Data: data, From: node.Id, To: receiverId}
	err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, receiverId)
	if err != nil {
		flog.Errorf("send message err, %v", err)
		return
	}
	return
}

// SendTradePhaseNotify Notifies all nodes to enter the transaction phase (leader)
func (node *Node) SendTradePhaseNotify() {
	flog := logger.GetSugaLogger().With(zap.String("func", "SendTradePhaseNotify"),
		zap.String("channel", node.ChannelName))
	// node is not a leader
	if !strings.EqualFold(node.Id, node.state.LeaderId) {
		flog.Errorf("you are not a leader")
		return
	}

	node.Once.Do(func() {
		node.SetPhase(TradePhase)

		// send all nodes
		for nodeId := range node.otherNodePubKey {
			if !strings.EqualFold(node.Id, nodeId) {
				flog.Infof("[SendTradePhaseNotify] from=%s, to=%s", node.Id, nodeId)
				msg := network.Message{MsgType: network.TradePhaseNotify, Data: make([]byte, 0), From: node.Id, To: nodeId}
				go func(tNodeId string) {
					err := network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, tNodeId)
					if err != nil {
						flog.Errorf("SendMsg TradePhaseNotify failed, err=%v", err)
						return
					}
				}(nodeId)
			}
		}

		// set trade timer
		nodeTimer = time.NewTimer(time.Second * time.Duration(TradeTimeout))
		go func() {
			<-nodeTimer.C
			// The timer is triggered, and the consensus phase is entered
			go node.sendConsensusPhaseNotify()
			nodeTimer.Stop()
		}()
	})
}

// Notifies all nodes to enter the consensus phase (leader)
func (node *Node) sendConsensusPhaseNotify() {
	flog := logger.GetSugaLogger().With(zap.String("func", "sendConsensusPhaseNotify"),
		zap.String("channel", node.ChannelName))
	flog.Infof("begin calculateNextState")

	// 设置超时，超时获取最新状态
	go func() {
		confirmTimer := time.NewTimer(time.Second * 5)
		node.ctx, node.ctxCancelFunc = context.WithCancel(context.Background())
		select {
		case <-confirmTimer.C:
			// The timer is triggered, and the consensus phase is entered
			go node.answer()
			confirmTimer.Stop()
		case <-node.ctx.Done():
			confirmTimer.Stop()
		}
	}()

	// node is not a leader
	if !strings.EqualFold(node.Id, node.state.LeaderId) {
		flog.Errorf("you are not a leader!")
		return
	}

	// set node phase is consensus phase
	node.SetPhase(ConsensusPhase)

	node.leader.newState = calculateNextState(node.state, node.leader.txSet, node.otherNodePubKey,
		node.leader.enrollmentSet, node.leader.withdrawSet, node.ChannelName)
	tempState := State{Epoch: node.leader.newState.Epoch, LeaderId: node.leader.newState.LeaderId,
		BalanceSet: node.leader.newState.BalanceSet, WithdrawSet: node.leader.newState.WithdrawSet}
	// sign the newState
	sig, err := crypto.SignTx(node.keyPair.PrivateKey, node.nodeSignOpts, tempState)
	if err != nil {
		flog.Errorf("sign state err, %v", err)
		return
	}
	// put the leader's signature in the newState to be sent
	node.leader.newState.SigSet[node.Id] = sig

	data, _ := json.Marshal(node.leader.newState)
	// send all nodes
	for nodeId := range node.otherNodePubKey {
		if !strings.EqualFold(node.Id, nodeId) {
			msg := network.Message{MsgType: network.UpdateStateRequest, Data: data, From: node.Id, To: nodeId}
			go func(tNodeId string) {
				err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, tNodeId)
				if err != nil {
					flog.Errorf("SendMsg UpdateStateRequest err, %v", err)
					return
				}
			}(nodeId)
		}
	}
}

// Calculates the new state based on the previous state
func calculateNextState(state *State, txSet []Tx, otherNode map[string]*NodeEntity, enrollmentSet map[string]int,
	withdrawalSet map[string]int, channelName string) (newState State) {
	flog := logger.GetSugaLogger().With(zap.String("func", "calculateNextState"), zap.String("channel", channelName))
	newState = State{
		Epoch:       state.Epoch + 1,
		BalanceSet:  make(map[string]int),
		SigSet:      make(map[string][]byte),
		WithdrawSet: withdrawalSet,
	}
	// 过滤掉上个 epoch 退出的节点
	for k, v := range state.BalanceSet {
		if _, ok := state.WithdrawSet[k]; !ok {
			newState.BalanceSet[k] = v
		}
	}

	for k, v := range enrollmentSet {
		flog.Infof("enrollment user, %s, balance=%d", k, v)
		newState.BalanceSet[k] = v
	}

	type user struct {
		ID      string
		TxCount int
	}

	txCounter := make(map[string]int)
	nextLeaderId := state.LeaderId

	// Calculate the balance of each node and select the leader of the next epoch
	for _, tx := range txSet {
		if tx.Amount > newState.BalanceSet[tx.Sender] || tx.Epoch != state.Epoch {
			flog.Errorf("calculateNextState error tx.Amount=%v, Sender.Balance=%v, tx.Epoch=%v, state.Epoch=%v\n",
				tx.Amount, newState.BalanceSet[tx.Sender], tx.Epoch, state.Epoch)
			return
		}

		newState.BalanceSet[tx.Sender] -= tx.Amount
		newState.BalanceSet[tx.Receiver] += tx.Amount

		txCounter[tx.Sender]++
		txCounter[tx.Receiver]++
	}

	var users = make([]user, 0, len(txCounter))
	for userId, count := range txCounter {
		users = append(users, user{ID: userId, TxCount: count})
	}

	// 从大到小排列
	sort.Slice(users, func(i, j int) bool { return users[i].TxCount > users[j].TxCount })
	for idx := range users {
		if _, ok := withdrawalSet[users[idx].ID]; !ok {
			nextLeaderId = users[idx].ID
			break
		}
	}

	newState.LeaderId = nextLeaderId
	for k, v := range newState.BalanceSet {
		if _, ok := newState.WithdrawSet[k]; ok {
			newState.WithdrawSet[k] = v
		}
	}
	flog.Infof("newState: [ Epoch: %v, leader %v, finishTask: %v, withdrea: %+v, balance: %+v ]\n", newState.Epoch,
		newState.LeaderId, len(txSet), newState.WithdrawSet, newState.BalanceSet)
	return newState
}

func (node *Node) commit() (err error) {
	flog := logger.GetSugaLogger().With(zap.String("func", "commit"),
		zap.String("channel", node.ChannelName), zap.String("userID", node.Id))
	flog.Infof("[%v][%v] begin commit", node.Id, node.ChannelName)

	var commitData []byte
	commitData, err = json.Marshal(node.leader.newState)
	if err != nil {
		flog.Error("marshal commit data failed", zap.Error(err))
		return
	}
	params := map[string][]byte{
		"channel_name": []byte(node.ChannelName),
		"data":         commitData,
	}
	err = contract.Commit(contract.Cli, config.GetAllConf().ContractName, params)
	if err != nil {
		flog.Error("Commit failed", zap.Error(err))
		return
	}

	flog.Infof("[%v][%v] Commit success", node.Id, node.ChannelName)

	// 通知其他节点
	var msg network.Message
	for nodeId := range node.otherNodePubKey {
		if !strings.EqualFold(node.Id, nodeId) {
			msg = network.Message{MsgType: network.UpdateStateConfirm, Data: commitData, From: node.Id, To: nodeId}
			go func(tNodeId string) {
				err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, tNodeId)
				if err != nil {
					flog.Errorf("SendMsg UpdateStateConfirm error, %v", err)
					return
				}
			}(nodeId)
		} else {
			go node.handlerUpdateStateConfirm(commitData, node.Id)
		}
	}
	return
}

// answer - answer challeage
func (node *Node) answer() {
	flog := logger.GetSugaLogger().With(zap.String("func", "answer"),
		zap.String("channel", node.ChannelName), zap.String("userID", node.Id))
	flog.Infof("[%v][%v] begin answer", node.Id, node.ChannelName)

	params := map[string][]byte{
		"channel_name": []byte(node.ChannelName),
	}
	resp, err := contract.GetChalleage(contract.Cli, config.GetAllConf().ContractName, params)
	if err != nil {
		flog.Error("GetChalleage failed", zap.Error(err))
		return
	}

	flog.Infof("[%v][%v] GetChalleage resp=%+v", node.Id, node.ChannelName, resp)

	if len(resp) == 0 {
		flog.Infof("[%v][%v] no challeage, begin commit", node.Id, node.ChannelName)
		// 无人挑战，提交状态到合约
		err = node.commit()
		if err != nil {
			flog.Error("commit failed", zap.Error(err))
			return
		}
		return
	}

	var challUserId string
	for _, userId := range resp {
		challUserId = userId
		break
	}

	var challTx = make([]Tx, 0)
	for idx := range node.leader.txSet {
		if node.leader.txSet[idx].Sender == challUserId || node.leader.txSet[idx].Receiver == challUserId {
			challTx = append(challTx, node.leader.txSet[idx])
		}
	}

	sort.Slice(challTx, func(i, j int) bool { return challTx[i].Id < challTx[j].Id })
	var txDatas = make([][]byte, 0, len(challTx))
	for idx := range challTx {
		var txData []byte
		txData, err = json.Marshal(challTx[idx])
		if err != nil {
			flog.Errorf("json marshal tx err, %v", err)
			return
		}
		txDatas = append(txDatas, txData)
	}

	var challTree *merkletree.MerkleTree
	if len(txDatas) == 0 {
		challTree = &merkletree.MerkleTree{Data: make([][]byte, 0), Nodes: make([][]byte, 0)}
	} else {
		challTree, err = merkletree.New(txDatas)
		if err != nil {
			flog.Errorf("merkletree tx err, %v", err)
			return
		}
	}

	challTreeData, err := json.Marshal(challTree)
	if err != nil {
		flog.Errorf("json marshal challTree err, %v", err)
		return
	}

	flog.Infof("[%v][%v] Answer data=%s", node.Id, node.ChannelName, string(challTreeData))

	params = map[string][]byte{
		"channel_name": []byte(node.ChannelName),
		"answer_id":    []byte(challUserId),
		"merkletree":   challTreeData,
	}
	answerResp, err := contract.Answer(contract.Cli, config.GetAllConf().ContractName, params)
	if err != nil {
		flog.Error("Answer failed", zap.Error(err))
		// 设置超时，超时获取最新状态
		go func() {
			confirmTimer := time.NewTimer(time.Second * 2)
			node.ctx, node.ctxCancelFunc = context.WithCancel(context.Background())

			for {
				select {
				case <-confirmTimer.C:
					// The timer is triggered, and the consensus phase is entered
					go node.getChannelInfo()
					confirmTimer.Reset(time.Second * 2)
				case <-node.ctx.Done():
					confirmTimer.Stop()
					return
				}
			}
		}()
		return
	}

	flog.Infof("[%v][%v] Answer success, answerResp=%s", node.Id, node.ChannelName, answerResp)

	if answerResp == common.True {
		err = node.commit()
		if err != nil {
			flog.Error("commit failed", zap.Error(err))
			return
		}
	} else {
		// 设置超时，超时获取最新状态
		go func() {
			confirmTimer := time.NewTicker(time.Second * 2)
			node.ctx, node.ctxCancelFunc = context.WithCancel(context.Background())
			select {
			case <-confirmTimer.C:
				// The timer is triggered, and the consensus phase is entered
				go node.getChannelInfo()
				confirmTimer.Stop()
			case <-node.ctx.Done():
				confirmTimer.Stop()
			}
		}()
	}
}
