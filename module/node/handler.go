/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package node

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"math/big"
	"sort"
	"strconv"
	"strings"
	"time"

	"chainmaker.org/ibpc/offchain-channel-scaling/common"
	"chainmaker.org/ibpc/offchain-channel-scaling/config"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/contract"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/crypto"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/logger"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/merkletree"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/network"

	"go.uber.org/zap"
)

// handlerPing handle heart beat ping
func (node *Node) handlerPing(data []byte, from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "handlerPing"), zap.String("channel", node.ChannelName))
	flog.Infof("[%v][%v] recv a msg from peer[%v], %s", node.Id, node.ChannelName, from, string(data))
}

func (node *Node) handlerJoinConsensusRequest(from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "handlerJoinConsensusRequest"),
		zap.String("channel", node.ChannelName))
	flog.Errorf("you are in ConsensusPhase, please wait a minute, phase: [%v] ", node.Phase())
	msgData, err := json.Marshal(JoinResp{
		Success: false,
		Reason:  "in consensus phase",
	})
	if err != nil {
		flog.Errorf("json marshal join resp data error, %v", err)
		return
	}

	var msg = network.Message{
		MsgType: network.NodeJoinResp,
		Data:    msgData,
		From:    node.Id,
		To:      from,
	}

	err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, from)
	if err != nil {
		flog.Errorf("send message error, %v", err)
		return
	}
}

func (node *Node) transmitJoinRequest2Leader(data []byte, from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "transmitJoinRequest2Leader"),
		zap.String("channel", node.ChannelName))
	flog.Errorf("I'm not leader node, I am [%s], leader is []", node.Id, node.LeaderID())
	var msg = network.Message{
		MsgType: network.NodeJoin,
		Data:    data,
		From:    from,
		To:      node.state.LeaderId,
	}

	err := network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, node.state.LeaderId)
	if err != nil {
		flog.Errorf("send message error, %v", err)
		return
	}
}

// handlerJoinRequest handle join channel request
func (node *Node) handlerJoinRequest(data []byte, from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "handlerJoinRequest"), zap.String("channel", node.ChannelName))
	flog.Infof("[%v][%v] recv a msg from peer[%v]", node.Id, node.ChannelName, from)

	if node.Phase() == ConsensusPhase {
		node.handlerJoinConsensusRequest(from)
		return
	}

	if !strings.EqualFold(node.Id, node.state.LeaderId) {
		node.transmitJoinRequest2Leader(data, from)
		return
	}

	var joinData JoinData
	err := json.Unmarshal(data, &joinData)
	if err != nil {
		flog.Errorf("json unmarshal error, %v", err)
		return
	}

	pubKey, err := crypto.ParsePublicKey(joinData.KeyType, joinData.PubKey)
	if err != nil {
		flog.Errorf("parse public key error, %v", err)
		return
	}

	params := map[string][]byte{
		"channel_name": []byte(node.ChannelName),
		"user_id":      []byte(from),
	}

	balanceStr, err := contract.UserInfo(contract.Cli, config.GetAllConf().ContractName, params)
	if err != nil {
		flog.Error("UserInfo failed", zap.Error(err))
		return
	}

	balance, err := strconv.ParseInt(balanceStr, 10, 64)
	if err != nil {
		flog.Error("ParseInt balanceStr failed", zap.Error(err))
		return
	}

	// 设置新加入用户信息
	node.Lock()
	node.otherNodePubKey[from] = &NodeEntity{
		PubKey: &crypto.PubKeyEntity{
			PublicKey:   pubKey,
			KeyType:     joinData.KeyType,
			PubKeyBytes: joinData.PubKey,
		},
		Seed: joinData.Seed,
	}

	node.state.BalanceSet[from] = int(balance)
	node.leader.enrollmentSet[from] = int(balance)
	node.Unlock()

	if strings.EqualFold(node.Id, node.state.LeaderId) {
		for nodeId, pubKey := range node.otherNodePubKey {
			// 同步新节点公钥到所有老节点
			if !strings.EqualFold(node.Id, nodeId) && !strings.EqualFold(from, nodeId) {
				msg := network.Message{MsgType: network.NodePubkeySync, Data: data, From: from, To: nodeId}
				go func(tNodeId string) {
					err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, tNodeId)
					if err != nil {
						flog.Infof("[%s]network.SendMsg failed, new pk to old user, err=%v", tNodeId, err)
					}
				}(nodeId)
			}

			// 同步所有老节点公钥到新节点
			if !strings.EqualFold(from, nodeId) {
				joinData.KeyType = pubKey.PubKey.KeyType
				joinData.PubKey = pubKey.PubKey.PubKeyBytes
				joinData.Seed = pubKey.Seed

				var leaderPubKeyData []byte
				leaderPubKeyData, err = json.Marshal(joinData)
				if err != nil {
					flog.Errorf("json marshal error, %v", err)
					return
				}
				msg := network.Message{MsgType: network.NodePubkeySync, Data: leaderPubKeyData, From: nodeId, To: from}
				go func(tNodeId string) {
					err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, tNodeId)
					if err != nil {
						flog.Infof("[%s]network.SendMsg failed, old pk to new user, err=%v", tNodeId, err)
					}
				}(from)
			}
		}

		if len(node.otherNodePubKey) >= 2 {
			// 当通道中有大于等于 2 个节点时，通知可以开始交易阶段
			go func() {
				time.Sleep(3 * time.Second)
				params := map[string][]byte{
					"channel_name": []byte(node.ChannelName),
				}
				err = contract.BeginExchange(contract.Cli, config.GetAllConf().ContractName, params)
				if err != nil {
					flog.Error("BeginExchange failed", zap.Error(err))
					return
				}
				node.SendTradePhaseNotify()
			}()
		}
	}

	msgData, err := json.Marshal(JoinResp{
		Success: true,
	})
	if err != nil {
		flog.Errorf("json marshal join resp data error, %v", err)
		return
	}

	var joinSuccMsg = network.Message{
		MsgType: network.NodeJoinResp,
		Data:    msgData,
		From:    node.Id,
		To:      from,
	}

	err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &joinSuccMsg, from)
	if err != nil {
		flog.Errorf("send message error, %v", err)
		return
	}

	// 发送 ping 消息
	go func() {
		timer := time.NewTimer(time.Second * 60)
		for {
			select {
			case <-timer.C:
				timer.Stop()
				return
			default:
				err = node.SendPing(from)
				if err != nil {
					flog.Errorf("send ping error, %v", err)
				}
				randN, err := rand.Int(rand.Reader, big.NewInt(2000))
				if err != nil {
					flog.Errorf("rand.Int error, %v", err)
				}
				time.Sleep(time.Duration(1000+randN.Int64()) * time.Millisecond)
			}
		}
	}()
}

// handlerJoinResponse handle join channel response
func (node *Node) handlerJoinResponse(data []byte, from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "handlerJoinResponse"),
		zap.String("channel", node.ChannelName), zap.String("userID", node.Id))
	flog.Infof("[%v][%v] recv a msg from peer[%v], %s", node.Id, node.ChannelName, from, string(data))

	var joinResp JoinResp
	err := json.Unmarshal(data, &joinResp)
	if err != nil {
		flog.Errorf("json unmarshal error, %v", err)
		return
	}

	if joinResp.Success {
		node.Lock()
		node.Joined = true
		node.Exited = false
		node.Unlock()

		flog.Infof("Join success.")
		return
	}

	flog.Errorf("Join failed.")
}

// handlerExitRequest handle exit channel request
func (node *Node) handlerExitRequest(data []byte, from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "handlerExitRequest"),
		zap.String("channel", node.ChannelName), zap.String("userID", node.Id))
	flog.Infof("[%v][%v] recv a msg from peer[%v], %s", node.Id, node.ChannelName, from, string(data))

	if !strings.EqualFold(node.Id, node.state.LeaderId) {
		flog.Errorf("I'm not leader node")
		return
	}

	node.Lock()
	node.leader.withdrawSet[from] = 0
	node.Unlock()
}

// handlerPubKeySyncRequest handle node pubkey sync request
func (node *Node) handlerPubKeySyncRequest(data []byte, from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "handlerPubKeySyncRequest"),
		zap.String("channel", node.ChannelName), zap.String("userID", node.Id))
	flog.Infof("[%v][%v] recv a msg from peer[%v]", node.Id, node.ChannelName, from)
	var joinData JoinData
	err := json.Unmarshal(data, &joinData)
	if err != nil {
		flog.Errorf("json unmarshal error, %v", err)
		return
	}

	pubKey, err := crypto.ParsePublicKey(joinData.KeyType, joinData.PubKey)
	if err != nil {
		flog.Errorf("parse public key error, %v", err)
		return
	}

	node.Lock()
	node.otherNodePubKey[from] = &NodeEntity{
		PubKey: &crypto.PubKeyEntity{
			PublicKey:   pubKey,
			KeyType:     joinData.KeyType,
			PubKeyBytes: joinData.PubKey,
		},
		Seed: joinData.Seed,
	}
	node.Unlock()
	err = node.NodeLibp2pNet.AddSeed(joinData.Seed)
	if err != nil {
		flog.Errorf("p2p net add seed failed, err=%v", err)
	}

	// 发送 ping 消息
	go func() {
		timer := time.NewTimer(time.Second * 60)
		for {
			select {
			case <-timer.C:
				timer.Stop()
				return
			default:
				err = node.SendPing(from)
				if err != nil {
					flog.Errorf("node send ping failed, err=%v", err)
				}
				randN, err := rand.Int(rand.Reader, big.NewInt(2000))
				if err != nil {
					flog.Errorf("rand.Int error, %v", err)
				}
				time.Sleep(time.Duration(1000+randN.Int64()) * time.Millisecond)
			}

		}
	}()
}

// handlerTxMsg handle transaction message
func (node *Node) handlerTxMsg(data []byte, from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "handlerTxMsg"),
		zap.String("channel", node.ChannelName), zap.String("userID", node.Id))
	// flog.Infof("[%v][%v] recv a msg from peer[%v]", node.Id, node.ChannelName, from)

	var tradeMsg TradeMsg
	err := json.Unmarshal(data, &tradeMsg)
	if err != nil {
		flog.Errorf("json unmarshal error, %v", err)
		return
	}

	msgChan := TradeMsgChan.GetChan(tradeMsg.TxId)
	if msgChan == nil {
		flog.Errorf("TradeMsgChan GetChan failed, txId=%s", tradeMsg.TxId)
		return
	}

	msgChan <- tradeMsg
}

// handlerTxRequest handle transaction request
func (node *Node) handlerTxRequest(data []byte, from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "handlerTxRequest"),
		zap.String("channel", node.ChannelName), zap.String("userID", node.Id))
	// flog.Infof("[%v][%v] recv a msg from peer[%v]", node.Id, node.ChannelName, from)

	var receiveTx Tx
	err := json.Unmarshal(data, &receiveTx)
	if err != nil {
		flog.Errorf("json unmarshal error, %v", err)
		return
	}

	defer func() {
		if err != nil {
			var tradeMsg = TradeMsg{
				Code: common.TradeMsgFail,
				TxId: receiveTx.Id,
				Msg:  err.Error(),
			}
			// send tradeMsg to sender
			var encodingMsg []byte
			encodingMsg, err = json.Marshal(tradeMsg)
			if err != nil {
				flog.Errorf("json marshal error, %v", err)
				return
			}
			msg := network.Message{MsgType: network.TxMsg, Data: encodingMsg, From: node.Id, To: receiveTx.Sender}
			if !strings.EqualFold(node.Id, receiveTx.Sender) {
				err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, receiveTx.Sender)
				if err != nil {
					flog.Errorf("network Send tradeMsg error, %v", err)
				}
			} else {
				go node.handlerTxMsg(encodingMsg, node.Id)
			}
		}
	}()

	if node.Phase() != TradePhase {
		err = fmt.Errorf("you are not in tradePhase, please wait a minute, phase: [%v] ", node.Phase())
		flog.Error(err)
		return
	}

	if receiveTx.Epoch != node.state.Epoch {
		err = fmt.Errorf("tx's epoch is not the latest, receiveTx.Epoch=%d, node.state.Epoch=%d",
			receiveTx.Epoch, node.state.Epoch)
		flog.Error(err)
		return
	}

	signTx := Tx{
		Id:       receiveTx.Id,
		Epoch:    receiveTx.Epoch,
		Sender:   receiveTx.Sender,
		Receiver: receiveTx.Receiver,
		Amount:   receiveTx.Amount,
	}
	if _, ok := node.otherNodePubKey[receiveTx.Sender]; !ok {
		err = fmt.Errorf("not has this user pubkey, user_id=%s", receiveTx.Sender)
		flog.Error(err)
		return
	}
	// verify sender sig
	ok, err := crypto.VerifyTX(receiveTx.SenderSig, signTx, node.otherNodePubKey[receiveTx.Sender].PubKey.PublicKey,
		node.nodeSignOpts)
	// sign tx (receiver)
	if !ok || err != nil {
		err = fmt.Errorf("verify tx failed, ok=%v, err=%v", ok, err)
		flog.Error(err)
		return
	}

	sig, err := crypto.SignTx(node.keyPair.PrivateKey, node.nodeSignOpts, signTx)
	if err != nil {
		flog.Errorf("sign tx err, %v", err)
		return
	}
	receiveTx.ReceiverSig = sig

	// send signed tx to leader
	encodingSignedTx, err := json.Marshal(receiveTx)
	if err != nil {
		flog.Errorf("json marshal error, %v", err)
		return
	}

	msg := network.Message{MsgType: network.TxCommit, Data: encodingSignedTx, From: node.Id, To: node.state.LeaderId}

	if !strings.EqualFold(node.Id, node.state.LeaderId) {
		go func() {
			err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, node.state.LeaderId)
			if err != nil {
				flog.Errorf("network Send tx commit error, %v", err)
			}
		}()
	} else {
		// receiver is the leader
		node.handlerTxCommit(encodingSignedTx, node.Id)
	}
}

// handlerTxCommit handle transaction commit
func (node *Node) handlerTxCommit(data []byte, from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "handlerTxCommit"),
		zap.String("channel", node.ChannelName), zap.String("userID", node.Id))

	var receiveTx Tx
	err := json.Unmarshal(data, &receiveTx)
	if err != nil {
		flog.Errorf("json unmarshal err, %v", err)
		return
	}

	defer func() {
		if err != nil {
			node.sendTxMsg(err, receiveTx.Id, receiveTx.Sender)
		}
	}()

	if receiveTx.Epoch != node.state.Epoch {
		err = fmt.Errorf("tx's epoch is not the latest, receiveTx.Epoch=%d, node.state.Epoch=%d",
			receiveTx.Epoch, node.state.Epoch)
		flog.Error(err)
		return
	}

	signTx := Tx{
		Id:       receiveTx.Id,
		Epoch:    receiveTx.Epoch,
		Sender:   receiveTx.Sender,
		Receiver: receiveTx.Receiver,
		Amount:   receiveTx.Amount,
	}
	if _, ok := node.otherNodePubKey[receiveTx.Sender]; !ok {
		err = fmt.Errorf("not has this user pubkey, user_id=%s", receiveTx.Sender)
		flog.Error(err)
		return
	}
	// verify sender sig
	ok, err := crypto.VerifyTX(receiveTx.SenderSig, signTx, node.otherNodePubKey[receiveTx.Sender].PubKey.PublicKey,
		node.nodeSignOpts)
	if !ok || err != nil {
		err = fmt.Errorf("verify sender sign failed, ok=%v, err=%v", ok, err)
		flog.Error(err)
		return
	}
	if _, ok = node.otherNodePubKey[receiveTx.Receiver]; !ok {
		err = fmt.Errorf("not has this user pubkey, user_id=%s", receiveTx.Sender)
		flog.Error(err)
		return
	}
	// verify receiver sig
	ok, err = crypto.VerifyTX(receiveTx.ReceiverSig, signTx, node.otherNodePubKey[receiveTx.Receiver].PubKey.PublicKey,
		node.nodeSignOpts)
	if !ok || err != nil {
		err = fmt.Errorf("verify receiver sgin failed, ok=%v, err=%v", ok, err)
		flog.Error(err)
		return
	}
	// sign leader sig
	sig, err := crypto.SignTx(node.keyPair.PrivateKey, node.nodeSignOpts, signTx)
	if err != nil {
		flog.Errorf("sign tx err, %v", err)
		return
	}
	receiveTx.LeaderSig = sig

	receiveTx.Duration = time.Since(receiveTx.StartTime)

	// update node.leader.state
	node.Lock()
	// verify sender balance
	if receiveTx.Amount > (node.state.BalanceSet[receiveTx.Sender] - node.leader.sendAmountSet[receiveTx.Sender]) {
		err = fmt.Errorf("sender does not have sufficient balance")
		flog.Error(err)
		node.Unlock()
		return
	}
	// verify tx id
	if node.leader.txIdUsed[receiveTx.Id] {
		flog.Errorf("txid has use")
		node.Unlock()
		return
	}
	node.leader.txSet = append(node.leader.txSet, receiveTx)
	node.leader.sendAmountSet[receiveTx.Sender] += receiveTx.Amount
	node.leader.receiveAmountSet[receiveTx.Receiver] += receiveTx.Amount
	node.leader.txIdUsed[receiveTx.Id] = true
	node.Unlock()

	// send signed tx to sender and receiver
	encodingSignedTx, err := json.Marshal(receiveTx)
	if err != nil {
		flog.Errorf("json marshal error, %v", err)
		return
	}
	msg := network.Message{MsgType: network.TxNotify, Data: encodingSignedTx, From: node.Id, To: node.state.LeaderId}
	// leader send signed tx
	if !strings.EqualFold(node.Id, receiveTx.Sender) {
		go func() {
			err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, receiveTx.Sender)
			if err != nil {
				flog.Errorf("network send TxNotify error, %v", err)
			}
		}()
	} else {
		// leader is the sender
		go node.handlerTxNotify(encodingSignedTx, node.Id)
	}

	if !strings.EqualFold(node.Id, receiveTx.Receiver) {
		go func() {
			err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, receiveTx.Receiver)
			if err != nil {
				flog.Errorf("network send TxNotify error, %v", err)
			}
		}()
	} else {
		// leader is the receiver
		go node.handlerTxNotify(encodingSignedTx, node.Id)
	}
}

// handlerTxNotify handle transaction notify
func (node *Node) handlerTxNotify(data []byte, from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "handlerTxNotify"),
		zap.String("channel", node.ChannelName), zap.String("userID", node.Id))
	// flog.Infof("[%v][%v] recv a msg from peer[%v]", node.Id, node.ChannelName, from)

	var receiveTx Tx
	err := json.Unmarshal(data, &receiveTx)
	if err != nil {
		flog.Error("json unmarshal err, %v", err)
		return
	}

	defer func() {
		if err != nil {
			node.sendTxMsg(err, receiveTx.Id, receiveTx.Sender)
		}
	}()

	if receiveTx.Epoch != node.state.Epoch {
		err = fmt.Errorf("tx's epoch is not the latest, receiveTx.Epoch=%d, node.state.Epoch=%d",
			receiveTx.Epoch, node.state.Epoch)
		flog.Error(err)
		return
	}

	signTx := Tx{
		Id:       receiveTx.Id,
		Epoch:    receiveTx.Epoch,
		Sender:   receiveTx.Sender,
		Receiver: receiveTx.Receiver,
		Amount:   receiveTx.Amount,
	}
	if _, ok := node.otherNodePubKey[node.state.LeaderId]; !ok {
		flog.Errorf("not has this user pubkey, user_id=%s", node.state.LeaderId)
		return
	}
	// verify leader sig
	ok, err := crypto.VerifyTX(receiveTx.LeaderSig, signTx, node.otherNodePubKey[node.state.LeaderId].PubKey.PublicKey,
		node.nodeSignOpts)
	if !ok || err != nil {
		err = fmt.Errorf("verify leader sign failed, ok=%v, err=%v", ok, err)
		flog.Error(err)
		return
	}

	// sender and receiver Confirm tx
	if strings.Compare(node.Id, receiveTx.Sender) == 0 {
		node.Lock()
		node.txSet = append(node.txSet, receiveTx)
		node.Balance -= receiveTx.Amount
		node.Unlock()

		var tradeMsg = TradeMsg{
			Code: common.TradeMsgOk,
			TxId: receiveTx.Id,
		}

		msgChan := TradeMsgChan.GetChan(tradeMsg.TxId)
		if msgChan == nil {
			flog.Errorf("TradeMsgChan GetChan failed, txId=%s", tradeMsg.TxId)
			return
		}

		msgChan <- tradeMsg
	} else if strings.Compare(node.Id, receiveTx.Receiver) == 0 {
		node.Lock()
		node.txSet = append(node.txSet, receiveTx)
		node.Balance += receiveTx.Amount
		node.Unlock()
	} else {
		flog.Error("not sender and receiver, invalid tx")
		return
	}
}

// handlerTradePhaseNotify handle update phase request
func (node *Node) handlerTradePhaseNotify(data []byte, from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "handlerTradePhaseNotify"),
		zap.String("channel", node.ChannelName), zap.String("userID", node.Id))
	flog.Infof("[%v][%v] recv a msg from peer[%v]", node.Id, node.ChannelName, from)
	if !strings.EqualFold(from, node.state.LeaderId) {
		return
	}

	node.SetPhase(TradePhase)
}

// handlerUpdateStateRequest handle update state reply
func (node *Node) handlerUpdateStateRequest(data []byte, from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "handlerUpdateStateRequest"),
		zap.String("channel", node.ChannelName))
	flog.Infof("enter consensusPhase: [%v][%v] recv a msg from peer[%v]", node.Id, node.ChannelName, from)
	if !strings.EqualFold(from, node.state.LeaderId) {
		flog.Errorf("%s not leader", from)
		return
	}

	node.SetPhase(ConsensusPhase)

	// 设置超时，超时获取最新状态
	go func() {
		confirmTimer := time.NewTimer(time.Second * 7)
		node.ctx, node.ctxCancelFunc = context.WithCancel(context.Background())

		for {
			select {
			case <-confirmTimer.C:
				// The timer is triggered, and the consensus phase is entered
				go node.getChannelInfo()
				confirmTimer.Reset(time.Second * 2)
			case <-node.ctx.Done():
				confirmTimer.Stop()
				return
			}
		}
	}()

	var newState State
	err := json.Unmarshal(data, &newState)
	if err != nil {
		flog.Errorf("json unmarshal err, %v", err)
		return
	}

	// TODO: 这里是测试共识有歧义，待删除
	if node.state.Epoch == 1 && node.Id == "QmcGaUjVHXDC3fBgFTPx91uebzVFs7DDcMKBFHGEuzjE3A" {
		node.Balance = 0
	}

	// If not the latest status
	if newState.Epoch != node.state.Epoch+1 {
		flog.Errorf("new state epoch error, newState.Epoch=%d, node.state.Epoch=%d", newState.Epoch, node.state.Epoch)
		return
	}
	// Verify that the node balance is correct
	if node.Balance != newState.BalanceSet[node.Id] {
		flog.Errorf("new state balance error, mybalance=%d, state.balance=%d", node.Balance, newState.BalanceSet[node.Id])

		sort.Slice(node.txSet, func(i, j int) bool { return node.txSet[i].Id < node.txSet[j].Id })
		var txDatas = make([][]byte, 0, len(node.txSet))
		for idx := range node.txSet {
			var txData []byte
			txData, err = json.Marshal(node.txSet[idx])
			if err != nil {
				flog.Errorf("json marshal tx err, %v", err)
				return
			}
			txDatas = append(txDatas, txData)
		}

		var challTree *merkletree.MerkleTree
		if len(txDatas) == 0 {
			challTree = &merkletree.MerkleTree{Data: make([][]byte, 0), Nodes: make([][]byte, 0)}
		} else {
			challTree, err = merkletree.New(txDatas)
			if err != nil {
				flog.Errorf("merkletree tx err, %v", err)
				return
			}
		}

		var challTreeData []byte
		challTreeData, err = json.Marshal(challTree)
		if err != nil {
			flog.Errorf("json marshal challTree err, %v", err)
			return
		}

		params := map[string][]byte{
			"channel_name": []byte(node.ChannelName),
			"merkletree":   challTreeData,
		}
		err = contract.Challeage(contract.Cli, config.GetAllConf().ContractName, params)
		if err != nil {
			flog.Error("Challeage failed", zap.Error(err))
			return
		}
		flog.Errorf("Challeage success, data=%s", string(challTreeData))
		return
	}

	tempState := State{Epoch: newState.Epoch, LeaderId: newState.LeaderId, BalanceSet: newState.BalanceSet,
		WithdrawSet: newState.WithdrawSet}
	if _, ok := node.otherNodePubKey[node.state.LeaderId]; !ok {
		flog.Errorf("not has this user pubkey, user_id=%s", node.state.LeaderId)
		return
	}
	ok, err := crypto.VerifyTX(newState.SigSet[node.state.LeaderId], tempState,
		node.otherNodePubKey[node.state.LeaderId].PubKey.PublicKey, node.nodeSignOpts)
	if !ok || err != nil {
		flog.Errorf("verify state leader sig err, %v", err)
		return
	}

	// sign the newState
	sig, err := crypto.SignTx(node.keyPair.PrivateKey, node.nodeSignOpts, tempState)
	if err != nil {
		flog.Errorf("sign state err, %v", err)
		return
	}

	// 只需要发签名即可
	msg := network.Message{MsgType: network.UpdateStateReply, Data: sig, From: node.Id, To: node.state.LeaderId}
	go func() {
		err = network.SendMsg(node.ChannelName, node.NodeLibp2pNet, &msg, node.state.LeaderId)
		if err != nil {
			flog.Errorf("network SendMsg UpdateStateReply failed, err=%v", err)
			return
		}
	}()
}

// handlerUpdateStateReply -
func (node *Node) handlerUpdateStateReply(data []byte, from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "handlerUpdateStateReply"),
		zap.String("channel", node.ChannelName), zap.String("userID", node.Id))
	flog.Infof("[%v][%v] recv a msg from peer[%v]", node.Id, node.ChannelName, from)

	// verify the stateSig
	tempState := State{Epoch: node.leader.newState.Epoch, LeaderId: node.leader.newState.LeaderId,
		BalanceSet:  node.leader.newState.BalanceSet,
		WithdrawSet: node.leader.newState.WithdrawSet}
	if _, ok := node.otherNodePubKey[from]; !ok {
		flog.Errorf("not has this user pubkey, user_id=%s", from)
		return
	}
	ok, err := crypto.VerifyTX(data, tempState, node.otherNodePubKey[from].PubKey.PublicKey, node.nodeSignOpts)
	if !ok || err != nil {
		flog.Errorf("verify state node sig err, %v", err)
		return
	}

	node.Lock()
	node.leader.newState.SigSet[from] = data
	node.Unlock()

	flog.Infof("all node: %d, sign node: %d", len(node.otherNodePubKey), len(node.leader.newState.SigSet))

	// The signatures of all nodes are collected
	if len(node.leader.newState.SigSet) >= len(node.otherNodePubKey) {
		flog.Infof("all node sign, begin commit, user_id=%s", from)

		// 提交到合约
		err = node.commit()
		if err != nil {
			flog.Error("commit failed", zap.Error(err))
			return
		}
	}
}

// handlerUpdateStateConfirm -
func (node *Node) handlerUpdateStateConfirm(data []byte, from string) {
	flog := logger.GetSugaLogger().With(zap.String("func", "handlerUpdateStateConfirm"),
		zap.String("channel", node.ChannelName), zap.String("userID", node.Id))
	flog.Infof("[%v][%v] recv a msg from peer[%v]", node.Id, node.ChannelName, from)

	if strings.Compare(from, node.state.LeaderId) != 0 {
		flog.Errorf("not leader send, you are: %s, leader are: %s", from, node.state.LeaderId)
		return
	}
	var newState State
	err := json.Unmarshal(data, &newState)
	if err != nil {
		flog.Errorf("json unmarshal err, %v", err)
		return
	}

	// Verify that all state signatures are correct
	tempState := State{Epoch: newState.Epoch, LeaderId: newState.LeaderId, BalanceSet: newState.BalanceSet,
		WithdrawSet: newState.WithdrawSet}
	sign, ok := newState.SigSet[node.Id]
	if !ok {
		flog.Errorf("get sig failed")
		return
	}

	node.Lock()
	// verify the state signature
	ok, err = crypto.VerifyTX(sign, tempState, node.otherNodePubKey[node.Id].PubKey.PublicKey, node.nodeSignOpts)
	if !ok || err != nil {
		flog.Errorf("verify state node sig err, %v, sign=%v, tempState=%+v, node.nodeSignOpts=%+v", err, sign,
			tempState, *node.nodeSignOpts)
		node.Unlock()
		return
	}

	node.state = &newState
	node.Balance = newState.BalanceSet[node.Id]
	node.nodePhase = TradePhase
	node.txSet = make([]Tx, 0)
	for nodeId := range node.state.WithdrawSet {
		if node.Id != nodeId {
			delete(node.otherNodePubKey, nodeId)
		}
		if nodeId == node.Id {
			node.nodePhase = ExitPhase
		}
	}
	node.Unlock()

	// 关闭定时器
	node.ctxCancelFunc()

	// 如果是退出节点
	if _, ok := node.state.WithdrawSet[node.Id]; ok {
		// ctx, _ := context.WithTimeout(context.Background(), time.Second*2)
		// resp, err := cli.Cli.ExitChannel(ctx, &pb.ExitChannelRequest{
		// 	ChanName: node.ChannelName,
		// 	UserId:   node.Id,
		// })
		// if err != nil {
		// 	flog.Errorf("[%s] exit channel failed, err=%v", node.Id, err)
		// }
		params := map[string][]byte{
			"channel_name": []byte(node.ChannelName),
		}
		resp, err := contract.Exit(contract.Cli, config.GetAllConf().ContractName, params)
		if err != nil {
			flog.Error("Commit failed", zap.Error(err))
			return
		}

		node.Exited = true
		flog.Infof("[%s] exit channel success, balance=%s", node.Id, resp)
		return
	}

	// The node is the new leader node
	if strings.EqualFold(node.Id, newState.LeaderId) {
		node.initLeaderNode()
		// set trade timer
		nodeTimer = time.NewTimer(time.Second * time.Duration(TradeTimeout))
		go func() {
			<-nodeTimer.C
			// The timer is triggered, and the consensus phase is entered
			go node.sendConsensusPhaseNotify()
			nodeTimer.Stop()
		}()
	}
}

// getChannelInfo - arbitrate state rollback
func (node *Node) getChannelInfo() {
	flog := logger.GetSugaLogger().With(zap.String("func", "getChannelInfo"),
		zap.String("channel", node.ChannelName), zap.String("userID", node.Id))
	flog.Infof("[%v][%v] begin getChannelInfo", node.Id, node.ChannelName)

	params := map[string][]byte{
		"channel_name": []byte(node.ChannelName),
	}
	resp, err := contract.ChannelInfo(contract.Cli, config.GetAllConf().ContractName, params)
	if err != nil {
		flog.Error("ChannelInfo failed", zap.Error(err))
		return
	}

	flog.Infof("[%v][%v] getChannelInfo resp=%+v", node.Id, node.ChannelName, resp)

	// Status == 2 共识阶段 ChalleageStatus == 2 挑战者成功，进行回滚
	if resp.Status == 2 && resp.ChalleageStatus == 2 {
		node.Lock()
		node.state.LeaderId = resp.LeaderId
		for userId, balance := range resp.BalanceSet {
			node.state.BalanceSet[userId] = balance
		}
		node.Balance = node.state.BalanceSet[node.Id]
		node.nodePhase = TradePhase
		node.Unlock()

		flog.Infof("[%v][%v] getChannelInfo BalanceSet=%+v, node.otherNodePubKey=%+v", node.Id, node.ChannelName,
			node.state.BalanceSet, node.otherNodePubKey)

		// The node is the new leader node
		if strings.EqualFold(node.Id, node.state.LeaderId) {
			params := map[string][]byte{
				"channel_name": []byte(node.ChannelName),
			}

			err = contract.Rollback(contract.Cli, config.GetAllConf().ContractName, params)
			if err != nil {
				flog.Error("Contract Rollback failed", zap.Error(err))
				return
			}

			node.initLeaderNode()
			// set trade timer
			nodeTimer = time.NewTimer(time.Second * time.Duration(TradeTimeout))
			go func() {
				<-nodeTimer.C
				// The timer is triggered, and the consensus phase is entered
				go node.sendConsensusPhaseNotify()
				nodeTimer.Stop()
			}()
		}
		// 关闭定时器
		node.ctxCancelFunc()
	} else if resp.Status == 1 && (resp.ChalleageStatus == 3 || resp.ChalleageStatus == 0) {
		node.Lock()
		node.state.Epoch = resp.Epoch
		node.state.LeaderId = resp.LeaderId
		for userId, balance := range resp.BalanceSet {
			node.state.BalanceSet[userId] = balance
		}
		node.Balance = node.state.BalanceSet[node.Id]
		node.nodePhase = TradePhase
		node.Unlock()

		// 关闭定时器
		node.ctxCancelFunc()
	}
}
