/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package node

import (
	"reflect"
	"testing"

	"chainmaker.org/chainmaker/net-libp2p/libp2pnet"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
	"chainmaker.org/ibpc/offchain-channel-scaling/config"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/contract"

	"github.com/agiledragon/gomonkey/v2"
	. "github.com/smartystreets/goconvey/convey"
)

func TestNode(t *testing.T) {
	config.ConfPath = "../network/testconf/config.yaml"
	config.InitConfig()

	gomonkey.ApplyFunc(contract.UserInfo, func(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp string, err error) {
		resp = "100000"
		return
	})

	var pnet libp2pnet.LibP2pNet
	gomonkey.ApplyMethod(reflect.TypeOf(&pnet), "Start", func(*libp2pnet.LibP2pNet) error {
		return nil
	})
	gomonkey.ApplyMethod(reflect.TypeOf(&pnet), "GetNodeUid", func(*libp2pnet.LibP2pNet) string {
		return nodeId
	})

	// create four node and run
	var err error
	var node1, node2 *Node

	node1, err = CreateNode("chain1")
	if err != nil {
		t.Fatalf("CreateNode1 failed, err=%v", err)
	}

	gomonkey.ApplyFunc(contract.CreateChannel, func(client *sdk.ChainClient, contractName string, params map[string][]byte, withSyncResult bool) (resp *contract.ChannelInfoResp, err error) {
		resp = &contract.ChannelInfoResp{
			LeaderId: node1.Id,
		}
		return
	})

	Convey("CreateNode", t, func() {
		err = node1.CreateChannel(10000)
		if err != nil {
			t.Fatalf("CreateChannel failed, err=%v", err)
		}
		So(err, ShouldBeNil)
	})

	// t.Logf("node1: %+v\n", node1)

	gomonkey.ApplyFunc(contract.JoinChannel, func(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp *contract.ChannelInfoResp, err error) {
		resp = new(contract.ChannelInfoResp)
		resp.LeaderId = node1.Id
		resp.LeaderAddr = node1.NetAddr

		return
	})

	config.ConfPath = "../network/testconf/config1.yaml"
	config.InitConfig()
	node2, err = CreateNode("chain1")
	if err != nil {
		t.Fatalf("CreateNode2 failed, err=%v", err)
	}

	Convey("JoinChannel", t, func() {
		_, err = node2.JoinChannel(1000)
		if err != nil {
			t.Fatalf("JoinChannel failed, err=%v", err)
		}
		So(err, ShouldBeNil)
	})

	Convey("LeaderID", t, func() {
		leaderID := node1.LeaderID()
		So(leaderID, ShouldEqual, node1.state.LeaderId)
	})

	Convey("SetPhase", t, func() {
		node1.SetPhase(TradePhase)

		Convey("Phase", func() {
			phase := node1.Phase()
			So(phase, ShouldEqual, TradePhase)
		})
	})

	Convey("CheckTxId", t, func() {
		exist := node1.CheckTxId("xxx")
		So(exist, ShouldEqual, false)
	})
}
