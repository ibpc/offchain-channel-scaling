/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package node

import (
	"context"
	"encoding/json"
	"reflect"
	"testing"
	"time"

	"chainmaker.org/chainmaker/net-libp2p/libp2pnet"
	"chainmaker.org/ibpc/offchain-channel-scaling/config"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/contract"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/crypto"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/network"

	sdk "chainmaker.org/chainmaker/sdk-go/v2"
	"github.com/agiledragon/gomonkey/v2"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	testConfPath = "../network/testconf/config.yaml"

	nodeId = "kj23h492492hnjk"
)

func TestHandler(t *testing.T) {
	config.ConfPath = testConfPath
	config.InitConfig()

	// create four node and run
	var err error
	var node *Node

	var pnet libp2pnet.LibP2pNet
	gomonkey.ApplyMethod(reflect.TypeOf(&pnet), "Start", func(*libp2pnet.LibP2pNet) error {
		return nil
	})
	gomonkey.ApplyMethod(reflect.TypeOf(&pnet), "GetNodeUid", func(*libp2pnet.LibP2pNet) string {
		return nodeId
	})

	node, err = CreateNode("chain1")
	if err != nil {
		t.Fatalf("CreateNode failed, err=%v", err)
	}

	gomonkey.ApplyFunc(contract.CreateChannel, func(client *sdk.ChainClient, contractName string, params map[string][]byte, withSyncResult bool) (resp *contract.ChannelInfoResp, err error) {
		resp = &contract.ChannelInfoResp{
			LeaderId: node.Id,
		}
		return
	})

	gomonkey.ApplyFunc(contract.UserInfo, func(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp string, err error) {
		resp = "100000"
		return
	})

	gomonkey.ApplyFunc(contract.Challeage, func(client *sdk.ChainClient, contractName string, params map[string][]byte) error {
		return nil
	})

	gomonkey.ApplyFunc(contract.Commit, func(client *sdk.ChainClient, contractName string, params map[string][]byte) error {
		return nil
	})

	gomonkey.ApplyFunc(contract.Exit, func(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp string, err error) {
		resp = "100000"
		return
	})

	gomonkey.ApplyFunc(contract.ChannelInfo, func(client *sdk.ChainClient, contractName string, params map[string][]byte) (resp *contract.ChannelInfoResp, err error) {
		resp = new(contract.ChannelInfoResp)
		resp.LeaderId = node.Id
		resp.LeaderAddr = node.NetAddr
		resp.Status = 1
		resp.ChalleageStatus = 0

		return
	})

	node.ctx, node.ctxCancelFunc = context.WithCancel(context.Background())

	err = node.CreateChannel(10000)
	if err != nil {
		t.Fatalf("CreateChannel failed, err=%v", err)
	}

	gomonkey.ApplyFunc(network.SendMsg, func(chanName string, node *libp2pnet.LibP2pNet, msg *network.Message, receiveId string) error {
		return nil
	})

	pubkeyBytes, err := node.keyPair.PublicKey.Bytes()
	if err != nil {
		t.Fatalf("node.keyPair.PublicKey.Bytes failed, err=%v", err)
	}

	msgData, err := json.Marshal(JoinData{
		KeyType: node.keyPair.PublicKey.Type(),
		PubKey:  pubkeyBytes,
		Seed:    node.NetAddr,
	})

	if err != nil {
		t.Fatalf("son.Marshal failed, err=%v", err)
	}

	var otherId = "xxx"

	Convey("JoinTest", t, func() {
		Convey("handlerPing", func() {
			So(func() {
				node.handlerPing(nil, otherId)
			}, ShouldNotPanic)
		})

		Convey("handlerJoinRequest", func() {
			node.nodePhase = ConsensusPhase
			So(func() {
				node.handlerJoinRequest(msgData, otherId)
			}, ShouldNotPanic)

			node.nodePhase = TradePhase

			So(func() {
				node.handlerJoinRequest(msgData, otherId)
			}, ShouldNotPanic)

			node.state.LeaderId = "xxxx"

			So(func() {
				node.handlerJoinRequest(msgData, "yyy")
			}, ShouldNotPanic)
		})

		Convey("handlerPubKeySyncRequest", func() {
			So(func() {
				node.handlerPubKeySyncRequest(msgData, otherId)
			}, ShouldNotPanic)
		})
	})

	Convey("TxTest", t, func() {
		// Generate a transaction ID (UUID)
		txId := uuid.New().String()
		// sender signs tx
		signTx := Tx{
			Id:       txId,
			Epoch:    node.state.Epoch,
			Sender:   otherId,
			Receiver: node.Id,
			Amount:   1,
		}
		sig, err := crypto.SignTx(node.keyPair.PrivateKey, node.nodeSignOpts, signTx)
		So(err, ShouldBeNil)
		signTx.SenderSig = sig
		signTx.StartTime = time.Now()
		signTx.ReceiverSig = sig
		signTx.LeaderSig = sig

		data, _ := json.Marshal(signTx)

		Convey("handlerTxRequest", func() {
			node.SetPhase(TradePhase)

			So(func() {
				node.handlerTxRequest(data, otherId)
			}, ShouldNotPanic)
		})

		Convey("handlerTxCommit", func() {
			signTx.Sender = node.Id
			signTx.Receiver = otherId

			So(func() {
				node.handlerTxCommit(data, otherId)
			}, ShouldNotPanic)
		})

		Convey("handlerTxNotify", func() {
			node.state.LeaderId = node.Id
			So(func() {
				node.handlerTxNotify(data, otherId)
			}, ShouldNotPanic)
		})

		Convey("handlerTradePhaseNotify", func() {
			So(func() {
				node.handlerTradePhaseNotify(nil, otherId)
			}, ShouldNotPanic)
		})
	})

	Convey("StateTest", t, func() {
		newState := State{
			Epoch:       node.state.Epoch + 1,
			LeaderId:    node.Id,
			BalanceSet:  make(map[string]int),
			SigSet:      make(map[string][]byte),
			WithdrawSet: make(map[string]int),
		}
		node.leader.newState = newState

		signState := State{Epoch: newState.Epoch, LeaderId: newState.LeaderId, BalanceSet: newState.BalanceSet, WithdrawSet: newState.WithdrawSet}
		sig, err := crypto.SignTx(node.keyPair.PrivateKey, node.nodeSignOpts, signState)
		So(err, ShouldBeNil)

		newState.SigSet[node.Id] = sig
		newState.SigSet[otherId] = sig

		data, err := json.Marshal(newState)
		So(err, ShouldBeNil)

		Convey("handlerUpdateStateRequest", func() {
			node.state.LeaderId = otherId

			So(func() {
				node.handlerUpdateStateRequest(data, otherId)
			}, ShouldNotPanic)

			node.Balance = 0
			So(func() {
				node.handlerUpdateStateRequest(data, otherId)
			}, ShouldNotPanic)
		})

		Convey("handlerUpdateStateReply", func() {
			node.leader.newState.SigSet["bbb"] = []byte("bbb")
			node.leader.newState.SigSet["ccc"] = []byte("ccc")
			So(func() {
				node.handlerUpdateStateReply(sig, node.Id)
			}, ShouldNotPanic)
		})

		withdrawState := State{
			Epoch:       node.state.Epoch + 1,
			BalanceSet:  make(map[string]int),
			WithdrawSet: map[string]int{node.Id: 1},
		}
		withdrawsig, err := crypto.SignTx(node.keyPair.PrivateKey, node.nodeSignOpts, withdrawState)
		So(err, ShouldBeNil)

		withdrawState.SigSet = map[string][]byte{node.Id: withdrawsig}
		withdrawsigData, err := json.Marshal(withdrawState)
		So(err, ShouldBeNil)

		Convey("handlerUpdateStateConfirm", func() {
			So(func() {
				node.handlerUpdateStateConfirm(data, otherId)
			}, ShouldNotPanic)

			node.state.LeaderId = otherId

			So(func() {
				node.handlerUpdateStateConfirm(withdrawsigData, otherId)
			}, ShouldNotPanic)
		})

		Convey("getChannelInfo", func() {
			So(func() {
				node.getChannelInfo()
			}, ShouldNotPanic)
		})
	})
}
