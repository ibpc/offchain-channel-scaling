/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package crypto

import (
	"bytes"
	"testing"

	"chainmaker.org/ibpc/offchain-channel-scaling/config"
)

const (
	testConfigPaht = "../network/testconf/config.yaml"
)

func TestParsePublicKey(t *testing.T) {
	config.ConfPath = testConfigPaht
	config.InitConfig()

	allConf := config.GetAllConf()
	if allConf == nil {
		t.Error("allConf is nil")
		return
	}

	t.Logf("user_config: %+v", *allConf.UserConfig)

	keyPair, _, err := GetKeyPair(allConf.AuthType, allConf.UserConfig)
	if err != nil {
		t.Errorf("GetKeyPair failed, %v", err)
		return
	}

	pubData, err := keyPair.PublicKey.Bytes()
	if err != nil {
		t.Errorf("keyPair.PublicKey.Bytes failed, %v", err)
		return
	}

	pubKey, err := ParsePublicKey(keyPair.KeyType, pubData)
	if err != nil {
		t.Errorf("ParsePublicKey failed, %v", err)
		return
	}

	parsePubData, err := pubKey.Bytes()
	if err != nil {
		t.Errorf("pubKey.Bytes() failed, %v", err)
		return
	}

	if !bytes.Equal(pubData, parsePubData) {
		t.Error("Compare failed")
		return
	}
}

func TestSignAndVerify(t *testing.T) {
	config.ConfPath = testConfigPaht
	config.InitConfig()

	allConf := config.GetAllConf()
	if allConf == nil {
		t.Error("allConf is nil")
		return
	}

	t.Logf("user_config: %+v", *allConf.UserConfig)

	keyPair, opts, err := GetKeyPair(allConf.AuthType, allConf.UserConfig)
	if err != nil {
		t.Errorf("GetKeyPair failed, %v", err)
		return
	}

	t.Logf("opts: %+v", *opts)

	var testStut = struct {
		A string
	}{A: "aaa"}

	data, err := SignTx(keyPair.PrivateKey, opts, testStut)
	if err != nil {
		t.Errorf("SignTx failed, %v", err)
		return
	}

	t.Logf("data: %+v", data)

	ok, err := VerifyTX(data, testStut, keyPair.PublicKey, opts)
	if !ok || err != nil {
		t.Errorf("VerifyTX failed, %v", err)
		return
	}
}
