/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package sha256

import (
	"encoding/hex"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

// _byteArray is a helper to turn a string in to a byte array
func _byteArray(input string) []byte {
	x, _ := hex.DecodeString(input)
	return x
}

func TestHash(t *testing.T) {
	var tests = []struct {
		data   []byte
		output []byte
	}{
		{ // 0
			data:   _byteArray("e9e0083e456539e9f6336164cd98700e668178f98af147ef750eb90afcf2f637"),
			output: _byteArray("a79f7ccca7a308d522390d816368157ce8b74ae7546bfde3d8207de440bf9717"),
		},
	}

	hash := New()
	Convey("TestHash", t, func() {
		for _, test := range tests {
			output := hash.Hash(test.data)
			// t.Logf("output: %s", hex.EncodeToString(output))
			So(output, ShouldResemble, test.output)
		}
	})

}

func TestMultiHash(t *testing.T) {
	var tests = []struct {
		data1  []byte
		data2  []byte
		data3  []byte
		data4  []byte
		output []byte
	}{
		{ // 0
			data1:  _byteArray("e9e0083e456539e9"),
			data2:  _byteArray("f6336164cd98700e"),
			data3:  _byteArray("668178f98af147ef"),
			data4:  _byteArray("750eb90afcf2f637"),
			output: _byteArray("a79f7ccca7a308d522390d816368157ce8b74ae7546bfde3d8207de440bf9717"),
		},
	}

	hash := New()
	Convey("TestHash", t, func() {
		for _, test := range tests {
			output := hash.Hash(test.data1, test.data2, test.data3, test.data4)
			// t.Logf("output: %s", hex.EncodeToString(output))
			So(output, ShouldResemble, test.output)
		}
	})
}

func TestHashLength(t *testing.T) {
	Convey("TestHashLength", t, func() {
		So(_hashlength, ShouldEqual, New().HashLength())
	})
}
