/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0

Copyright © 2018, 2019 Weald Technology Trading
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Package merkletree is an implementation of a Merkle tree (https://en.wikipedia.org/wiki/Merkle_tree).
It provides methods to create a tree and generate and verify proofs.  The hashing algorithm for the
tree is selectable between BLAKE2b and Keccak256, or you can supply your own.

This implementation includes advanced features salting and pollarding.  Salting is the act of
adding a piece of data to each value in the Merkle tree as it is initially hashed to form the
leaves, which helps avoid rainbow table attacks on leaf hashes presented as part of proofs.
Pollarding is the act of providing the root plus all branches to a certain height which can be
used to reduce the size of proofs.  This is useful when multiple proofs are presented against
the same tree as it can reduce the overall size.

Creating a Merkle tree requires a list of values that are each byte arrays.  Once a tree has
been created proofs can be generated using the tree's GenerateProof() function.

The package includes a function VerifyProof() to verify a generated proof given only the data
to prove, proof and the pollard of the relevant Merkle tree.  This allows for efficient verification
of proofs without requiring the entire Merkle tree to be stored or recreated.


Implementation notes


The tree pads its values to the next highest power of 2; values not supplied are treated as null
with a value hash of 0.  This can be seen graphically by generating a DOT representation of the
graph with DOT().

If salting is enabled it appends an 4-byte value to each piece of data.  The value is the binary
representation of the index in big-endian form.  Note that if there are more than 2^32 values in
the tree the salt will wrap, being modulo 2^32
*/

package merkletree

import (
	"fmt"
	"testing"
)

func TestMerkleTree(t *testing.T) {
	data1 := [][]byte{
		[]byte("Foo"),
		[]byte("Bar"),
		[]byte("Baz"),
		[]byte("Caz"),
		[]byte("Daz"),
		[]byte("Qaz"),
	}

	data2 := [][]byte{
		[]byte("Foo"),
		[]byte("Bar"),
		[]byte("Baz"),
		[]byte("Caz"),
		[]byte("Daz"),
		[]byte("Qaz"),
	}

	// Create the tree
	tree1, err := New(data1)
	if err != nil {
		t.Fatalf("NewMerkleTree failed, err=%v", err)
	}

	// Create the tree
	tree2, err := New(data2)
	if err != nil {
		t.Fatalf("NewMerkleTree failed, err=%v", err)
	}

	lenE, e, idx := tree1.LeafDiff(tree2)

	fmt.Println(lenE, e, idx)
}

func TestMerkleTree2(t *testing.T) {
	data1 := [][]byte{
		[]byte("Foo"),
		[]byte("Bar"),
		[]byte("Baz"),
		[]byte("Caz"),
		[]byte("Daz"),
		[]byte("Qaz"),
	}

	data2 := [][]byte{
		[]byte("Foo"),
		[]byte("Bar"),
		[]byte("Baz"),
		[]byte("Caz"),
		[]byte("Qaz"),
		[]byte("Daz"),
	}

	// Create the tree
	tree1, err := New(data1)
	if err != nil {
		t.Fatalf("NewMerkleTree failed, err=%v", err)
	}

	// Create the tree
	tree2, err := New(data2)
	if err != nil {
		t.Fatalf("NewMerkleTree failed, err=%v", err)
	}

	lenE, e, idx := tree1.LeafDiff(tree2)

	fmt.Println(lenE, e, idx)
}

func TestMerkleTree3(t *testing.T) {
	data1 := [][]byte{
		[]byte("Foo"),
		[]byte("Bar"),
		[]byte("Baz"),
		[]byte("Caz"),
		[]byte("Daz"),
		[]byte("Qaz"),
	}

	data2 := [][]byte{
		[]byte("Foo"),
		[]byte("Bar"),
		[]byte("嘻嘻嘻"),
		[]byte("Caz"),
		[]byte("Qaz"),
		[]byte("Daz"),
	}

	// Create the tree
	tree1, err := New(data1)
	if err != nil {
		t.Fatalf("NewMerkleTree failed, err=%v", err)
	}

	// Create the tree
	tree2, err := New(data2)
	if err != nil {
		t.Fatalf("NewMerkleTree failed, err=%v", err)
	}

	lenE, e, idx := tree1.LeafDiff(tree2)

	fmt.Println(lenE, e, idx)
}

func TestMerkleTree4(t *testing.T) {
	data1 := [][]byte{
		[]byte("Foo"),
		[]byte("Bar"),
		[]byte("Baz"),
		[]byte("Caz"),
		[]byte("Daz"),
		[]byte("Qaz"),
	}

	data2 := [][]byte{
		[]byte("Foo"),
		[]byte("Bar"),
		[]byte("Baz"),
		[]byte("Caz"),
		[]byte("Qaz"),
		[]byte("Daz"),
		[]byte("xxx"),
	}

	// Create the tree
	tree1, err := New(data1)
	if err != nil {
		t.Fatalf("NewMerkleTree failed, err=%v", err)
	}

	// Create the tree
	tree2, err := New(data2)
	if err != nil {
		t.Fatalf("NewMerkleTree failed, err=%v", err)
	}

	lenE, e, idx := tree1.LeafDiff(tree2)

	fmt.Println(lenE, e, idx)
}
