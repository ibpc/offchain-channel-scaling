/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain 
and Privacy Computing . All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/

pipeline {
    agent any
    environment {
        GOPROXY="https://goproxy.io,direct"
        PATH="$PATH:$GOROOT/bin:$GOPATH/bin"
        GIT_BRACH_NAME= "${gitlabSourceBranch}"
    }
    stages {
        stage("Prepare") {
            steps {
                git branch: env.GIT_BRACH_NAME, credentialsId: '9d6dafa2-177e-4987-8da8-446cc26bf397', url:'https://git.code.tencent.com/IBPC/02-cap-expansion.git'
                script {
                    env.GIT_COMMIT = sh (script: 'git rev-parse HEAD', returnStdout: true).trim()
                    env.GIT_EMAIL = sh (script: 'git log -1 --pretty=format:"%ce"', returnStdout: true).trim()
                    env.GIT_COMMITTER = sh (script: 'git log -1 --pretty=format:"%cn %ce"', returnStdout: true).trim()
                }
            }
        }
        stage('Update Status') {
            steps{
                sh '''
                    curl https://git.code.tencent.com/api/v3/projects/IBPC%2F02-cap-expansion/commit/${GIT_COMMIT}/statuses -v -X POST --header "PRIVATE-TOKEN: xxxxxx" --header "Content-Type: application/json" -d '{"state": "pending","target_url": "http://jenkins.chainmaker.org.cn/project/chainmaker-offchain/'${BUILD_NUMBER}'/console","description": "building","context": "jenkins","block": false}'
                '''
            }
        }
        stage("Build") {
            steps {
            sh  ''' 
                make build
                '''
            }
        }
        stage('Parallel Stage') {
            parallel {
                stage('UT') {
                    steps {
                        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                            sh 'go test -coverprofile cover.out ./module/node/ ./module/network/ ./module/crypto/ ./module/noeq/'
        
                             script {
                                env.UT_COVERAGE=sh (script: 'go tool cover -func=cover.out | tail -1 | grep -P "\\d+\\.\\d+(?=\\%)" -o', returnStdout: true).trim()
                                env.COMMENT_COVERAGE=sh (script: "gocloc --include-lang=Go --output-type=json --not-match=_test.go . | jq '(.total.comment-.total.files*6)/(.total.code+.total.comment)*100'", returnStdout: true).trim()
                            }
                            sh '''
                            total=$(go tool cover -func=cover.out | tail -1)
coverage=$(echo ${total} | grep -P "\\d+\\.\\d+(?=\\%)" -o) 
echo "单测覆盖率: $coverage%" 
threshold=40
(( $(awk 'BEGIN {print ("'${coverage}'" >= "'${threshold}'")}') )) || (echo "单测覆盖率: $coverage% 低于$threshold%"; exit 1)
(( $(awk "BEGIN {print (${COMMENT_COVERAGE} >= 10)}") )) || (echo "注释覆盖率: ${COMMENT_COVERAGE} 低于 10%"; exit 1)
                            '''
                        }
                        script {
                            if (currentBuild.result=='FAILURE') {  env.BUILD_STATUS = "failed" }
                        }
                    }
                }
                stage('Lint') {
                    steps {
                        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                            sh '''
                                golangci-lint run ./...
                             '''
                        }
                        script {
                            if (currentBuild.result=='FAILURE') {  env.BUILD_STATUS = "failed" }
                        }
                    }
                }
            }
        }
    }
    
    post {
        aborted {
            sh '''
                    curl https://git.code.tencent.com/api/v3/projects/IBPC%2F02-cap-expansion/commit/${GIT_COMMIT}/statuses -v -X POST --header "PRIVATE-TOKEN: xxxxxx" --header "Content-Type: application/json" -d '{"state": "failure","target_url": "http://jenkins.chainmaker.org.cn/project/chainmaker-offchain/'${BUILD_NUMBER}'/console","description": "build abort","context": "jenkins","block": false}'
                '''
        }
        failure {
            sh '''
                    curl https://git.code.tencent.com/api/v3/projects/IBPC%2F02-cap-expansion/commit/${GIT_COMMIT}/statuses -v -X POST --header "PRIVATE-TOKEN: xxxxxx" --header "Content-Type: application/json" -d '{"state": "error","target_url": "http://jenkins.chainmaker.org.cn/project/chainmaker-offchain/'${BUILD_NUMBER}'/console","description": "build error","context": "jenkins","block": false}'
                '''
                emailext body: "本次提交CI构建失败。 请点击以下链接查看错误详情：${BUILD_URL}console",
                subject: "CI构建${JOB_NAME}失败",
                to: "${GIT_EMAIL}"
        }
        success {
            sh '''
                    curl https://git.code.tencent.com/api/v3/projects/IBPC%2F02-cap-expansion/commit/${GIT_COMMIT}/statuses -v -X POST --header "PRIVATE-TOKEN: xxxxxx" --header "Content-Type: application/json" -d '{"state": "success","target_url": "http://jenkins.chainmaker.org.cn/project/chainmaker-offchain/'${BUILD_NUMBER}'/console","description": "build success","context": "jenkins","block": false}'
                '''
        }
    }
}
