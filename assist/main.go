/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/ibpc/offchain-channel-scaling/config"
	"chainmaker.org/ibpc/offchain-channel-scaling/module/contract"
)

var (
	contractName      = "channel_contract"
	stakeContractName = "stake_contract"
	contractPath      = "./testdata/contract/channel_contract.7z"
	stakeContractPath = "./testdata/contract/stake_contract.7z"
)

func main() {
	config.ConfPath = "./config/config.yaml"
	config.InitConfig()

	cli, err := contract.CreateChainClientWithSDKConf(config.GetAllConf().SdkConfFile)
	if err != nil {
		fmt.Printf("Contract cli sdk failed, err=%v", err)
		return
	}

	fmt.Println("Please choose deploy contract:")
	fmt.Println("1. channel contract")
	fmt.Println("2. stake contract")
	fmt.Println("3. both of all")
	fmt.Print("Input option number: ")

	var inputStr string
	num, err := fmt.Scan(&inputStr)
	if num == 0 || err != nil {
		fmt.Printf("Scan failed, num=%d, err=%v", num, err)
		return
	}

	if inputStr != "1" && inputStr != "2" && inputStr != "3" {
		fmt.Printf("Input failed, input=%s", inputStr)
		return
	}

	usernames := []string{contract.UserNameOrg1Admin1, contract.UserNameOrg2Admin1,
		contract.UserNameOrg3Admin1, contract.UserNameOrg4Admin1}

	var txId string
	if inputStr == "1" || inputStr == "3" {
		// 创建通道合约
		txId, err = contract.CreateContract(cli, contractName, contract.Version, contractPath, true, usernames...)
		if err != nil {
			fmt.Printf("CreateChannelContract failed, err=%v", err)
			return
		}

		var info *common.TransactionInfo
		info, err = cli.GetTxByTxId(txId)
		if err != nil {
			fmt.Printf("GetTxByTxId failed, err=%v", err)
			return
		}

		fmt.Printf("result code:%d, msg:%s\n", info.Transaction.Result.Code, info.Transaction.Result.Code.String())
		fmt.Printf("contract result code:%d, msg:%s\n",
			info.Transaction.Result.ContractResult.Code, info.Transaction.Result.ContractResult.Message)
	}

	if inputStr == "2" || inputStr == "3" {
		// 创建权益合约
		txId, err = contract.CreateContract(cli, stakeContractName, contract.Version, stakeContractPath, true, usernames...)
		if err != nil {
			fmt.Printf("CreateStakeContract failed, err=%v", err)
			return
		}

		var info *common.TransactionInfo
		info, err = cli.GetTxByTxId(txId)
		if err != nil {
			fmt.Printf("GetTxByTxId failed, err=%v", err)
			return
		}

		fmt.Printf("result code:%d, msg:%s\n", info.Transaction.Result.Code, info.Transaction.Result.Code.String())
		fmt.Printf("contract result code:%d, msg:%s\n",
			info.Transaction.Result.ContractResult.Code, info.Transaction.Result.ContractResult.Message)
	}
}
