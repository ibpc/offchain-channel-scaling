/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package common

const (
	// TradeMsgFail 交易失败
	TradeMsgFail = 0
	// TradeMsgOk 交易成果
	TradeMsgOk = 1
)
