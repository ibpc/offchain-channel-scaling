/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"log"

	"channel-contract/channel"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sandbox"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// ChannelContract 链下通道合约名称
type ChannelContract struct {
}

func (f *ChannelContract) InitContract() protogo.Response {
	return sdk.Success([]byte("Init contract success"))
}

func (f *ChannelContract) UpgradeContract() protogo.Response {
	return sdk.Success([]byte("Upgrade contract success"))
}

func (f *ChannelContract) InvokeContract(method string) protogo.Response {
	switch method {
	case "bind":
		return channel.Bind()
	case "create":
		return channel.CreateChannel()
	case "join":
		return channel.JoinChannel()
	case "exchange":
		return channel.BeginExchange()
	case "commit":
		return channel.Commit()
	case "challeage":
		return channel.Challeage()
	case "getchalleage":
		return channel.GetChalleage()
	case "answer":
		return channel.Answer()
	case "chaninfo":
		return channel.GetChannelInfo()
	case "userinfo":
		return channel.GetUserInfo()
	case "rollback":
		return channel.Rollback()
	case "exit":
		return channel.Exit()
	default:
		return sdk.Error("invalid method")
	}
}

// sdk代码中，有且仅有一个main()方法
func main() {
	// main()方法中，下面的代码为必须代码，不建议修改main()方法当中的代码
	// 其中，TestContract为用户实现合约的具体名称
	err := sandbox.Start(new(ChannelContract))
	if err != nil {
		log.Fatal(err)
	}
}
