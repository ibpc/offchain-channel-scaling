/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0

Copyright © 2018, 2019 Weald Technology Trading
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package merkletree

import (
	"fmt"
	"testing"
)

func TestMerkleTree(t *testing.T) {
	data1 := [][]byte{
		[]byte("Foo"),
		[]byte("Bar"),
		[]byte("Baz"),
		[]byte("Caz"),
		[]byte("Daz"),
		[]byte("Qaz"),
	}

	data2 := [][]byte{
		[]byte("Foo"),
		[]byte("Bar"),
		[]byte("Baz"),
		[]byte("Caz"),
		[]byte("Daz"),
		[]byte("Qaz"),
	}

	// Create the tree
	tree1, err := New(data1)
	if err != nil {
		t.Fatalf("NewMerkleTree failed, err=%v", err)
	}

	// Create the tree
	tree2, err := New(data2)
	if err != nil {
		t.Fatalf("NewMerkleTree failed, err=%v", err)
	}

	lenE, e, idx := tree1.LeafDiff(tree2)

	fmt.Println(lenE, e, idx)
}
