/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0

Copyright © 2018, 2019 Weald Technology Trading
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Package merkletree is an implementation of a Merkle tree (https://en.wikipedia.org/wiki/Merkle_tree).
It provides methods to create a tree and generate and verify proofs.  The hashing algorithm for the
tree is selectable between BLAKE2b and Keccak256, or you can supply your own.

This implementation includes advanced features salting and pollarding.  Salting is the act of
adding a piece of data to each value in the Merkle tree as it is initially hashed to form the
leaves, which helps avoid rainbow table attacks on leaf hashes presented as part of proofs.
Pollarding is the act of providing the root plus all branches to a certain height which can be
used to reduce the size of proofs.  This is useful when multiple proofs are presented against
the same tree as it can reduce the overall size.

Creating a Merkle tree requires a list of values that are each byte arrays.  Once a tree has
been created proofs can be generated using the tree's GenerateProof() function.

The package includes a function VerifyProof() to verify a generated proof given only the data
to prove, proof and the pollard of the relevant Merkle tree.  This allows for efficient verification
of proofs without requiring the entire Merkle tree to be stored or recreated.


Implementation notes


The tree pads its values to the next highest power of 2; values not supplied are treated as null
with a value hash of 0.  This can be seen graphically by generating a DOT representation of the
graph with DOT().

If salting is enabled it appends an 4-byte value to each piece of data.  The value is the binary
representation of the index in big-endian form.  Note that if there are more than 2^32 values in
the tree the salt will wrap, being modulo 2^32
*/

package merkletree

import (
	"bytes"
	"errors"
	"fmt"
	"math"

	"channel-contract/merkletree/sha256"
)

// MerkleTree is the structure for the Merkle tree.
type MerkleTree struct {
	// data is the data from which the Merkle tree is created
	Data [][]byte
	// nodes are the leaf and branch nodes of the Merkle tree
	Nodes [][]byte
}

func (t *MerkleTree) indexOf(input []byte) (uint64, error) {
	for i, data := range t.Data {
		if bytes.Equal(data, input) {
			return uint64(i), nil
		}
	}
	return 0, errors.New("data not found")
}

// LeafDiff 与另一个默克尔树对比，查找不同的 index
// lenEqual 两个默克尔树数据长度相同
// equal 表示相同 idx 的数据相同，无论数据长度是否相同
// 例如：[1,2,3,4,5] [1,2,3,4] 这两个 equal 为 true
// [1,2,3,2,5] [1,2,3,4] equal 为 false
// index 为不相等时的索引
func (t *MerkleTree) LeafDiff(other *MerkleTree) (lenEqual, equal bool, index int64) {
	if len(t.Data) == 0 || len(other.Data) == 0 {
		lenEqual = len(t.Data) == len(other.Data)
		equal = true
		index = 0
		return
	}

	if bytes.Equal(t.Root(), other.Root()) {
		lenEqual = true
		equal = true
		index = -1
		return
	}
	if len(t.Data) == len(other.Data) {
		lenEqual = true
	}

	// 默克尔树高度不同表示数据量不同，则直接对比叶子节点
	// 相同则可以通过树查找
	if t.Height() == other.Height() {
		idx := 2
		nodeLen := len(t.Nodes)
		for idx < nodeLen {
			if bytes.Equal(t.Nodes[idx], other.Nodes[idx]) {
				idx++
				continue
			}

			if idx*2 < nodeLen {
				idx = idx * 2
			} else {
				break
			}
		}
		// 算出默克尔树 idx 对应数据 idx
		idx = idx - nodeLen/2
		if idx >= len(t.Data) || idx >= len(other.Data) {
			equal = true
		}
		index = int64(idx)
		return
	}

	// 对比叶子节点
	tLeft := t.Leaf()
	oLeft := other.Leaf()
	idx := 0
	for ; idx < len(t.Data) && idx < len(other.Data); idx++ {
		if !bytes.Equal(tLeft[idx], oLeft[idx]) {
			break
		}
	}
	if idx >= len(t.Data) || idx >= len(other.Data) {
		equal = true
	}
	index = int64(idx)
	return
}

// New creates a new Merkle tree using the provided raw data and default hash type.  Salting is not used.
// data must contain at least one element for it to be valid.
func New(data [][]byte) (*MerkleTree, error) {
	myHash := sha256.New()
	if len(data) == 0 {
		return nil, errors.New("tree must have at least 1 piece of data")
	}

	branchesLen := int(math.Exp2(math.Ceil(math.Log2(float64(len(data))))))

	// We pad our data length up to the power of 2
	nodes := make([][]byte, branchesLen+len(data)+(branchesLen-len(data)))
	for i := range data {
		nodes[i+branchesLen] = myHash.Hash(data[i])
	}
	for i := len(data) + branchesLen; i < len(nodes); i++ {
		nodes[i] = make([]byte, myHash.HashLength())
	}
	// Branches
	for i := branchesLen - 1; i > 0; i-- {
		nodes[i] = myHash.Hash(nodes[i*2], nodes[i*2+1])
	}

	tree := &MerkleTree{
		Nodes: nodes,
		Data:  data,
	}

	return tree, nil
}

// Pollard returns the Merkle root plus branches to a certain height.  Height 0 will return just the root, height 1 the root plus
// the two branches directly above it, height 2 the root, two branches directly above it and four branches directly above them, etc.
func (t *MerkleTree) Pollard(height int) [][]byte {
	return t.Nodes[1:int(math.Exp2(float64(height+1)))]
}

// Root returns the Merkle root (hash of the root node) of the tree.
func (t *MerkleTree) Datas() [][]byte {
	return t.Data
}

// Root returns the Merkle root (hash of the root node) of the tree.
func (t *MerkleTree) Leaf() [][]byte {
	branchesLen := int(math.Exp2(math.Ceil(math.Log2(float64(len(t.Data))))))
	if len(t.Nodes) < branchesLen {
		return nil
	}
	return t.Nodes[branchesLen:]
}

// Root returns the Merkle root (hash of the root node) of the tree.
func (t *MerkleTree) Root() []byte {
	return t.Nodes[1]
}

// Height returns the height of the Merkle tree.
func (t *MerkleTree) Height() int {
	return int(math.Ceil(math.Log2(float64(len(t.Data)))))
}

// String implements the stringer interface
func (t *MerkleTree) String() string {
	return fmt.Sprintf("%x", t.Nodes[1])
}
