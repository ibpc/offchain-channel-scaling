#!/bin/bash
#
# Copyright (C) BABEC. All rights reserved.
# Copyright (C) Beijing Advanced Innovation Center for Future Blockchain 
# and Privacy Computing. All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

contract_name='channel_contract'
go build -ldflags="-s -w" -o $contract_name
7z a $contract_name $contract_name
mv $contract_name.7z ../testdata/contract/

stake_contract_name='stake_contract'
go build -ldflags="-s -w" -o $stake_contract_name ./stake
7z a $stake_contract_name $stake_contract_name
mv $stake_contract_name.7z ../testdata/contract/