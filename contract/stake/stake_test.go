/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"encoding/base64"
	"testing"

	"channel-contract/common"
	"channel-contract/mock"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
	"github.com/agiledragon/gomonkey/v2"
)

// TestInitUser 测试合约初始化
func TestInitUser(t *testing.T) {
	args := make(map[string][]byte)
	args["channel_name"] = []byte("channel1")
	args["user_id"] = []byte("user_id1")
	args["user_address"] = []byte("user_address1")

	pubkey := "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEywSJ5aRfDQ5FG76D4/FDJirX4SiGdtZQcSRrWRmCPUHWiTDJkmTi9KAy8Kdf1ygdsb5pmUrtNSZRFqtQhMs40w=="
	pubkeyBytes, err := base64.StdEncoding.DecodeString(pubkey)
	if err != nil {
		t.Fatal("base64.StdEncoding.DecodeString faield, ", err)
	}

	args["user_pubkey"] = pubkeyBytes
	args["key_type"] = []byte("8")
	args["hash_type"] = []byte("5")
	args["max_user"] = []byte("50")
	args["balance"] = []byte("1000")

	sdk.Instance = &mock.SDKInatanceMock{
		Args: args,
	}

	// gomonkey.ApplyFunc(getUsers, func(channelName string) (Users, error) {
	// 	var user = Users{}
	// 	return user, nil
	// })

	var contract = new(StakeContract)

	resp := contract.InitContract()
	if resp.Status == 0 {
		t.Logf("Payload=%s", string(resp.Payload))
		return
	}

	t.Fatalf("resp=%+v, Payload=%s", resp, string(resp.Payload))
}

// TestTransfer 测试转账
func TestTransfer(t *testing.T) {
	args := make(map[string][]byte)
	args["from"] = []byte(common.ChannelContract)
	args["to"] = []byte("xxx")
	args["rand_key"] = []byte("ttt")
	args["num"] = []byte("100")

	sdk.Instance = &mock.SDKInatanceMock{
		Args: args,
	}

	gomonkey.ApplyFunc(getUsers, func() (Users, error) {
		var user = Users{
			RandKey: "ttt",
			Users: map[string]User{
				"xxx": {},
			},
		}
		return user, nil
	})

	var contract = new(StakeContract)

	resp := contract.Transfer()
	if resp.Status == 0 {
		t.Logf("Payload=%s", string(resp.Payload))
		return
	}

	t.Fatalf("resp=%+v, Payload=%s", resp, string(resp.Payload))
}

// TestBalanceOf 测试获取用户余额
func TestBalanceOf(t *testing.T) {
	args := make(map[string][]byte)
	args["from"] = []byte("xxx")

	sdk.Instance = &mock.SDKInatanceMock{
		Args: args,
	}

	gomonkey.ApplyFunc(getUsers, func() (Users, error) {
		var user = Users{
			Users: map[string]User{
				"xxx": {
					UserPK:  "xxx",
					Balance: 100,
				},
			},
		}
		return user, nil
	})

	var contract = new(StakeContract)

	resp := contract.BalanceOf()
	if resp.Status == 0 {
		t.Logf("Payload=%s", string(resp.Payload))
		return
	}

	t.Fatalf("resp=%+v, Payload=%s", resp, string(resp.Payload))
}
