/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"math/rand"
	"strconv"

	"channel-contract/common"
	inCrypto "channel-contract/crypto"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sandbox"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// StakeContract 权益合约
type StakeContract struct {
}

// 用户信息
type User struct {
	UserID      string           `json:"user_id"`
	UserPK      string           `json:"user_pk"`
	Balance     int64            `json:"balance"`
	PubKeyBytes []byte           `json:"pubkey_bytes"`
	KeyType     crypto.KeyType   `json:"key_type"`
	SignOpts    *crypto.SignOpts `json:"sign_opts"`
}

type Users struct {
	RandKey   string            `json:"rand_key"`
	Users     map[string]User   `json:"users"`
	UserPk2ID map[string]string `json:"userpk2id"`
}

const (
	DefaultBalance = 10000000
)

func (f *StakeContract) InitContract() protogo.Response {
	funcName := "InitContract"
	opentimeStamp, err := sdk.Instance.GetTxTimeStamp()
	if err != nil {
		errStr := fmt.Sprintf("[%s] sdk.Instance.GetTxTimeStamp failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}
	openTime, err := strconv.Atoi(opentimeStamp)
	if err != nil {
		errStr := fmt.Sprintf("[%s] Atoi opentimeStamp failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	rand.Seed(int64(openTime))
	randKey := rand.Int63n(math.MaxInt64)

	users, err := getUsers()
	if err != nil {
		errStr := fmt.Sprintf("[%s] getUsers failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	users.RandKey = fmt.Sprint(randKey)
	err = setUsers(users)
	if err != nil {
		errStr := fmt.Sprintf("[%s] setUsers failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	resp := sdk.Instance.CallContract(common.ChannelContract, "bind", map[string][]byte{
		"rand_key": []byte(fmt.Sprint(randKey)),
	})

	if resp.Status != 0 {
		errStr := fmt.Sprintf("[%s] call %s bind failed, resp=%+v", funcName, common.ChannelContract, resp)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}
	return sdk.Success([]byte("Init contract success"))
}

func (f *StakeContract) UpgradeContract() protogo.Response {
	return sdk.Success([]byte("Upgrade contract success"))
}

func (f *StakeContract) InvokeContract(method string) protogo.Response {
	switch method {
	case "init":
		return f.InitUser()
	case "transfer":
		return f.Transfer()
	case "balanceOf":
		return f.BalanceOf()
	default:
		return sdk.Error("invalid method")
	}
}

// InitUser 初始化用户
func (f *StakeContract) InitUser() protogo.Response {
	funcName := "InitUser"
	params := sdk.Instance.GetArgs()
	userPk, err := sdk.Instance.GetSenderPk()
	if err != nil {
		errStr := fmt.Sprintf("[%s] GetSenderPk failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}
	userID := string(params["user_id"])
	pubkeyBytes := params["user_pubkey"]
	keyTypeStr := string(params["key_type"])
	keyType, err := strconv.Atoi(keyTypeStr)
	if err != nil {
		errStr := fmt.Sprintf("[%s] Atoi key_type failed, key_type=%s, err=%v", funcName, keyTypeStr, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}
	_, err = inCrypto.ParsePublicKey(crypto.KeyType(keyType), pubkeyBytes)
	if err != nil {
		errStr := fmt.Sprintf("[%s] ParsePublicKey failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}
	hashTypeStr := string(params["hash_type"])
	hashType, err := strconv.Atoi(hashTypeStr)
	if err != nil {
		errStr := fmt.Sprintf("[%s] Atoi hash_type failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}
	opts := &crypto.SignOpts{Hash: crypto.HashType(hashType)}

	users, err := getUsers()
	if err != nil {
		errStr := fmt.Sprintf("[%s] getUsers failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	if users.Users != nil {
		if _, ok := users.Users[userID]; !ok {
			users.Users[userID] = User{
				UserID:      userID,
				UserPK:      userPk,
				Balance:     DefaultBalance,
				PubKeyBytes: pubkeyBytes,
				KeyType:     crypto.KeyType(keyType),
				SignOpts:    opts,
			}
		}
	} else {
		users.Users = make(map[string]User)
		users.Users[userID] = User{
			UserID:      userID,
			UserPK:      userPk,
			Balance:     DefaultBalance,
			PubKeyBytes: pubkeyBytes,
			KeyType:     crypto.KeyType(keyType),
			SignOpts:    opts,
		}
	}

	if users.UserPk2ID == nil {
		users.UserPk2ID = make(map[string]string)
	}

	users.UserPk2ID[userPk] = userID

	err = setUsers(users)
	if err != nil {
		errStr := fmt.Sprintf("[%s] setUsers failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	successStr := fmt.Sprintf("[%s] user_pk=%s, user_id=%s init success", funcName, userPk, userID)
	// 记录日志
	sdk.Instance.Infof(successStr)
	// 返回结果
	return sdk.Success([]byte(successStr))
}

// Transfer 转账
func (f *StakeContract) Transfer() protogo.Response {
	funcName := "Transfer"
	params := sdk.Instance.GetArgs()
	sdk.Instance.Sender()
	userPk, err := sdk.Instance.GetSenderPk()
	if err != nil {
		errStr := fmt.Sprintf("[%s] GetSenderPk failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}
	from := string(params["from"])
	to := string(params["to"])
	sign := params["sign"]
	randKey := params["rand_key"]
	stakeNumStr := string(params["num"])
	stakeNum, err := strconv.Atoi(stakeNumStr)
	if err != nil {
		errStr := fmt.Sprintf("[%s] Atoi stakeNumStr failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	users, err := getUsers()
	if err != nil {
		errStr := fmt.Sprintf("[%s] getUsers failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	if users.Users == nil {
		errStr := fmt.Sprintf("[%s] you need init, user_pk=%s, user_id=%s", funcName, userPk, from)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	var (
		user   User
		toUser User
		ok     bool
	)
	// 用户转账需要验证，合约调用不需要验证
	if from == common.ChannelContract {
		if users.RandKey != string(randKey) {
			errStr := fmt.Sprintf("[%s] you need init account, user_pk=%s, user_id=%s", funcName, userPk, from)
			sdk.Instance.Infof(errStr)
			return sdk.Error(errStr)
		}
	} else {
		user, ok = users.Users[from]
		if !ok {
			errStr := fmt.Sprintf("[%s] you need init account, user_pk=%s, user_id=%s", funcName, userPk, from)
			sdk.Instance.Infof(errStr)
			return sdk.Error(errStr)
		}

		if user.Balance < int64(stakeNum) {
			errStr := fmt.Sprintf("[%s] from user banlance not enouth, user_pk=%s, user_id=%s, fromBlance=%d, stakeNum=%d", funcName, userPk, user.UserID, user.Balance, stakeNum)
			sdk.Instance.Infof(errStr)
			return sdk.Error(errStr)
		}

		if user.UserPK != userPk {
			errStr := fmt.Sprintf("[%s] user_pk incorrect, has no permission, user_pk=%s, user_id=%s", funcName, userPk, user.UserID)
			sdk.Instance.Infof(errStr)
			return sdk.Error(errStr)
		}

		pubKey, err := inCrypto.ParsePublicKey(user.KeyType, user.PubKeyBytes)
		if err != nil {
			errStr := fmt.Sprintf("[%s] ParsePublicKey failed, err=%v", funcName, err)
			sdk.Instance.Infof(errStr)
			return sdk.Error(errStr)
		}

		signBody := fmt.Sprintf("%s_%s_%s", from, to, stakeNumStr)

		// 验证转账用户签名
		ok, err = inCrypto.VerifyTX(sign, signBody, pubKey, user.SignOpts)
		if !ok || err != nil {
			errStr := fmt.Sprintf("[%s] VerifyTX failed, user_pk=%s, userId=%s, sign=%v, signBody=%s, err=%v", funcName, userPk, user.UserID, sign, signBody, err)
			sdk.Instance.Infof(errStr)
			return sdk.Error(errStr)
		}

		user.Balance = user.Balance - int64(stakeNum)
		users.Users[from] = user
	}

	// 用户转账需要验证，合约调用不需要验证
	if to != common.ChannelContract {
		toUser, ok = users.Users[to]
		if !ok {
			errStr := fmt.Sprintf("[%s] transfer user need init account, to_user_id=%s", funcName, to)
			sdk.Instance.Infof(errStr)
			return sdk.Error(errStr)
		}

		toUser.Balance = toUser.Balance + int64(stakeNum)
		users.Users[to] = toUser
	}

	err = setUsers(users)
	if err != nil {
		errStr := fmt.Sprintf("[%s] setUsers failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	return sdk.Success([]byte("转账成功"))
}

// BalanceOf 获取用户余额
func (f *StakeContract) BalanceOf() protogo.Response {
	funcName := "BalanceOf"
	params := sdk.Instance.GetArgs()
	userPk, err := sdk.Instance.GetSenderPk()
	if err != nil {
		errStr := fmt.Sprintf("[%s] GetSenderPk failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}
	from := string(params["from"])

	users, err := getUsers()
	if err != nil {
		errStr := fmt.Sprintf("[%s] getUsers failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	if users.Users == nil {
		errStr := fmt.Sprintf("[%s] you need init, user_pk=%s, user_id=%s", funcName, userPk, from)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	user, ok := users.Users[from]
	if !ok {
		errStr := fmt.Sprintf("[%s] you need init account, user_pk=%s, user_id=%s", funcName, userPk, from)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	if user.UserPK != userPk {
		errStr := fmt.Sprintf("[%s] user_pk incorrect, has no permission, user_pk=%s, user_id=%s", funcName, userPk, user.UserID)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	return sdk.Success([]byte(fmt.Sprint(user.Balance)))
}

func getUsers() (users Users, err error) {
	usersByte, err := sdk.Instance.GetStateFromKeyByte("users")
	if err != nil {
		return
	}

	if len(usersByte) == 0 {
		return
	}

	err = json.Unmarshal(usersByte, &users)
	return
}

func setUsers(users Users) error {
	usersByte, err := json.Marshal(users)
	if err != nil {
		return err
	}

	err = sdk.Instance.PutStateFromKeyByte("users", usersByte)
	if err != nil {
		return err
	}
	return nil
}

// sdk代码中，有且仅有一个main()方法
func main() {
	// main()方法中，下面的代码为必须代码，不建议修改main()方法当中的代码
	// 其中，TestContract为用户实现合约的具体名称
	err := sandbox.Start(new(StakeContract))
	if err != nil {
		log.Fatal(err)
	}
}
