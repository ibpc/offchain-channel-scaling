/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package mock

import (
	"fmt"
	"time"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
	vmPb "chainmaker.org/chainmaker/pb-go/v2/vm"
)

type SDKInatanceMock struct {
	Store map[string][]byte
	Args  map[string][]byte
}

func (cmc *SDKInatanceMock) GetArgs() map[string][]byte {
	return cmc.Args
}

// GetState get [key, field] from chain and db
// @param key: 获取的参数名
// @param field: 获取的参数名
// @return1: 获取结果，格式为string
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetState(key, field string) (state string, err error) {
	return
}

// GetBatchState get [BatchKeys] from chain and db
// @param batchKey: 获取的参数名
// @return1: 获取结果
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetBatchState(batchKeys []*vmPb.BatchKey) ([]*vmPb.BatchKey, error) {
	return nil, nil
}

// GetStateByte get [key, field] from chain and db
// @param key: 获取的参数名
// @param field: 获取的参数名
// @return1: 获取结果，格式为[]byte
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetStateByte(key, field string) (state []byte, err error) {
	return
}

// GetStateFromKey get [key] from chain and db
// @param key: 获取的参数名
// @return1: 获取结果，格式为string
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetStateFromKey(key string) (state string, err error) {
	return
}

// GetStateFromKeyByte get [key] from chain and db
// @param key: 获取的参数名
// @return1: 获取结果，格式为[]byte
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetStateFromKeyByte(key string) (state []byte, err error) {
	return
}

// PutState put [key, field, value] to chain
// @param1 key: 参数名
// @param1 field: 参数名
// @param2 value: 参数值，类型为string
// @return1: 上传参数错误信息
func (cmc *SDKInatanceMock) PutState(key, field string, value string) error {
	return nil
}

// PutStateByte put [key, field, value] to chain
// @param1 key: 参数名
// @param1 field: 参数名
// @param2 value: 参数值，类型为[]byte
// @return1: 上传参数错误信息
func (cmc *SDKInatanceMock) PutStateByte(key, field string, value []byte) error {
	return nil
}

// PutStateFromKey put [key, value] to chain
// @param1 key: 参数名
// @param2 value: 参数值，类型为string
// @return1: 上传参数错误信息
func (cmc *SDKInatanceMock) PutStateFromKey(key string, value string) error {
	return nil
}

// PutStateFromKeyByte put [key, value] to chain
// @param1 key: 参数名
// @param2 value: 参数值，类型为[]byte
// @return1: 上传参数错误信息
func (cmc *SDKInatanceMock) PutStateFromKeyByte(key string, value []byte) error {
	return nil
}

// DelState delete [key, field] to chain
// @param1 key: 删除的参数名
// @param1 field: 删除的参数名
// @return1：删除参数的错误信息
func (cmc *SDKInatanceMock) DelState(key, field string) error {
	return nil
}

// DelStateFromKey delete [key] to chain
// @param1 key: 删除的参数名
// @return1：删除参数的错误信息
func (cmc *SDKInatanceMock) DelStateFromKey(key string) error {
	return nil
}

// GetCreatorOrgId get tx creator org id
// @return1: 合约创建者的组织ID
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetCreatorOrgId() (orgid string, err error) {
	return
}

// GetCreatorRole get tx creator role
// @return1: 合约创建者的角色
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetCreatorRole() (role string, err error) {
	return
}

// GetCreatorPk get tx creator pk
// @return1: 合约创建者的公钥
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetCreatorPk() (role string, err error) {
	return
}

// GetSenderOrgId get tx sender org id
// @return1: 交易发起者的组织ID
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetSenderOrgId() (orgid string, err error) {
	return
}

// GetSenderRole get tx sender role
// @return1: 交易发起者的角色
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetSenderRole() (role string, err error) {
	return
}

// GetSenderPk get tx sender pk
// @return1: 交易发起者的公钥
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetSenderPk() (pk string, err error) {
	pk = "xxx"
	return
}

// GetBlockHeight get tx block height
// @return1: 当前块高度
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetBlockHeight() (height int, err error) {
	return
}

// GetTxId get current tx id
// @return1: 交易ID
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetTxId() (txId string, err error) {
	return
}

// GetTxInfo get tx info
// @param txId :合约交易ID
func (cmc *SDKInatanceMock) GetTxInfo(txId string) (resp protogo.Response) {
	return
}

// GetTxTimeStamp get tx timestamp
// @return1: 交易timestamp
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetTxTimeStamp() (timeStamp string, err error) {
	timeStamp = fmt.Sprint(time.Now().Unix())
	return
}

// EmitEvent emit event, you can subscribe to the event using the SDK
// @param1 topic: 合约事件的主题
// @param2 data: 合约事件的数据，参数数量不可大于16
func (cmc *SDKInatanceMock) EmitEvent(topic string, data []string) {
	return
}

// Log record log to chain server
// @param message: 事情日志的信息
func (cmc *SDKInatanceMock) Log(message string) {
	fmt.Println(message)
	return
}

// Log record log to chain server
// @param message: 事情日志的信息
func (cmc *SDKInatanceMock) Debugf(format string, a ...interface{}) {
	fmt.Printf(format, a...)
	return
}

// Infof record log to chain server
// @param format: 日志格式化模板
// @param a: 模板参数
func (cmc *SDKInatanceMock) Infof(format string, a ...interface{}) {
	fmt.Printf(format, a...)
	return
}

// Warnf record log to chain server
// @param format: 日志格式化模板
// @param a: 模板参数
func (cmc *SDKInatanceMock) Warnf(format string, a ...interface{}) {
	fmt.Printf(format, a...)
	return
}

// Errorf record log to chain server
// @param format: 日志格式化模板
// @param a: 模板参数
func (cmc *SDKInatanceMock) Errorf(format string, a ...interface{}) {
	fmt.Printf(format, a...)
	return
}

// CallContract invoke another contract and get response
// @param1: 合约名称
// @param2: 合约版本
// @param3: 合约参数
// @return1: 合约结果
func (cmc *SDKInatanceMock) CallContract(contractName, contractVersion string, args map[string][]byte) (resp protogo.Response) {
	resp.Status = 0
	return
}

// NewIterator range of [startKey, limitKey), front closed back open
// @param1: 范围查询起始key
// @param2: 范围查询结束key
// @return1: 根据起始key生成的迭代器
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) NewIterator(startKey string, limitKey string) (rkv sdk.ResultSetKV, err error) {
	return
}

// NewIteratorWithField range of [key+"#"+startField, key+"#"+limitField), front closed back open
// @param1: 分别与param2, param3 构成查询起始和结束的key
// @param2: [param1 + "#" + param2] 来获取查询起始的key
// @param3: [param1 + "#" + param3] 来获取查询结束的key
// @return1: 根据起始位置生成的迭代器
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) NewIteratorWithField(key string, startField string, limitField string) (rkv sdk.ResultSetKV, err error) {
	return
}

// NewIteratorPrefixWithKeyField range of [key+"#"+field, key+"#"+field], front closed back closed
// @param1: [ param1 + "#" +param2 ] 构成前缀范围查询的key
// @param2: [ param1 + "#" +param2 ] 构成前缀范围查询的key
// @return1: 根据起始位置生成的迭代器
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) NewIteratorPrefixWithKeyField(key string, field string) (skv sdk.ResultSetKV, err error) {
	return
}

// NewIteratorPrefixWithKey range of [key, key], front closed back closed
// @param1: 前缀范围查询起始key
// @return1: 根据起始位置生成的迭代器
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) NewIteratorPrefixWithKey(key string) (skv sdk.ResultSetKV, err error) {
	return
}

// NewHistoryKvIterForKey query all historical data of key, field
// @param1: 查询历史的key
// @param2: 查询历史的field
// @return1: 根据key, field 生成的历史迭代器
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) NewHistoryKvIterForKey(key, field string) (kvi sdk.KeyHistoryKvIter, err error) {
	return
}

// GetSenderAddr Get the address of the tx sender
// @return1: 交易发起方地址
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) GetSenderAddr() (addr string, err error) {
	return
}

// Sender Get the address of the sender address, if the contract is called by another contract, the result will be
// the caller contract's address
// @return1: sender address
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) Sender() (resp string, err error) {
	return
}

// Origin Get the address of the tx origin caller address
// @return1: origin caller address
// @return2: 获取错误信息
func (cmc *SDKInatanceMock) Origin() (resp string, err error) {
	return
}
