/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package crypto

import (
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"fmt"

	"chainmaker.org/chainmaker/common/v2/crypto"
	bcecdsa "chainmaker.org/chainmaker/common/v2/crypto/asym/ecdsa"
	bcrsa "chainmaker.org/chainmaker/common/v2/crypto/asym/rsa"
	bcsm2 "chainmaker.org/chainmaker/common/v2/crypto/asym/sm2"
	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"github.com/btcsuite/btcd/btcec"
	"github.com/tjfoc/gmsm/sm2"
	gmx509 "github.com/tjfoc/gmsm/x509"
)

type PubKeyEntity struct {
	PublicKey   crypto.PublicKey
	KeyType     crypto.KeyType
	PubKeyBytes []byte
}

// KeyPair private key and public key pair
type KeyPair struct {
	PrivateKey crypto.PrivateKey
	PublicKey  crypto.PublicKey
	KeyType    crypto.KeyType
}

// ParseCert 解析证书
func ParseCert(crtPEM []byte) (*bcx509.Certificate, error) {
	certBlock, _ := pem.Decode(crtPEM)
	if certBlock == nil {
		return nil, fmt.Errorf("decode pem failed, invalid certificate")
	}

	cert, err := bcx509.ParseCertificate(certBlock.Bytes)
	if err != nil {
		return nil, fmt.Errorf("x509 parse cert failed, %s", err)
	}

	return cert, nil
}

// ParsePublicKey 从字节数组中解析出公钥
func ParsePublicKey(keyType crypto.KeyType, data []byte) (pubKey crypto.PublicKey, err error) {
	switch keyType {
	case -1:
		// rsa
		var rsaPubKey *rsa.PublicKey
		rsaPubKey, err = x509.ParsePKCS1PublicKey(data)
		if err == nil {
			return
		}
		pubKey = &bcrsa.PublicKey{K: rsaPubKey}
		return
	case crypto.ECC_NISTP256, crypto.ECC_NISTP384, crypto.ECC_NISTP521:
		var pubInt interface{}
		pubInt, err = x509.ParsePKIXPublicKey(data)
		if err != nil {
			return
		}
		rsaPubKey, ok := pubInt.(*ecdsa.PublicKey)
		if !ok {
			err = fmt.Errorf("is not ecdsa public key, %T, %v", pubInt, pubInt)
			return
		}
		pubKey = &bcecdsa.PublicKey{K: rsaPubKey}
		return
	case crypto.ECC_Secp256k1:
		var btcPubKey *btcec.PublicKey
		btcPubKey, err = btcec.ParsePubKey(data, btcec.S256())
		if err != nil {
			return
		}
		var ecdsaPubKey = ecdsa.PublicKey{
			X:     btcPubKey.X,
			Y:     btcPubKey.Y,
			Curve: btcPubKey.Curve,
		}
		pubKey = &bcecdsa.PublicKey{K: &ecdsaPubKey}
		return
	case crypto.SM2:
		var pubInt interface{}
		pubInt, err = gmx509.ParsePKIXPublicKey(data)
		if err != nil {
			return
		}
		sm2PubKey, ok := pubInt.(*sm2.PublicKey)
		if !ok {
			err = fmt.Errorf("is not sm2 public key")
			return
		}
		pubKey = &bcsm2.PublicKey{K: sm2PubKey}
		return
	default:
		err = fmt.Errorf("incorrect key type")
		return
	}
}

// SignTx sign msg
// @Param msg messages that need to be signed
// @Param privatekey this variable is the private key used by the node
// @Param crypto.SignOpts this variable contains the conditions to be set for node signature
// @Return sig digital signature
func SignTx(privatekey crypto.PrivateKey, opts *crypto.SignOpts, msg interface{}) ([]byte, error) {
	msgMarshal, err := json.Marshal(msg)
	if err != nil {
		return nil, fmt.Errorf("json marshal error, %v", err)
	}

	//select the hash algorithm used by the signature function
	sig, err := privatekey.SignWithOpts(msgMarshal, opts)
	if err != nil {
		return nil, fmt.Errorf("sign error, %v", err)
	}
	return sig, nil
}

// VerifyTX verify msg sign
// @Param msg  messages that need to be signed
// @Param sig  digital signature
// @Param pk this variable is the public key used by the node
// @Param opts this variable contains the conditions to be set for node signature
// @Return sig digital signature
func VerifyTX(sig []byte, msg interface{}, pk crypto.PublicKey, opts *crypto.SignOpts) (bool, error) {
	msgMarshal, err := json.Marshal(msg)
	if err != nil {
		return false, fmt.Errorf("marshal error, %v", err)
	}
	ok, err := pk.VerifyWithOpts(msgMarshal, sig, opts)
	if err != nil {
		return ok, fmt.Errorf("verifyWithOpts error, %v", err)
	}
	return ok, nil
}
