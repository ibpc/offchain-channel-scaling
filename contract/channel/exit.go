/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"fmt"

	"channel-contract/common"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// Exit 退出通道
func Exit() protogo.Response {
	funcName := "Exit"
	params := sdk.Instance.GetArgs()
	userPK, err := sdk.Instance.GetSenderPk()
	if err != nil {
		errStr := fmt.Sprintf("[%s] GetSenderPk failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	channelName := string(params["channel_name"])
	channel, err := GetChannel(channelName)
	if err != nil {
		errStr := fmt.Sprintf("[%s] getChannel failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	userID, ok := channel.UserPk2ID[userPK]
	if !ok {
		errStr := fmt.Sprintf("[%s] find userid failed, user_pk=%s", funcName, userPK)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	user, ok := channel.Users[userID]
	if !ok {
		errStr := fmt.Sprintf("[%s] find user failed, user_pk=%s", funcName, userPK)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	withdraw, err := GetChannelWithdraw(channelName)
	if err != nil {
		errStr := fmt.Sprintf("[%s] get withdraw failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	balance, ok := withdraw[userID]
	if !ok {
		errStr := fmt.Sprintf("[%s] cann't find withdraw with user, user_id=%s", funcName, userID)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	cKey, err := GetStakeRandKey()
	if err != nil {
		errStr := fmt.Sprintf("[%s] getStakeRandKey failed, %s", funcName, channelName)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	// 调用权益合约转账
	resp := sdk.Instance.CallContract(common.StakeContract, "transfer", map[string][]byte{
		"from":     []byte(common.ChannelContract),
		"to":       []byte(userID),
		"num":      []byte(fmt.Sprint(balance)),
		"rand_key": []byte(fmt.Sprint(cKey.RandKey)),
	})

	if resp.Status != 0 {
		errStr := fmt.Sprintf("[%s] call %s failed, resp=%+v", funcName, common.StakeContract, resp)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	channel.PlayerCount = channel.PlayerCount - 1
	if channel.PlayerCount == 0 {
		channel.Status = Close
	}

	user.Exit = true
	channel.Users[userID] = user
	err = SetChannel(channelName, channel)
	if err != nil {
		errStr := fmt.Sprintf("[%s] set channel failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	sdk.Instance.Log(fmt.Sprintf("[%s] success", funcName))
	return sdk.Success([]byte("Exit success"))
}
