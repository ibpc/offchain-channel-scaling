/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"encoding/json"
	"fmt"
	"strconv"

	"channel-contract/common"
	inCrypto "channel-contract/crypto"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// JoinChannel 加入通道
func JoinChannel() protogo.Response {
	funcName := "JoinChannel"
	params := sdk.Instance.GetArgs()
	userPk, err := sdk.Instance.GetSenderPk()
	if err != nil {
		errStr := fmt.Sprintf("[%s] GetSenderPk failed, err=%v", funcName, err)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}
	channelName := string(params["channel_name"])
	userID := string(params["user_id"])
	userAddress := string(params["user_address"])
	sign := params["sign"]
	pubkeyBytes := params["user_pubkey"]
	keyTypeStr := string(params["key_type"])
	keyType, err := strconv.Atoi(keyTypeStr)
	if err != nil {
		errStr := fmt.Sprintf("[%s] Atoi key_type failed, key_type=%s, err=%v", funcName, keyTypeStr, err)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}
	_, err = inCrypto.ParsePublicKey(crypto.KeyType(keyType), pubkeyBytes)
	if err != nil {
		errStr := fmt.Sprintf("[%s] ParsePublicKey failed, err=%v", funcName, err)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}
	hashTypeStr := string(params["hash_type"])
	hashType, err := strconv.Atoi(hashTypeStr)
	if err != nil {
		errStr := fmt.Sprintf("[%s] Atoi hash_type failed, err=%v", funcName, err)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}
	opts := &crypto.SignOpts{Hash: crypto.HashType(hashType)}
	balanceStr := string(params["balance"])
	balance, err := strconv.Atoi(balanceStr)
	if err != nil {
		errStr := fmt.Sprintf("[%s] Atoi balance failed, err=%v", funcName, err)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}

	// channelName 不存在不会报错，已验证
	existChan, err := GetChannel(channelName)
	if err != nil {
		errStr := fmt.Sprintf("[%s] getChannel failed, err=%v", funcName, err)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}

	// 判断该通道是否已经存在
	if existChan.LeaderId == "" {
		errStr := fmt.Sprintf("[%s] channel %s not exist", funcName, channelName)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}

	// 判断用户是否已经存在，并且没有退出
	if user, ok := existChan.Users[userID]; ok && !user.Exit {
		state, err := GetChannelState(channelName)
		if err != nil {
			errStr := fmt.Sprintf("[%s] getChannel state failed, err=%v", funcName, err)
			sdk.Instance.Errorf(errStr)
			return sdk.Error(errStr)
		}

		if state.BalanceSet != nil {
			for _, user := range existChan.Users {
				if _, ok := state.BalanceSet[user.UserID]; !ok {
					state.BalanceSet[user.UserID] = user.Balance
				}
			}
		}

		var chaninfo = ChannelInfo{
			LeaderId:        existChan.LeaderId,
			LeaderAddr:      existChan.LeaderAddr,
			Epoch:           existChan.Epoch,
			Status:          existChan.Status,
			ChalleageStatus: existChan.ChalleageStatus,
			BalanceSet:      state.BalanceSet,
		}

		chaninfoBytes, err := json.Marshal(chaninfo)
		if err != nil {
			errStr := fmt.Sprintf("[%s] json Marshal challeageIds failed, err=%v", funcName, err)
			sdk.Instance.Errorf(errStr)
			return sdk.Error(errStr)
		}

		sdk.Instance.Infof(fmt.Sprintf("[%s] channel %s is exist", funcName, channelName))
		return sdk.Success(chaninfoBytes)
	}

	if existChan.PlayerCount >= existChan.MaxPayers {
		errStr := fmt.Sprintf("[%s] join channel failed, beyond the maximum number of player", funcName)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}

	resp := sdk.Instance.CallContract(common.StakeContract, "init", map[string][]byte{
		"user_id":     []byte(userID),
		"user_pubkey": pubkeyBytes,
		"key_type":    []byte(keyTypeStr),
		"hash_type":   []byte(hashTypeStr),
	})

	if resp.Status != 0 {
		errStr := fmt.Sprintf("[%s] call %s init failed, resp=%+v", funcName, common.StakeContract, resp)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}

	resp = sdk.Instance.CallContract(common.StakeContract, "transfer", map[string][]byte{
		"from": []byte(userID),
		"to":   []byte(common.ChannelContract),
		"num":  []byte(balanceStr),
		"sign": sign,
	})

	if resp.Status != 0 {
		errStr := fmt.Sprintf("[%s] call %s transfer failed, resp=%+v", funcName, common.StakeContract, resp)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}

	userState := UserState{
		Address:     userAddress,
		Balance:     balance,
		UserID:      userID,
		UserPK:      userPk,
		PubKeyBytes: pubkeyBytes,
		KeyType:     crypto.KeyType(keyType),
		SignOpts:    opts,
	}
	existChan.PlayerCount += 1
	existChan.Users[userID] = userState
	existChan.UserPk2ID[userPk] = userID

	// 发送事件
	sdk.Instance.EmitEvent("topic_join_channel", []string{channelName, userAddress})
	err = SetChannel(channelName, existChan)
	if err != nil {
		errStr := fmt.Sprintf("[%s] setChannel failed, err=%s", funcName, err.Error())
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}
	// 记录日志
	sdk.Instance.Errorf(fmt.Sprintf("[%s] ChannelName=%s, UserAddr=%s, Balance=%d", funcName,
		existChan.ChannelName, userAddress, userState.Balance))

	var state = State{
		BalanceSet: make(map[string]int),
	}

	state.BalanceSet[userID] = balance

	// 返回结果
	var chaninfo = ChannelInfo{
		LeaderId:        existChan.LeaderId,
		LeaderAddr:      existChan.LeaderAddr,
		Epoch:           existChan.Epoch,
		Status:          existChan.Status,
		ChalleageStatus: existChan.ChalleageStatus,
		BalanceSet:      state.BalanceSet,
	}

	chaninfoBytes, err := json.Marshal(chaninfo)
	if err != nil {
		errStr := fmt.Sprintf("[%s] json Marshal chaninfo failed, err=%v", funcName, err)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}

	// 返回结果
	return sdk.Success(chaninfoBytes)
}
