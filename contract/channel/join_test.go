/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"encoding/base64"
	"testing"

	"channel-contract/mock"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
	"github.com/agiledragon/gomonkey/v2"
)

// TestJoinChannel 测试加入通道
func TestJoinChannel(t *testing.T) {
	args := make(map[string][]byte)
	args["channel_name"] = []byte("channel1")
	args["user_id"] = []byte("user_id1")
	args["user_address"] = []byte("user_address1")

	pubkey := "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEywSJ5aRfDQ5FG76D4/FDJirX4SiGdtZQcSRrWRmCPUHWiTDJkmTi9KAy8Kdf1ygdsb5pmUrtNSZRFqtQhMs40w=="
	pubkeyBytes, err := base64.StdEncoding.DecodeString(pubkey)
	if err != nil {
		t.Fatal("base64.StdEncoding.DecodeString faield, ", err)
	}

	args["user_pubkey"] = pubkeyBytes
	args["key_type"] = []byte("8")
	args["hash_type"] = []byte("5")
	args["max_user"] = []byte("50")
	args["balance"] = []byte("1000")

	sdk.Instance = &mock.SDKInatanceMock{
		Args: args,
	}

	gomonkey.ApplyFunc(GetChannel, func(channelName string) (Channel, error) {
		var chann = Channel{
			MaxPayers: 100,
			Users:     make(map[string]UserState),
			UserPk2ID: make(map[string]string),
			LeaderId:  "xxx",
		}
		return chann, nil
	})

	resp := JoinChannel()
	if resp.Status == 0 {
		t.Logf("Payload=%s", string(resp.Payload))
		return
	}

	t.Fatalf("resp=%+v, Payload=%s", resp, string(resp.Payload))
}
