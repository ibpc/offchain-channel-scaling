/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"fmt"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// BeginExchange 在init阶段，一定数量用户加入通道后进入交易阶段
func BeginExchange() protogo.Response {
	funcName := "BeginExchange"
	params := sdk.Instance.GetArgs()
	userPK, err := sdk.Instance.GetSenderPk()
	if err != nil {
		errStr := fmt.Sprintf("[%s] GetSenderPk failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	channelName := string(params["channel_name"])
	channel, err := GetChannel(channelName)
	if err != nil {
		errStr := fmt.Sprintf("[%s] getChannel failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	// 验证是否 leader
	if userPK != channel.LeaderPK {
		errStr := fmt.Sprintf("[%s] user not leader, only the leader could begin exchange", funcName)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	channel.Status = Exchange
	err = SetChannel(channelName, channel)
	if err != nil {
		errStr := fmt.Sprintf("[%s] setChannel failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	sdk.Instance.EmitEvent("topic_open_channel", []string{channel.LeaderId, "open channel"})
	sdk.Instance.Log(fmt.Sprintf("[%s] %s begin exchange", funcName, channel.LeaderId))
	return sdk.Success([]byte("Begin exchange success"))
}
