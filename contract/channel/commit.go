/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"encoding/json"
	"fmt"

	inCrypto "channel-contract/crypto"
	"channel-contract/merkletree"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// Commit 阶段提交 epoch 共识后的状态
func Commit() protogo.Response {
	funcName := "Commit"
	params := sdk.Instance.GetArgs()
	userPK, err := sdk.Instance.GetSenderPk()
	if err != nil {
		errStr := fmt.Sprintf("[%s] GetSenderPk failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	channelName := string(params["channel_name"])
	channel, err := GetChannel(channelName)
	if err != nil {
		errStr := fmt.Sprintf("[%s] getChannel failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	// 验证是否 leader
	if userPK != channel.LeaderPK {
		errStr := fmt.Sprintf("[%s] user not leader, only the leader could begin exchange", funcName)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	if channel.ChalleageStatus != NoChalleage && channel.ChalleageStatus != SolveStatus {
		errStr := fmt.Sprintf("[%s] has challeage", funcName)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	stateData := params["data"]
	var state State
	err = json.Unmarshal(stateData, &state)
	if err != nil {
		errStr := fmt.Sprintf("[%s] get data failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	if state.Epoch != channel.Epoch+1 {
		errStr := fmt.Sprintf("[%s] epoch failed, state.epoch=%d, channel.epoch=%d", funcName,
			state.Epoch, channel.Epoch)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	signState := State{Epoch: state.Epoch, LeaderId: state.LeaderId, BalanceSet: state.BalanceSet,
		WithdrawSet: state.WithdrawSet}

	// 判断签名是否正确
	var signOk bool
	for userId, sign := range state.SigSet {
		// 不用验证挑战者的签名，因为已经 solve 过
		if _, ok := channel.Challeage[userId]; ok {
			continue
		}
		user, ok := channel.Users[userId]
		if !ok {
			sdk.Instance.Log(fmt.Sprintf("[%s] Cann't find user state, user_id=%s", funcName, userId))
			continue
		}

		pubKey, err := inCrypto.ParsePublicKey(user.KeyType, user.PubKeyBytes)
		if err != nil {
			sdk.Instance.Log(fmt.Sprintf("[%s] ParsePublicKey failed, err=%v", funcName, err))
			continue
		}

		ok, err = inCrypto.VerifyTX(sign, signState, pubKey, user.SignOpts)
		if !ok || err != nil {
			errStr := fmt.Sprintf("[%s] VerifyTX failed, user_id=%s, ok=%v, err=%v", funcName, userId, ok, err)
			sdk.Instance.Log(errStr)
			return sdk.Error(errStr)
		}
		signOk = true
		break
	}

	if !signOk {
		errStr := fmt.Sprintf("[%s] state sign failed", funcName)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	// 签名验证通过，设置下一 epoch leader
	channel.Epoch = state.Epoch
	user, ok := channel.Users[state.LeaderId]
	if !ok {
		errStr := fmt.Sprintf("[%s] Cann't find next state leader state, leader_id=%s", funcName, state.LeaderId)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	channel.LeaderId = user.UserID
	channel.LeaderAddr = user.Address
	channel.LeaderPK = user.UserPK
	channel.Status = Exchange
	channel.ChalleageStatus = NoChalleage
	channel.ChalleageTime = 0
	channel.Challeage = make(map[string]merkletree.MerkleTree)

	for userId, val := range state.BalanceSet {
		if userState, ok := channel.Users[userId]; ok {
			userState.Balance = val
			channel.Users[userId] = userState
		}
	}

	withdraw, err := GetChannelWithdraw(channelName)
	if err != nil {
		errStr := fmt.Sprintf("[%s] get withdraw failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	for key, val := range state.WithdrawSet {
		withdraw[key] = val
	}

	err = SetChannelWithdraw(channelName, withdraw)
	if err != nil {
		errStr := fmt.Sprintf("[%s] set withdraw failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	// 存储相关信息
	err = SetChannelState(channelName, &state)
	if err != nil {
		errStr := fmt.Sprintf("[%s] Cann't save state, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	err = SetChannel(channelName, channel)
	if err != nil {
		errStr := fmt.Sprintf("[%s] Cann't save channel, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	sdk.Instance.Log(fmt.Sprintf("[%s] %s commit success", funcName, channel.LeaderId))
	return sdk.Success([]byte("Commit success"))
}
