/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"encoding/json"
	"testing"

	"channel-contract/merkletree"
	"channel-contract/mock"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
	"github.com/agiledragon/gomonkey/v2"
)

// TestChalleage 测试提交
func TestChalleage(t *testing.T) {
	mkltree, err := merkletree.New([][]byte{[]byte("data1"), []byte("data2")})
	if err != nil {
		t.Fatal("merkletree.New failed, ", err)
	}

	mkltreedata, err := json.Marshal(mkltree)
	if err != nil {
		t.Fatal("json.Marshal failed, ", err)
	}

	args := make(map[string][]byte)
	args["channel_name"] = []byte("channel1")
	args["merkletree"] = mkltreedata

	sdk.Instance = &mock.SDKInatanceMock{
		Args: args,
	}

	gomonkey.ApplyFunc(GetChannel, func(channelName string) (Channel, error) {
		var chann = Channel{
			LeaderPK: "yyy",
			UserPk2ID: map[string]string{
				"xxx": "aaa",
			},
			Challeage: make(map[string]merkletree.MerkleTree),
		}
		return chann, nil
	})

	resp := Challeage()
	if resp.Status == 0 {
		t.Logf("Payload=%s", string(resp.Payload))
		return
	}

	t.Fatalf("resp=%+v, Payload=%s", resp, string(resp.Payload))
}
