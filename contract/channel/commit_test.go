/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"encoding/json"
	"testing"

	"channel-contract/mock"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
	"github.com/agiledragon/gomonkey/v2"
)

// TestCommit 测试提交状态
func TestCommit(t *testing.T) {
	args := make(map[string][]byte)
	args["channel_name"] = []byte("channel1")

	var state = State{
		Epoch:    2,
		LeaderId: "QmUtQmnGVyvmBcKHqbmywiHJwU5p5CrbsVynKuRSocKrDs",
		BalanceSet: map[string]int{
			"QmSbUfSLtQavZQ4CmfNsrgzXw3iyHPSEbVQhngRpGwd9vw": 30000,
			"QmTJLZjed7mnAyCdLhPeEvaMgvsH3Hf1GgdN8zwsZ2Xz2P": 30000,
			"QmUtQmnGVyvmBcKHqbmywiHJwU5p5CrbsVynKuRSocKrDs": 100000,
			"QmfHFdpTN1RCLvvqpivwF5KwDTefaGMNezqxw7MoZRzmjh": 30000,
		},
		WithdrawSet: make(map[string]int),
		SigSet: map[string][]byte{
			"QmfHFdpTN1RCLvvqpivwF5KwDTefaGMNezqxw7MoZRzmjh": {48, 69, 2, 33, 0, 163, 94, 206, 115, 64, 120, 36, 188, 122, 5, 188, 101, 222, 227, 244, 202, 169, 168, 155, 47, 39, 78, 102, 230, 56, 44, 181, 54, 136, 104, 253, 200, 2, 32, 69, 24, 137, 142, 137, 158, 90, 183, 33, 215, 119, 210, 1, 159, 138, 8, 52, 153, 184, 210, 232, 67, 147, 173, 52, 89, 115, 114, 2, 99, 8, 207},
		},
	}

	data, _ := json.Marshal(state)
	args["data"] = data

	sdk.Instance = &mock.SDKInatanceMock{
		Args: args,
	}

	gomonkey.ApplyFunc(GetChannel, func(channelName string) (Channel, error) {
		var chann = Channel{
			LeaderPK:        "xxx",
			ChalleageStatus: SolveStatus,
			Epoch:           1,
			Users: map[string]UserState{
				"QmfHFdpTN1RCLvvqpivwF5KwDTefaGMNezqxw7MoZRzmjh": {
					KeyType: 8,
					SignOpts: &crypto.SignOpts{
						Hash: 5,
					},
					PubKeyBytes: []byte{48, 89, 48, 19, 6, 7, 42, 134, 72, 206, 61, 2, 1, 6, 8, 42, 134, 72, 206, 61, 3, 1, 7, 3, 66, 0, 4, 190, 31, 228, 196, 74, 244, 94, 140, 65, 89, 235, 155, 171, 119, 66, 61, 70, 171, 207, 169, 27, 80, 56, 198, 14, 196, 41, 148, 2, 119, 72, 65, 184, 30, 87, 169, 233, 23, 109, 135, 97, 25, 85, 207, 88, 158, 255, 101, 201, 128, 249, 37, 150, 21, 52, 97, 249, 68, 12, 234, 179, 143, 174, 134},
				},
				"QmUtQmnGVyvmBcKHqbmywiHJwU5p5CrbsVynKuRSocKrDs": {},
			},
		}
		return chann, nil
	})

	resp := Commit()
	if resp.Status == 0 {
		t.Logf("Payload=%s", string(resp.Payload))
		return
	}

	t.Fatalf("resp=%+v, Payload=%s", resp, string(resp.Payload))
}
