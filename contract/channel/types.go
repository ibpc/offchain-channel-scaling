/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"time"

	"channel-contract/merkletree"

	"chainmaker.org/chainmaker/common/v2/crypto"
)

// Phase 阶段类型定义
type Phase int8

// ChalleageStatus 挑战状态类型定义
type ChalleageStatus int8

const (
	// Init 状态枚举初始化
	Init      Phase = iota
	Exchange        // Exchange 状态枚举交易
	Consensus       // Consensus 状态枚举共识
	Close           // Consensus 状态枚举关闭
)

const (
	// NoChalleage 挑战状态枚举无挑战
	NoChalleage    ChalleageStatus = iota
	HasChalleage                   // HasChalleage 挑战状态枚举有挑战
	RollbackStatus                 // Rollback 挑战状态枚举回滚
	SolveStatus                    // Solve 挑战状态枚举争议解决
)

const (
	defaultMaxUser = 100
)

type ContractKey struct {
	RandKey string `json:"rand_key"`
}

// ChannelInfo 通道信息
type ChannelInfo struct {
	LeaderId        string          `json:"leader_id"`
	LeaderAddr      string          `json:"leader_address"`
	Epoch           int             `json:"epoch"`
	Status          Phase           `json:"status"`
	ChalleageStatus ChalleageStatus `json:"challeage_status"`
	BalanceSet      map[string]int  `json:"balance"`
}

// State 状态
type State struct {
	Epoch       int
	LeaderId    string
	BalanceSet  map[string]int
	WithdrawSet map[string]int // node exit set
	SigSet      map[string][]byte
}

// Channel 通道结构定义
type Channel struct {
	ChannelName     string                           `json:"channel_name"`
	ChannelAddress  string                           `json:"channel_address"`
	PlayerCount     int                              `json:"player_count"`
	MaxPayers       int                              `json:"max_payers"`
	Epoch           int                              `json:"epoch"`
	Status          Phase                            `json:"status"`
	ChalleageStatus ChalleageStatus                  `json:"challeage_status"`
	LeaderId        string                           `json:"leader_id"`
	LeaderPK        string                           `json:"leader_pk"`
	LeaderAddr      string                           `json:"leader_address"`
	Challeage       map[string]merkletree.MerkleTree `json:"challeage"`
	ChalleageTime   int                              `json:"challeage_time"`
	OpenTime        int                              `json:"open_time"`
	UserPk2ID       map[string]string                `json:"userpk2id"`
	Users           map[string]UserState             `json:"users"`
	Reject          map[string]int                   `json:"reject"` // 合约判定挑战失败
}

// UserState 用户状态
type UserState struct {
	Address     string           `json:"address"`
	Balance     int              `json:"balance"`
	UserID      string           `json:"user_id"`
	UserPK      string           `json:"user_pk"`
	Exit        bool             `json:"exit"`
	PubKeyBytes []byte           `json:"pubkey_bytes"`
	KeyType     crypto.KeyType   `json:"key_type"`
	SignOpts    *crypto.SignOpts `json:"sign_opts"`
}

// Tx transaction struct
type Tx struct {
	Id          string // tx id
	Epoch       int    // epoch of tx
	Sender      string // sender id
	Receiver    string // receiver id
	Amount      int    // tx amount
	LeaderSig   []byte // leader sig
	SenderSig   []byte // sender sig
	ReceiverSig []byte // receiver sig

	StartTime time.Time     // tx start time
	Duration  time.Duration // tx duration time
}
