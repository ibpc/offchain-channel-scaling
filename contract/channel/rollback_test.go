/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"testing"

	"channel-contract/mock"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
	"github.com/agiledragon/gomonkey/v2"
)

// TestRollback 测试加入通道
func TestRollback(t *testing.T) {
	args := make(map[string][]byte)
	args["channel_name"] = []byte("channel1")

	sdk.Instance = &mock.SDKInatanceMock{
		Args: args,
	}

	gomonkey.ApplyFunc(GetChannel, func(channelName string) (Channel, error) {
		var chann = Channel{
			MaxPayers: 100,
			Users:     make(map[string]UserState),
			UserPk2ID: make(map[string]string),
			LeaderId:  "xxx",
			LeaderPK:  "xxx",
		}
		return chann, nil
	})

	resp := Rollback()
	if resp.Status == 0 {
		t.Logf("Payload=%s", string(resp.Payload))
		return
	}

	t.Fatalf("resp=%+v, Payload=%s", resp, string(resp.Payload))
}
