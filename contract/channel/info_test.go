/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"testing"

	"channel-contract/mock"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
	"github.com/agiledragon/gomonkey/v2"
)

// TestGetChannelInfo 测试获取通道信息
func TestGetChannelInfo(t *testing.T) {
	args := make(map[string][]byte)
	args["channel_name"] = []byte("channel1")

	sdk.Instance = &mock.SDKInatanceMock{
		Args: args,
	}

	gomonkey.ApplyFunc(GetChannel, func(channelName string) (Channel, error) {
		var chann = Channel{
			LeaderPK: "xxx",
		}
		return chann, nil
	})

	resp := GetChannelInfo()
	if resp.Status == 0 {
		t.Logf("Payload=%s", string(resp.Payload))
		return
	}

	t.Fatalf("resp=%+v, Payload=%s", resp, string(resp.Payload))
}

// TestGetUserInfo 测试领导者获取用户信息
func TestGetUserInfo(t *testing.T) {
	args := make(map[string][]byte)
	args["channel_name"] = []byte("channel1")
	args["user_id"] = []byte("nnn")

	sdk.Instance = &mock.SDKInatanceMock{
		Args: args,
	}

	gomonkey.ApplyFunc(GetChannel, func(channelName string) (Channel, error) {
		var chann = Channel{
			LeaderPK: "xxx",
			Users: map[string]UserState{
				"nnn": {
					Balance: 200,
				},
			},
		}
		return chann, nil
	})

	resp := GetUserInfo()
	if resp.Status == 0 {
		t.Logf("Payload=%s", string(resp.Payload))
		return
	}

	t.Fatalf("resp=%+v, Payload=%s", resp, string(resp.Payload))
}
