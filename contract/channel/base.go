/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"encoding/json"
	"fmt"

	inCrypto "channel-contract/crypto"
	"channel-contract/merkletree"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// SetStakeRandKey 设置合约密钥
func SetStakeRandKey(ckey ContractKey) error {
	ckeyByte, err := json.Marshal(ckey)
	if err != nil {
		return err
	}

	err = sdk.Instance.PutStateFromKeyByte("stake_rand_key", ckeyByte)
	if err != nil {
		return err
	}
	return nil
}

// GetStakeRandKey 获取合约密钥
func GetStakeRandKey() (ckey ContractKey, err error) {
	ckeyByte, err := sdk.Instance.GetStateFromKeyByte("stake_rand_key")
	if err != nil {
		return
	}
	if len(ckeyByte) == 0 {
		return
	}
	err = json.Unmarshal(ckeyByte, &ckey)
	return
}

//  SetChannel 存储通道信息
//  @Description:
//  @param channelName
//  @param channel
//  @return error
//
func SetChannel(channelName string, channel Channel) error {
	channelByte, err := json.Marshal(channel)
	if err != nil {
		return err
	}

	err = sdk.Instance.PutStateByte(channelName, channelName, channelByte)
	if err != nil {
		return err
	}
	return nil
}

//  GetChannel 获取通道信息
//  @Description:
//  @param channelName
//  @return Channel
//  @return error
//
func GetChannel(channelName string) (Channel, error) {
	channelByte, err := sdk.Instance.GetStateByte(channelName, channelName)
	if err != nil {
		return Channel{}, err
	}
	if len(channelByte) == 0 {
		return Channel{}, nil
	}
	var channel Channel
	err = json.Unmarshal(channelByte, &channel)
	return channel, err
}

//  SetChannelWithdraw 存储退出信息
//  @Description:
//  @param channelName
//  @param withdraw
//  @return error
//
func SetChannelWithdraw(channelName string, withdraw map[string]int) error {
	withdrawByte, err := json.Marshal(withdraw)
	if err != nil {
		return err
	}

	err = sdk.Instance.PutStateByte(channelName, "withdraw", withdrawByte)
	if err != nil {
		return err
	}
	return nil
}

//  GetChannelWithdraw 获取退出信息
//  @Description:
//  @param channelName
//  @return withdraw
//  @return error
//
func GetChannelWithdraw(channelName string) (withdraw map[string]int, err error) {
	withdraw = make(map[string]int)
	withdrawByte, err := sdk.Instance.GetStateByte(channelName, "withdraw")
	if err != nil {
		return
	}
	if len(withdrawByte) == 0 {
		return
	}
	err = json.Unmarshal(withdrawByte, &withdraw)
	return
}

//  SetChannelState 存储通道状态
//  @Description:
//  @param channelName
//  @param state
//  @return error
//
func SetChannelState(channelName string, state *State) error {
	stateByte, err := json.Marshal(state)
	if err != nil {
		return err
	}

	err = sdk.Instance.PutStateByte(channelName, "state", stateByte)
	if err != nil {
		return err
	}
	return nil
}

//  GetChannelState 获取通道状态
//  @Description:
//  @param channelName
//  @return state
//  @return error
//
func GetChannelState(channelName string) (state State, err error) {
	channelByte, err := sdk.Instance.GetStateByte(channelName, "state")
	if err != nil {
		return
	}
	if len(channelByte) == 0 {
		return
	}
	err = json.Unmarshal(channelByte, &state)
	return
}

//  VerifySign 验证签名
//  @Description:
//  @param channel
//  @param mkltree
//  @param userId
//  @param index
//  @param verifyLeader
//  @return verifyOk
//  @return error
//
func VerifySign(channel *Channel, mkltree *merkletree.MerkleTree, userId string, index int64,
	verifyLeader bool) (verifyOk bool, err error) {
	state, ok := channel.Users[userId]
	if !ok {
		sdk.Instance.Log(fmt.Sprintf("[verifySign] Cann't find user state, user_id=%s", userId))
		return
	}

	pubKey, err := inCrypto.ParsePublicKey(state.KeyType, state.PubKeyBytes)
	if err != nil {
		sdk.Instance.Log(fmt.Sprintf("[verifySign] ParsePublicKey answerId failed, err=%v", err))
		return
	}

	// 验证从此处开始的所有交易，挑战者签名
	txLen := int64(len(mkltree.Data))
	verifyOk = true
	for leaderIdx := index; leaderIdx < txLen; leaderIdx++ {
		var verifyTx Tx
		err = json.Unmarshal(mkltree.Data[leaderIdx], &verifyTx)
		if err != nil {
			sdk.Instance.Log(fmt.Sprintf("[verifySign] leader tx json unmarshal failed, err=%v", err))
			verifyOk = false
			break
		}

		var verifySign []byte
		// 验证 leader 提交的交易，则验证挑战者的签名
		// 反之则验证 leader 的签名
		if verifyLeader {
			if verifyTx.Sender == userId {
				verifySign = verifyTx.SenderSig
			} else {
				verifySign = verifyTx.ReceiverSig
			}
		} else {
			verifySign = verifyTx.LeaderSig
		}

		var signTx = Tx{
			Id:       verifyTx.Id,
			Epoch:    verifyTx.Epoch,
			Sender:   verifyTx.Sender,
			Receiver: verifyTx.Receiver,
			Amount:   verifyTx.Amount,
		}
		ok, err = inCrypto.VerifyTX(verifySign, signTx, pubKey, state.SignOpts)
		if !ok || err != nil {
			sdk.Instance.Log(fmt.Sprintf("[Answer] VerifyTX failed, userId=%s, ok=%v, err=%v", userId, ok, err))
			verifyOk = false
			break
		}
	}
	return
}
