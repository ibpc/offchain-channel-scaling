/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"testing"

	"channel-contract/mock"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// TestBind 测试绑定权益合约 rand key
func TestBind(t *testing.T) {
	args := make(map[string][]byte)
	args["rand_key"] = []byte("xxx")

	sdk.Instance = &mock.SDKInatanceMock{
		Args: args,
	}

	resp := Bind()
	if resp.Status == 0 {
		t.Logf("Payload=%s", string(resp.Payload))
		return
	}

	t.Fatalf("resp=%+v, Payload=%s", resp, string(resp.Payload))
}
