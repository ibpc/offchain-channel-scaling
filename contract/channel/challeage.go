/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"encoding/json"
	"fmt"

	"channel-contract/merkletree"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// Challeage 共识阶段用户发起挑战
func Challeage() protogo.Response {
	funcName := "Challeage"
	params := sdk.Instance.GetArgs()
	userPK, err := sdk.Instance.GetSenderPk()
	if err != nil {
		errStr := fmt.Sprintf("[%s] GetSenderPk failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	channelName := string(params["channel_name"])
	channel, err := GetChannel(channelName)
	if err != nil {
		errStr := fmt.Sprintf("[%s] getChannel failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	// 验证是否 leader
	if userPK == channel.LeaderPK {
		errStr := fmt.Sprintf("[%s] user are leader, cann't challeage", funcName)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	merkletreeBytes := params["merkletree"]

	var mkltree merkletree.MerkleTree
	err = json.Unmarshal(merkletreeBytes, &mkltree)
	if err != nil {
		errStr := fmt.Sprintf("[%s] json unmarshal mkltree data failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	userID, ok := channel.UserPk2ID[userPK]
	if !ok {
		errStr := fmt.Sprintf("[%s] find userid failed", funcName)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	// 发起挑战，进入共识状态
	channel.Status = Consensus
	channel.ChalleageStatus = HasChalleage

	if len(channel.Challeage) == 0 {
		channel.Challeage = make(map[string]merkletree.MerkleTree)
	}
	channel.Challeage[userID] = mkltree

	err = SetChannel(channelName, channel)
	if err != nil {
		errStr := fmt.Sprintf("[%s] set channel failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	sdk.Instance.Log(fmt.Sprintf("[%s] success", funcName))
	return sdk.Success([]byte("Challeage success"))
}

// GetChalleage 领导者查询共识阶段挑战
func GetChalleage() protogo.Response {
	funcName := "GetChalleage"
	params := sdk.Instance.GetArgs()
	userPK, err := sdk.Instance.GetSenderPk()
	if err != nil {
		errStr := fmt.Sprintf("[%s] GetSenderPk failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	channelName := string(params["channel_name"])
	channel, err := GetChannel(channelName)
	if err != nil {
		errStr := fmt.Sprintf("[%s] getChannel failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	// 验证是否 leader
	if userPK != channel.LeaderPK {
		errStr := fmt.Sprintf("[%s] user are not leader, cann't get challeage", funcName)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	var challeageIds = make([]string, 0, len(channel.Challeage))
	for challeageId := range channel.Challeage {
		challeageIds = append(challeageIds, challeageId)
	}

	challeageIdBytes, err := json.Marshal(challeageIds)
	if err != nil {
		errStr := fmt.Sprintf("[challeage] json Marshal challeageIds failed, err=%v", err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	sdk.Instance.Log(fmt.Sprintf("[%s] success", funcName))
	return sdk.Success(challeageIdBytes)
}
