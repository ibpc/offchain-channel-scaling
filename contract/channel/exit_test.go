/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"testing"

	"channel-contract/mock"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
	"github.com/agiledragon/gomonkey/v2"
)

// TestExit 测试退出
func TestExit(t *testing.T) {
	args := make(map[string][]byte)
	args["channel_name"] = []byte("channel1")
	sdk.Instance = &mock.SDKInatanceMock{
		Args: args,
	}

	gomonkey.ApplyFunc(GetChannel, func(channelName string) (Channel, error) {
		var chann = Channel{
			LeaderPK:        "xxx",
			ChalleageStatus: SolveStatus,
			Epoch:           1,
			UserPk2ID: map[string]string{
				"xxx": "ttt",
			},
			Users: map[string]UserState{
				"ttt": {},
			},
		}
		return chann, nil
	})

	gomonkey.ApplyFunc(GetChannelWithdraw, func(channelName string) (withdraw map[string]int, err error) {
		withdraw = make(map[string]int)
		withdraw["ttt"] = 200
		return
	})

	resp := Exit()
	if resp.Status == 0 {
		t.Logf("Payload=%s", string(resp.Payload))
		return
	}

	t.Fatalf("resp=%+v, Payload=%s", resp, string(resp.Payload))
}
