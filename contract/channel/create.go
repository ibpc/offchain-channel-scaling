/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"encoding/json"
	"fmt"
	"strconv"

	"channel-contract/common"
	inCrypto "channel-contract/crypto"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// CreateChannel 创建通道
func CreateChannel() protogo.Response {
	funcName := "CreateChannel"
	params := sdk.Instance.GetArgs()
	userPk, err := sdk.Instance.GetSenderPk()
	if err != nil {
		errStr := fmt.Sprintf("[%s] GetSenderPk failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}
	channelName := string(params["channel_name"])
	userID := string(params["user_id"])
	userAddress := string(params["user_address"])
	sign := params["sign"]
	pubkeyBytes := params["user_pubkey"]
	keyTypeStr := string(params["key_type"])
	keyType, err := strconv.Atoi(keyTypeStr)
	if err != nil {
		errStr := fmt.Sprintf("[%s] Atoi key_type failed, key_type=%s, err=%v", funcName, keyTypeStr, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}
	_, err = inCrypto.ParsePublicKey(crypto.KeyType(keyType), pubkeyBytes)
	if err != nil {
		errStr := fmt.Sprintf("[%s] ParsePublicKey failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}
	hashTypeStr := string(params["hash_type"])
	hashType, err := strconv.Atoi(hashTypeStr)
	if err != nil {
		errStr := fmt.Sprintf("[%s] Atoi hash_type failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}
	opts := &crypto.SignOpts{Hash: crypto.HashType(hashType)}
	maxUserStr := string(params["max_user"])
	maxUser, err := strconv.Atoi(maxUserStr)
	if err != nil {
		sdk.Instance.Infof(fmt.Sprintf("[%s] Atoi max_user failed, err=%v", funcName, err))
		maxUser = defaultMaxUser
	}
	balanceStr := string(params["balance"])
	balance, err := strconv.Atoi(balanceStr)
	if err != nil {
		errStr := fmt.Sprintf("[%s] Atoi balance failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}
	opentimeStamp, err := sdk.Instance.GetTxTimeStamp()
	if err != nil {
		errStr := fmt.Sprintf("[%s] sdk.Instance.GetTxTimeStamp failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}
	openTime, err := strconv.Atoi(opentimeStamp)
	if err != nil {
		errStr := fmt.Sprintf("[%s] Atoi opentimeStamp failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	// channelName 不存在不会报错，已验证
	existChan, err := GetChannel(channelName)
	if err != nil {
		errStr := fmt.Sprintf("[%s] getChannel failed, err=%v", funcName, err)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	// 判断该通道是否已经存在，若已经存在返回
	if existChan.LeaderId != "" {
		state, err := GetChannelState(channelName)
		if err != nil {
			errStr := fmt.Sprintf("[%s] getChannel state failed, err=%v", funcName, err)
			sdk.Instance.Errorf(errStr)
			return sdk.Error(errStr)
		}

		if state.BalanceSet != nil {
			for _, user := range existChan.Users {
				if _, ok := state.BalanceSet[user.UserID]; !ok {
					state.BalanceSet[user.UserID] = user.Balance
				}
			}
		}

		var chaninfo = ChannelInfo{
			LeaderId:        existChan.LeaderId,
			LeaderAddr:      existChan.LeaderAddr,
			Epoch:           existChan.Epoch,
			Status:          existChan.Status,
			ChalleageStatus: existChan.ChalleageStatus,
			BalanceSet:      state.BalanceSet,
		}

		chaninfoBytes, err := json.Marshal(chaninfo)
		if err != nil {
			errStr := fmt.Sprintf("[%s] json Marshal challeageIds failed, err=%v", funcName, err)
			sdk.Instance.Errorf(errStr)
			return sdk.Error(errStr)
		}

		sdk.Instance.Infof(fmt.Sprintf("[%s] channel %s is exist", funcName, channelName))
		return sdk.Success(chaninfoBytes)
	}

	resp := sdk.Instance.CallContract(common.StakeContract, "init", map[string][]byte{
		"user_id":     []byte(userID),
		"user_pubkey": pubkeyBytes,
		"key_type":    []byte(keyTypeStr),
		"hash_type":   []byte(hashTypeStr),
	})

	if resp.Status != 0 {
		errStr := fmt.Sprintf("[%s] call %s init failed, resp=%+v", funcName, common.StakeContract, resp)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	sdk.Instance.Infof("user_id=%s, to=%s, num=%s, sign=%v", userID, common.ChannelContract, balanceStr, sign)

	resp = sdk.Instance.CallContract(common.StakeContract, "transfer", map[string][]byte{
		"from": []byte(userID),
		"to":   []byte(common.ChannelContract),
		"num":  []byte(balanceStr),
		"sign": sign,
	})

	if resp.Status != 0 {
		errStr := fmt.Sprintf("[%s] call %s transfer failed, resp=%+v", funcName, common.StakeContract, resp)
		sdk.Instance.Infof(errStr)
		return sdk.Error(errStr)
	}

	channel := &Channel{
		ChannelName: channelName,
		MaxPayers:   maxUser,
		PlayerCount: 1,
		Epoch:       0,
		Status:      Init,
		LeaderId:    userID,
		LeaderPK:    userPk,
		LeaderAddr:  userAddress,
		OpenTime:    openTime,
		UserPk2ID: map[string]string{
			userPk: userID,
		},
		Users: map[string]UserState{
			userID: {
				Address:     userAddress,
				Balance:     balance,
				UserID:      userID,
				UserPK:      userPk,
				PubKeyBytes: pubkeyBytes,
				KeyType:     crypto.KeyType(keyType),
				SignOpts:    opts,
			},
		},
	} //初始化结构体
	sdk.Instance.EmitEvent("create_channel", []string{channel.ChannelName, fmt.Sprint(channel.OpenTime)})
	err = SetChannel(channelName, *channel)
	if err != nil {
		sdk.Instance.Infof(fmt.Sprintf("[%s] setChannel failed, err=%v", funcName, err))
		return sdk.Error("fail to create channel")
	}

	var state = State{
		BalanceSet: make(map[string]int),
	}

	state.BalanceSet[userID] = balance

	var chaninfo = ChannelInfo{
		LeaderId:        channel.LeaderId,
		LeaderAddr:      channel.LeaderAddr,
		Epoch:           channel.Epoch,
		Status:          channel.Status,
		ChalleageStatus: channel.ChalleageStatus,
		BalanceSet:      state.BalanceSet,
	}

	chaninfoBytes, err := json.Marshal(chaninfo)
	if err != nil {
		errStr := fmt.Sprintf("[%s] json Marshal challeageIds failed, err=%v", funcName, err)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}

	// 记录日志
	sdk.Instance.Infof(fmt.Sprintf("[%s] ChannelName=%s", funcName, channel.ChannelName))
	// 返回结果
	return sdk.Success(chaninfoBytes)
}
