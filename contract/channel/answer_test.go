/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"encoding/json"
	"testing"

	"channel-contract/merkletree"
	"channel-contract/mock"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
	"github.com/agiledragon/gomonkey/v2"
)

// TestAnswer 测试回应挑战
func TestAnswer(t *testing.T) {
	args := make(map[string][]byte)
	args["channel_name"] = []byte("channel1")

	mkltree, err := merkletree.New([][]byte{[]byte("data1"), []byte("data2")})
	if err != nil {
		t.Fatal("merkletree.New failed, ", err)
	}

	mkltreedata, err := json.Marshal(mkltree)
	if err != nil {
		t.Fatal("json.Marshal failed, ", err)
	}

	args["answer_id"] = []byte("ttt")
	args["merkletree"] = mkltreedata
	sdk.Instance = &mock.SDKInatanceMock{
		Args: args,
	}

	gomonkey.ApplyFunc(GetChannel, func(channelName string) (Channel, error) {
		var chann = Channel{
			LeaderPK:        "xxx",
			ChalleageStatus: SolveStatus,
			Epoch:           1,
			UserPk2ID: map[string]string{
				"xxx": "ttt",
			},
			Users: map[string]UserState{
				"ttt": {},
			},
			Reject: make(map[string]int),
			Challeage: map[string]merkletree.MerkleTree{
				"ttt": *mkltree,
			},
		}
		return chann, nil
	})

	gomonkey.ApplyFunc(GetChannelWithdraw, func(channelName string) (withdraw map[string]int, err error) {
		withdraw = make(map[string]int)
		withdraw["ttt"] = 200
		return
	})

	resp := Answer()
	if resp.Status == 0 {
		t.Logf("Payload=%s", string(resp.Payload))
		return
	}

	t.Fatalf("resp=%+v, Payload=%s", resp, string(resp.Payload))
}
