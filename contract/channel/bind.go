/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"fmt"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// Bind 绑定权益合约 rand key
func Bind() protogo.Response {
	funcName := "Bind"
	params := sdk.Instance.GetArgs()

	randKey := string(params["rand_key"])
	var stakeRandKey = ContractKey{
		RandKey: randKey,
	}
	oldStakeRandKey, err := GetStakeRandKey()
	if err != nil {
		errStr := fmt.Sprintf("[%s] getStakeRandKey failed, err=%v", funcName, err)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}

	if oldStakeRandKey.RandKey != "" {
		errStr := fmt.Sprintf("[%s] Has bind rand key", funcName)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}

	err = SetStakeRandKey(stakeRandKey)
	if err != nil {
		errStr := fmt.Sprintf("[%s] setStakeRandKey failed, err=%v", funcName, err)
		sdk.Instance.Errorf(errStr)
		return sdk.Error(errStr)
	}

	sdk.Instance.Log(fmt.Sprintf("[%s] bind success", funcName))
	return sdk.Success([]byte("Bind success"))
}
