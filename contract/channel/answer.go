/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"encoding/json"
	"fmt"

	"channel-contract/merkletree"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// Answer 领导者回应挑战
// return true 为领导者正确
// return false 为挑战者正确
func Answer() protogo.Response {
	funcName := "Answer"
	params := sdk.Instance.GetArgs()
	userPK, err := sdk.Instance.GetSenderPk()
	if err != nil {
		errStr := fmt.Sprintf("[%s] GetSenderPk failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	channelName := string(params["channel_name"])
	channel, err := GetChannel(channelName)
	if err != nil {
		errStr := fmt.Sprintf("[%s] getChannel failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	// 验证是否 leader
	if userPK != channel.LeaderPK {
		errStr := fmt.Sprintf("[%s] user are not leader, cann't answer", funcName)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	answerId := string(params["answer_id"])
	merkletreeBytes := params["merkletree"]

	var mkltree merkletree.MerkleTree
	err = json.Unmarshal(merkletreeBytes, &mkltree)
	if err != nil {
		errStr := fmt.Sprintf("[%s] json unmarshal mkltree data failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	challMerkleTree, ok := channel.Challeage[answerId]
	if !ok {
		errStr := fmt.Sprintf("[%s] answerId not exist, answerId=%s", funcName, answerId)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	leaderTxLen := int64(len(mkltree.Data))
	challTxLen := int64(len(challMerkleTree.Data))

	var leaderOk bool
	lenEqual, equal, index := mkltree.LeafDiff(&challMerkleTree)
	switch {
	case lenEqual && equal:
		// 完全相等，认为挑战无效
		leaderOk = true
	case !lenEqual && equal:
		// 交易数量不同，索引相同的部分相同
		// 如果挑战者提交的交易多，验证多的部分交易的 leader 签名
		// 否则验证 leader 提交的多的部分交易的挑战者签名
		if leaderTxLen < challTxLen {
			challTxOk, err := VerifySign(&channel, &challMerkleTree, channel.LeaderId, index, false)
			if !challTxOk || err != nil {
				sdk.Instance.Log(fmt.Sprintf("[%s] verifySign failed, leaderId=%s, answerId=%s, err=%v", funcName, channel.LeaderId, answerId, err))
				leaderOk = true
				break
			}
		} else {
			leaderTxOk, err := VerifySign(&channel, &mkltree, answerId, index, true)
			if !leaderTxOk || err != nil {
				sdk.Instance.Log(fmt.Sprintf("[%s] verifySign failed, answerId=%s, err=%v", funcName, answerId, err))
				break
			}
			leaderOk = true
		}
	case !equal:
		// 存在不一样的交易，进行验签：
		// 验证 leader 该笔交易「挑战者」签名
		leaderTxOk, err := VerifySign(&channel, &mkltree, answerId, index, true)
		if err != nil {
			sdk.Instance.Log(fmt.Sprintf("[%s] verifySign failed, answerId=%s, err=%v", funcName, answerId, err))
		}

		challTxOk, err := VerifySign(&channel, &challMerkleTree, channel.LeaderId, index, false)
		if err != nil {
			sdk.Instance.Log(fmt.Sprintf("[%s] verifySign failed, leaderId=%s, answerId=%s, err=%v", funcName, channel.LeaderId, answerId, err))
		}

		// 当两边交易签名都 OK，则根据交易数量
		// 交易数量多的对，短的为故意少提交交易
		// 相等则认为挑战无效，因为挑战者私钥认为不会泄露，然又有不同交易
		// 说明挑战者和 leader 都少提交交易，既然挑战者涉及自身利益时都不提交完整交易
		// 则认为 leader ok
		if leaderTxOk && challTxOk {
			if leaderTxLen < challTxLen {
				leaderOk = false
			} else if leaderTxLen > challTxLen {
				leaderOk = true
			} else {
				leaderOk = true
			}
		}
	}

	// leaderOk 是判断为 leader 对，否则挑战者对
	// leader 对则进行下一轮交易
	// 反之回滚，选择新的 leader
	if leaderOk {
		if len(channel.Reject) == 0 {
			channel.Reject = make(map[string]int)
		}
		channel.Reject[answerId] = 1
		channel.ChalleageStatus = SolveStatus
		err = SetChannel(channelName, channel)
		if err != nil {
			errStr := fmt.Sprintf("[%s] resolve setChannel failed, err=%v", funcName, err)
			sdk.Instance.Log(errStr)
			return sdk.Error(errStr)
		}
		return sdk.Success([]byte("true"))
	} else {
		user := channel.Users[answerId]
		channel.LeaderId = user.UserID
		channel.LeaderPK = user.UserPK
		channel.LeaderAddr = user.Address
		channel.ChalleageStatus = RollbackStatus
		err = SetChannel(channelName, channel)
		if err != nil {
			errStr := fmt.Sprintf("[%s] setChannel failed, err=%v", funcName, err)
			sdk.Instance.Log(errStr)
			return sdk.Error(errStr)
		}
		return sdk.Success([]byte("false"))
	}
}
