/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"fmt"

	"channel-contract/merkletree"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// Rollback 共识阶段用户挑战成功，新领导者节点进行回滚
func Rollback() protogo.Response {
	funcName := "Rollback"
	params := sdk.Instance.GetArgs()
	userPK, err := sdk.Instance.GetSenderPk()
	if err != nil {
		errStr := fmt.Sprintf("[%s] GetSenderPk failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	channelName := string(params["channel_name"])
	channel, err := GetChannel(channelName)
	if err != nil {
		errStr := fmt.Sprintf("[%s] getChannel failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	// 验证是否 leader
	if userPK != channel.LeaderPK {
		errStr := fmt.Sprintf("[%s] user are not leader, cann't rollback", funcName)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	channel.Status = Exchange
	channel.ChalleageStatus = NoChalleage
	channel.Challeage = make(map[string]merkletree.MerkleTree)

	err = SetChannel(channelName, channel)
	if err != nil {
		errStr := fmt.Sprintf("[%s] set channel failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	sdk.Instance.Log(fmt.Sprintf("[%s] success", funcName))
	return sdk.Success([]byte("Rollback success"))
}
