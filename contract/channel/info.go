/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
and Privacy Computing. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package channel

import (
	"encoding/json"
	"fmt"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// GetChannelInfo 获取通道信息
func GetChannelInfo() protogo.Response {
	funcName := "GetChannelInfo"
	params := sdk.Instance.GetArgs()
	channelName := string(params["channel_name"])
	channel, err := GetChannel(channelName)
	if err != nil {
		errStr := fmt.Sprintf("[%s] getChannel failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	state, err := GetChannelState(channelName)
	if err != nil {
		errStr := fmt.Sprintf("[%s] getChannel state failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	if state.BalanceSet != nil {
		for _, user := range channel.Users {
			if _, ok := state.BalanceSet[user.UserID]; !ok {
				state.BalanceSet[user.UserID] = user.Balance
			}
		}
	}

	var chaninfo = ChannelInfo{
		LeaderId:        channel.LeaderId,
		LeaderAddr:      channel.LeaderAddr,
		Epoch:           channel.Epoch,
		Status:          channel.Status,
		ChalleageStatus: channel.ChalleageStatus,
		BalanceSet:      state.BalanceSet,
	}

	chaninfoBytes, err := json.Marshal(chaninfo)
	if err != nil {
		errStr := fmt.Sprintf("[%s] json Marshal challeageIds failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	sdk.Instance.Log(fmt.Sprintf("[%s] get channel info success", funcName))
	return sdk.Success(chaninfoBytes)
}

// GetUserInfo 领导者获取用户信息
func GetUserInfo() protogo.Response {
	funcName := "GetUserInfo"
	params := sdk.Instance.GetArgs()
	userPK, err := sdk.Instance.GetSenderPk()
	if err != nil {
		errStr := fmt.Sprintf("[%s] GetSenderPk failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	channelName := string(params["channel_name"])
	channel, err := GetChannel(channelName)
	if err != nil {
		errStr := fmt.Sprintf("[%s] getChannel failed, err=%v", funcName, err)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}
	// 验证是否 leader
	if userPK != channel.LeaderPK {
		errStr := fmt.Sprintf("[%s] user are not leader, cann't get challeage", funcName)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	userID := string(params["user_id"])

	user, ok := channel.Users[userID]
	if !ok {
		errStr := fmt.Sprintf("[%s] Cann't find user state, user_id=%s", funcName, userID)
		sdk.Instance.Log(errStr)
		return sdk.Error(errStr)
	}

	return sdk.Success([]byte(fmt.Sprint(user.Balance)))
}
